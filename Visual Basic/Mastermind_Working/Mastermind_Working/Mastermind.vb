Public Class frmMastermind
    Dim objRandom As New System.Random( _
      CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer)) 'random colour gen

    'load / save
    Dim filename As String = ""
    Dim pathInUse As String = ""
    Dim fileInUse As String = ""

    Dim w As Integer
    Dim x As Integer
    Dim y As Integer
    Dim z As Integer

    'Black count
    Dim Row1 As Integer 'is the black count for row 1
    Dim Row2 As Integer
    Dim Row3 As Integer
    Dim Row4 As Integer
    Dim Row5 As Integer
    Dim Row6 As Integer
    Dim Row7 As Integer
    Dim Row8 As Integer

    'White count
    Dim Black1 As Integer 'is the white count for row 1
    Dim Black2 As Integer
    Dim Black3 As Integer
    Dim Black4 As Integer
    Dim Black5 As Integer
    Dim Black6 As Integer
    Dim Black7 As Integer
    Dim Black8 As Integer

    'colour array
    Dim PictureColour() As Color = {Color.Red, Color.Blue, Color.Lime, Color.Yellow, Color.Purple, Color.Orange}
    Public Function GetRandomNumber( _
      Optional ByVal Low As Integer = 0, _
      Optional ByVal High As Integer = 5) As Integer
        ' Returns a random number,
        ' between the optional Low and High parameters
        Return objRandom.Next(Low, High + 1)
    End Function
    Private Sub frmMastermind_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        w = GetRandomNumber(0, 5) 'gets a random number from 0 - 5
        x = GetRandomNumber(0, 5)
        y = GetRandomNumber(0, 5)
        z = GetRandomNumber(0, 5)

        gboxRow1.Visible = False
        gboxRow2.Visible = False
        gboxRow3.Visible = False
        gboxRow4.Visible = False
        gboxRow5.Visible = False
        gboxRow6.Visible = False
        gboxRow7.Visible = False
        gboxRow8.Visible = False

        'colour generator
        If w = 0 Then pboxSoloution1.BackColor = PictureColour(0) Else  'if the random number = 0, then assign the pboxsoloution backcolour to the array of 0
        If w = 1 Then pboxSoloution1.BackColor = PictureColour(1) Else 
        If w = 2 Then pboxSoloution1.BackColor = PictureColour(2) Else 
        If w = 3 Then pboxSoloution1.BackColor = PictureColour(3) Else 
        If w = 4 Then pboxSoloution1.BackColor = PictureColour(4) Else 
        If w = 5 Then pboxSoloution1.BackColor = PictureColour(5)
        If x = 0 Then pboxSoloution2.BackColor = PictureColour(0) Else 
        If x = 1 Then pboxSoloution2.BackColor = PictureColour(1) Else 
        If x = 2 Then pboxSoloution2.BackColor = PictureColour(2) Else 
        If x = 3 Then pboxSoloution2.BackColor = PictureColour(3) Else 
        If x = 4 Then pboxSoloution2.BackColor = PictureColour(4) Else 
        If x = 5 Then pboxSoloution2.BackColor = PictureColour(5)
        If y = 0 Then pboxSoloution3.BackColor = PictureColour(0) Else 
        If y = 1 Then pboxSoloution3.BackColor = PictureColour(1) Else 
        If y = 2 Then pboxSoloution3.BackColor = PictureColour(2) Else 
        If y = 3 Then pboxSoloution3.BackColor = PictureColour(3) Else 
        If y = 4 Then pboxSoloution3.BackColor = PictureColour(4) Else 
        If y = 5 Then pboxSoloution3.BackColor = PictureColour(5)
        If z = 0 Then pboxSoloution4.BackColor = PictureColour(0) Else 
        If z = 1 Then pboxSoloution4.BackColor = PictureColour(1) Else 
        If z = 2 Then pboxSoloution4.BackColor = PictureColour(2) Else 
        If z = 3 Then pboxSoloution4.BackColor = PictureColour(3) Else 
        If z = 4 Then pboxSoloution4.BackColor = PictureColour(4) Else 
        If z = 5 Then pboxSoloution4.BackColor = PictureColour(5)

        pboxRed.BackColor = PictureColour(0) 'basic colours for selecting
        pboxBlue.BackColor = PictureColour(1)
        pboxGreen.BackColor = PictureColour(2)
        pboxYellow.BackColor = PictureColour(3)
        pboxPurple.BackColor = PictureColour(4)
        pboxOrange.BackColor = PictureColour(5)
    End Sub

    Private Sub pboxGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGreen.Click
        pboxColour.BackColor = PictureColour(2) 'when pboxgreen is clicked, assign is the value of green in the array
    End Sub

    Private Sub pboxOrange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxOrange.Click
        pboxColour.BackColor = PictureColour(5)
    End Sub

    Private Sub pboxBlue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxBlue.Click
        pboxColour.BackColor = PictureColour(1)
    End Sub

    Private Sub pboxPurple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxPurple.Click
        pboxColour.BackColor = PictureColour(4)
    End Sub

    Private Sub pboxYellow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxYellow.Click
        pboxColour.BackColor = PictureColour(3)
    End Sub

    Private Sub pboxRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxRed.Click
        pboxColour.BackColor = PictureColour(0)
    End Sub

    Private Sub btnHint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHint.Click
        pboxHide2.Visible = False 'reveals one of the solution boxes
        btnHint.Enabled = False 'disables the buttons
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        gboxRow1.Visible = True 'makes the first row visible and ensures all guess boxes are the default colour
        pboxGuess1.BackColor = Color.Black
        pboxGuess2.BackColor = Color.Black
        pboxGuess3.BackColor = Color.Black
        pboxGuess4.BackColor = Color.Black
        pboxGuess5.BackColor = Color.Black
        pboxGuess6.BackColor = Color.Black
        pboxGuess7.BackColor = Color.Black
        pboxGuess8.BackColor = Color.Black
        pboxGuess9.BackColor = Color.Black
        pboxGuess10.BackColor = Color.Black
        pboxGuess11.BackColor = Color.Black
        pboxGuess12.BackColor = Color.Black
        pboxGuess13.BackColor = Color.Black
        pboxGuess14.BackColor = Color.Black
        pboxGuess15.BackColor = Color.Black
        pboxGuess16.BackColor = Color.Black
        pboxGuess17.BackColor = Color.Black
        pboxGuess18.BackColor = Color.Black
        pboxGuess19.BackColor = Color.Black
        pboxGuess20.BackColor = Color.Black
        pboxGuess21.BackColor = Color.Black
        pboxGuess22.BackColor = Color.Black
        pboxGuess23.BackColor = Color.Black
        pboxGuess24.BackColor = Color.Black
        pboxGuess25.BackColor = Color.Black
        pboxGuess26.BackColor = Color.Black
        pboxGuess27.BackColor = Color.Black
        pboxGuess28.BackColor = Color.Black
        pboxGuess29.BackColor = Color.Black
        pboxGuess30.BackColor = Color.Black
        pboxGuess31.BackColor = Color.Black
        pboxGuess32.BackColor = Color.Black
        btnStart.Enabled = False
    End Sub

    Private Sub btnTurn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn1.Click
        'win method - If the guess boxes = the solution boxes in the correct place, then the user wins.
        If pboxGuess1.BackColor = pboxSoloution1.BackColor And pboxGuess2.BackColor = pboxSoloution2.BackColor And pboxGuess3.BackColor = pboxSoloution3.BackColor And pboxGuess4.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in ONE move!")
            'restart method
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
        Else
            'if any of the guess boxes = the default colour then display a message
            If pboxGuess1.BackColor = Color.Black Or pboxGuess2.BackColor = Color.Black Or pboxGuess3.BackColor = Color.Black Or pboxGuess4.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                'method for guessing - show the next row and disable the first; this stops the user changing their guesses
                gboxRow2.Visible = True
                gboxRow1.Enabled = False

                'row = blackcount black1-8 = whitecount
                'if guess = the solution and is in the correct position add 1 to the black count
                If pboxGuess1.BackColor = pboxSoloution1.BackColor Then Row1 = Row1 + 1
                If pboxGuess2.BackColor = pboxSoloution2.BackColor Then Row1 = Row1 + 1
                If pboxGuess3.BackColor = pboxSoloution3.BackColor Then Row1 = Row1 + 1
                If pboxGuess4.BackColor = pboxSoloution4.BackColor Then Row1 = Row1 + 1

                'if the guess = the right colour in a different solution box besides the solution then add 1 to the white count
                If pboxGuess1.BackColor = pboxSoloution2.BackColor Then Black1 = Black1 + 1
                If pboxGuess1.BackColor = pboxSoloution3.BackColor Then Black1 = Black1 + 1
                If pboxGuess1.BackColor = pboxSoloution4.BackColor Then Black1 = Black1 + 1
                If pboxGuess2.BackColor = pboxSoloution1.BackColor Then Black1 = Black1 + 1
                If pboxGuess2.BackColor = pboxSoloution3.BackColor Then Black1 = Black1 + 1
                If pboxGuess2.BackColor = pboxSoloution4.BackColor Then Black1 = Black1 + 1
                If pboxGuess3.BackColor = pboxSoloution1.BackColor Then Black1 = Black1 + 1
                If pboxGuess3.BackColor = pboxSoloution4.BackColor Then Black1 = Black1 + 1
                If pboxGuess3.BackColor = pboxSoloution2.BackColor Then Black1 = Black1 + 1
                If pboxGuess4.BackColor = pboxSoloution1.BackColor Then Black1 = Black1 + 1
                If pboxGuess4.BackColor = pboxSoloution3.BackColor Then Black1 = Black1 + 1
                If pboxGuess4.BackColor = pboxSoloution2.BackColor Then Black1 = Black1 + 1
                'failproof - in case the score is over 4, ensure it stays at 4
                If Black1 > 4 Then Black1 = 4
            End If
        End If
        'dimming the labels to the values of the black and white count
        lblBlack1.Text = Black1
        lblRow1.Text = Row1
    End Sub

    Private Sub btnTurn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn2.Click
        If pboxGuess5.BackColor = pboxSoloution1.BackColor And pboxGuess6.BackColor = pboxSoloution2.BackColor And pboxGuess7.BackColor = pboxSoloution3.BackColor And pboxGuess8.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in TWO moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
        Else
            If pboxGuess5.BackColor = Color.Black Or pboxGuess6.BackColor = Color.Black Or pboxGuess7.BackColor = Color.Black Or pboxGuess8.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow3.Visible = True
                gboxRow2.Enabled = False
                If pboxGuess5.BackColor = pboxSoloution1.BackColor Then Row2 = Row2 + 1
                If pboxGuess6.BackColor = pboxSoloution2.BackColor Then Row2 = Row2 + 1
                If pboxGuess7.BackColor = pboxSoloution3.BackColor Then Row2 = Row2 + 1
                If pboxGuess8.BackColor = pboxSoloution4.BackColor Then Row2 = Row2 + 1
                If pboxGuess5.BackColor = pboxSoloution2.BackColor Then Black2 = Black2 + 1
                If pboxGuess1.BackColor = pboxSoloution3.BackColor Then Black2 = Black2 + 1
                If pboxGuess1.BackColor = pboxSoloution4.BackColor Then Black2 = Black2 + 1
                If pboxGuess6.BackColor = pboxSoloution1.BackColor Then Black2 = Black2 + 1
                If pboxGuess2.BackColor = pboxSoloution3.BackColor Then Black2 = Black2 + 1
                If pboxGuess2.BackColor = pboxSoloution4.BackColor Then Black2 = Black2 + 1
                If pboxGuess7.BackColor = pboxSoloution1.BackColor Then Black2 = Black2 + 1
                If pboxGuess3.BackColor = pboxSoloution4.BackColor Then Black2 = Black2 + 1
                If pboxGuess3.BackColor = pboxSoloution2.BackColor Then Black2 = Black2 + 1
                If pboxGuess8.BackColor = pboxSoloution1.BackColor Then Black2 = Black2 + 1
                If pboxGuess4.BackColor = pboxSoloution3.BackColor Then Black2 = Black2 + 1
                If pboxGuess4.BackColor = pboxSoloution2.BackColor Then Black2 = Black2 + 1
                If Black2 > 4 Then Black2 = 4
            End If
            End If
        lblRow2.Text = Row2
        lblBlack2.Text = Black2
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnTurn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn3.Click
        If pboxGuess9.BackColor = pboxSoloution1.BackColor And pboxGuess10.BackColor = pboxSoloution2.BackColor And pboxGuess11.BackColor = pboxSoloution3.BackColor And pboxGuess12.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in THREE moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
            gboxRow2.Enabled = True
        Else
            If pboxGuess9.BackColor = Color.Black Or pboxGuess10.BackColor = Color.Black Or pboxGuess11.BackColor = Color.Black Or pboxGuess12.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow4.Visible = True
                gboxRow3.Enabled = False
                If pboxGuess9.BackColor = pboxSoloution1.BackColor Then Row3 = Row3 + 1
                If pboxGuess10.BackColor = pboxSoloution2.BackColor Then Row3 = Row3 + 1
                If pboxGuess11.BackColor = pboxSoloution3.BackColor Then Row3 = Row3 + 1
                If pboxGuess12.BackColor = pboxSoloution4.BackColor Then Row3 = Row3 + 1
                If pboxGuess9.BackColor = pboxSoloution2.BackColor Then Black3 = Black3 + 1
                If pboxGuess9.BackColor = pboxSoloution3.BackColor Then Black3 = Black3 + 1
                If pboxGuess9.BackColor = pboxSoloution4.BackColor Then Black3 = Black3 + 1
                If pboxGuess10.BackColor = pboxSoloution1.BackColor Then Black3 = Black3 + 1
                If pboxGuess10.BackColor = pboxSoloution3.BackColor Then Black3 = Black3 + 1
                If pboxGuess10.BackColor = pboxSoloution4.BackColor Then Black3 = Black3 + 1
                If pboxGuess11.BackColor = pboxSoloution1.BackColor Then Black3 = Black3 + 1
                If pboxGuess11.BackColor = pboxSoloution4.BackColor Then Black3 = Black3 + 1
                If pboxGuess11.BackColor = pboxSoloution2.BackColor Then Black3 = Black3 + 1
                If pboxGuess12.BackColor = pboxSoloution1.BackColor Then Black3 = Black3 + 1
                If pboxGuess12.BackColor = pboxSoloution3.BackColor Then Black3 = Black3 + 1
                If pboxGuess12.BackColor = pboxSoloution2.BackColor Then Black3 = Black3 + 1
                If Black3 > 4 Then Black3 = 4
            End If
        End If
        lblRow3.Text = Row3
        lblBlack3.Text = Black3
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnTurn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn4.Click
        If pboxGuess13.BackColor = pboxSoloution1.BackColor And pboxGuess14.BackColor = pboxSoloution2.BackColor And pboxGuess15.BackColor = pboxSoloution3.BackColor And pboxGuess16.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in FOUR moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
            gboxRow2.Enabled = True
            gboxRow3.Enabled = True
        Else
            If pboxGuess13.BackColor = Color.Black Or pboxGuess14.BackColor = Color.Black Or pboxGuess15.BackColor = Color.Black Or pboxGuess16.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow5.Visible = True
                gboxRow4.Enabled = False
                If pboxGuess13.BackColor = pboxSoloution1.BackColor Then Row4 = Row4 + 1
                If pboxGuess14.BackColor = pboxSoloution2.BackColor Then Row4 = Row4 + 1
                If pboxGuess15.BackColor = pboxSoloution3.BackColor Then Row4 = Row4 + 1
                If pboxGuess16.BackColor = pboxSoloution4.BackColor Then Row4 = Row4 + 1
                If pboxGuess13.BackColor = pboxSoloution2.BackColor Then Black4 = Black4 + 1
                If pboxGuess13.BackColor = pboxSoloution3.BackColor Then Black4 = Black4 + 1
                If pboxGuess13.BackColor = pboxSoloution4.BackColor Then Black4 = Black4 + 1
                If pboxGuess14.BackColor = pboxSoloution1.BackColor Then Black4 = Black4 + 1
                If pboxGuess14.BackColor = pboxSoloution3.BackColor Then Black4 = Black4 + 1
                If pboxGuess14.BackColor = pboxSoloution4.BackColor Then Black4 = Black4 + 1
                If pboxGuess15.BackColor = pboxSoloution1.BackColor Then Black4 = Black4 + 1
                If pboxGuess15.BackColor = pboxSoloution4.BackColor Then Black4 = Black4 + 1
                If pboxGuess15.BackColor = pboxSoloution2.BackColor Then Black4 = Black4 + 1
                If pboxGuess16.BackColor = pboxSoloution1.BackColor Then Black4 = Black4 + 1
                If pboxGuess16.BackColor = pboxSoloution3.BackColor Then Black4 = Black4 + 1
                If pboxGuess16.BackColor = pboxSoloution2.BackColor Then Black4 = Black4 + 1
                If Black4 > 4 Then Black4 = 4
            End If
        End If
        lblRow4.Text = Row4
        lblBlack4.Text = Black4
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnTurn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn5.Click
        If pboxGuess17.BackColor = pboxSoloution1.BackColor And pboxGuess18.BackColor = pboxSoloution2.BackColor And pboxGuess19.BackColor = pboxSoloution3.BackColor And pboxGuess20.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in FIVE moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
            gboxRow2.Enabled = True
            gboxRow3.Enabled = True
            gboxRow4.Enabled = True
        Else
            If pboxGuess17.BackColor = Color.Black Or pboxGuess18.BackColor = Color.Black Or pboxGuess19.BackColor = Color.Black Or pboxGuess20.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow6.Visible = True
                gboxRow5.Enabled = False
                If pboxGuess17.BackColor = pboxSoloution1.BackColor Then Row5 = Row5 + 1
                If pboxGuess18.BackColor = pboxSoloution2.BackColor Then Row5 = Row5 + 1
                If pboxGuess19.BackColor = pboxSoloution3.BackColor Then Row5 = Row5 + 1
                If pboxGuess20.BackColor = pboxSoloution4.BackColor Then Row5 = Row5 + 1
                If pboxGuess17.BackColor = pboxSoloution2.BackColor Then Black5 = Black5 + 1
                If pboxGuess17.BackColor = pboxSoloution3.BackColor Then Black5 = Black5 + 1
                If pboxGuess17.BackColor = pboxSoloution4.BackColor Then Black5 = Black5 + 1
                If pboxGuess18.BackColor = pboxSoloution1.BackColor Then Black5 = Black5 + 1
                If pboxGuess18.BackColor = pboxSoloution3.BackColor Then Black5 = Black5 + 1
                If pboxGuess18.BackColor = pboxSoloution4.BackColor Then Black5 = Black5 + 1
                If pboxGuess19.BackColor = pboxSoloution1.BackColor Then Black5 = Black5 + 1
                If pboxGuess19.BackColor = pboxSoloution4.BackColor Then Black5 = Black5 + 1
                If pboxGuess19.BackColor = pboxSoloution2.BackColor Then Black5 = Black5 + 1
                If pboxGuess20.BackColor = pboxSoloution1.BackColor Then Black5 = Black5 + 1
                If pboxGuess20.BackColor = pboxSoloution3.BackColor Then Black5 = Black5 + 1
                If pboxGuess20.BackColor = pboxSoloution2.BackColor Then Black5 = Black5 + 1
                If Black5 > 5 Then Black5 = 0
            End If
        End If
        lblRow5.Text = Row5
        lblBlack5.Text = Black5
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnTurn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn6.Click
        If pboxGuess21.BackColor = pboxSoloution1.BackColor And pboxGuess22.BackColor = pboxSoloution2.BackColor And pboxGuess23.BackColor = pboxSoloution3.BackColor And pboxGuess24.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in SIX moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
            gboxRow2.Enabled = True
            gboxRow3.Enabled = True
            gboxRow4.Enabled = True
            gboxRow5.Enabled = True
        Else
            If pboxGuess21.BackColor = Color.Black Or pboxGuess22.BackColor = Color.Black Or pboxGuess23.BackColor = Color.Black Or pboxGuess24.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow7.Visible = True
                gboxRow6.Enabled = False
                If pboxGuess21.BackColor = pboxSoloution1.BackColor Then Row6 = Row6 + 1
                If pboxGuess22.BackColor = pboxSoloution2.BackColor Then Row6 = Row6 + 1
                If pboxGuess23.BackColor = pboxSoloution3.BackColor Then Row6 = Row6 + 1
                If pboxGuess24.BackColor = pboxSoloution4.BackColor Then Row6 = Row6 + 1
                If pboxGuess21.BackColor = pboxSoloution2.BackColor Then Black6 = Black6 + 1
                If pboxGuess21.BackColor = pboxSoloution3.BackColor Then Black6 = Black6 + 1
                If pboxGuess21.BackColor = pboxSoloution4.BackColor Then Black6 = Black6 + 1
                If pboxGuess22.BackColor = pboxSoloution1.BackColor Then Black6 = Black6 + 1
                If pboxGuess22.BackColor = pboxSoloution3.BackColor Then Black6 = Black6 + 1
                If pboxGuess22.BackColor = pboxSoloution4.BackColor Then Black6 = Black6 + 1
                If pboxGuess23.BackColor = pboxSoloution1.BackColor Then Black6 = Black6 + 1
                If pboxGuess23.BackColor = pboxSoloution4.BackColor Then Black6 = Black6 + 1
                If pboxGuess23.BackColor = pboxSoloution2.BackColor Then Black6 = Black6 + 1
                If pboxGuess24.BackColor = pboxSoloution1.BackColor Then Black6 = Black6 + 1
                If pboxGuess24.BackColor = pboxSoloution3.BackColor Then Black6 = Black6 + 1
                If pboxGuess24.BackColor = pboxSoloution2.BackColor Then Black6 = Black6 + 1
                If Black6 > 4 Then Black6 = 0
            End If
        End If
        lblRow6.Text = Row6
        lblBlack6.Text = Black6
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnTurn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn7.Click
        If pboxGuess25.BackColor = pboxSoloution1.BackColor And pboxGuess26.BackColor = pboxSoloution2.BackColor And pboxGuess27.BackColor = pboxSoloution3.BackColor And pboxGuess28.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in SEVEN moves!")
            Me.frmMastermind_Load(sender, e)
            Me.Show()
            pboxHide1.Visible = True
            pboxHide2.Visible = True
            pboxHide3.Visible = True
            pboxHide4.Visible = True
            btnHint.Enabled = True
            btnStart.Enabled = True
            gboxRow1.Enabled = True
            gboxRow2.Enabled = True
            gboxRow3.Enabled = True
            gboxRow4.Enabled = True
            gboxRow5.Enabled = True
            gboxRow6.Enabled = True
        Else
            If pboxGuess25.BackColor = Color.Black Or pboxGuess26.BackColor = Color.Black Or pboxGuess27.BackColor = Color.Black Or pboxGuess28.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                gboxRow8.Visible = True
                gboxRow7.Enabled = False
                If pboxGuess25.BackColor = pboxSoloution1.BackColor Then Row7 = Row7 + 1
                If pboxGuess26.BackColor = pboxSoloution2.BackColor Then Row7 = Row7 + 1
                If pboxGuess27.BackColor = pboxSoloution3.BackColor Then Row7 = Row7 + 1
                If pboxGuess28.BackColor = pboxSoloution4.BackColor Then Row7 = Row7 + 1
                If pboxGuess25.BackColor = pboxSoloution2.BackColor Then Black7 = Black7 + 1
                If pboxGuess25.BackColor = pboxSoloution3.BackColor Then Black7 = Black7 + 1
                If pboxGuess25.BackColor = pboxSoloution4.BackColor Then Black7 = Black7 + 1
                If pboxGuess26.BackColor = pboxSoloution1.BackColor Then Black7 = Black7 + 1
                If pboxGuess26.BackColor = pboxSoloution3.BackColor Then Black7 = Black7 + 1
                If pboxGuess26.BackColor = pboxSoloution4.BackColor Then Black7 = Black7 + 1
                If pboxGuess27.BackColor = pboxSoloution1.BackColor Then Black7 = Black7 + 1
                If pboxGuess27.BackColor = pboxSoloution4.BackColor Then Black7 = Black7 + 1
                If pboxGuess27.BackColor = pboxSoloution2.BackColor Then Black7 = Black7 + 1
                If pboxGuess28.BackColor = pboxSoloution1.BackColor Then Black7 = Black7 + 1
                If pboxGuess28.BackColor = pboxSoloution3.BackColor Then Black7 = Black7 + 1
                If pboxGuess28.BackColor = pboxSoloution2.BackColor Then Black7 = Black7 + 1
                If Black7 > 4 Then Black4 = 4
            End If
        End If
        lblRow7.Text = Row7
        lblBlack7.Text = Black7
        'refer to btnTurn1_click for a detailed explanation
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Dim res = MessageBox.Show("Are you sure you want to exit?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
        If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
            Me.Close()
        End If
        If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
        End If
    End Sub

    Private Sub pboxGuess1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess1.Click
        pboxGuess1.BackColor = pboxColour.BackColor 'the method of colour seleting - when the guessbox is clicked it = the colour selected
    End Sub

    Private Sub pboxGuess2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess2.Click
        pboxGuess2.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess3.Click
        pboxGuess3.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess4.Click
        pboxGuess4.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess5.Click
        pboxGuess5.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess6.Click
        pboxGuess6.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess7.Click
        pboxGuess7.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess8.Click
        pboxGuess8.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess9.Click
        pboxGuess9.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess10.Click
        pboxGuess10.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess11.Click
        pboxGuess11.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess12.Click
        pboxGuess12.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess13.Click
        pboxGuess13.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess14.Click
        pboxGuess14.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess15.Click
        pboxGuess15.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess16.Click
        pboxGuess16.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess17.Click
        pboxGuess17.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess18.Click
        pboxGuess18.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess19.Click
        pboxGuess19.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess20.Click
        pboxGuess20.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess21.Click
        pboxGuess21.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess22.Click
        pboxGuess22.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess23.Click
        pboxGuess23.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess24.Click
        pboxGuess24.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess25.Click
        pboxGuess25.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess26.Click
        pboxGuess26.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess27.Click
        pboxGuess27.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess28.Click
        pboxGuess28.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess29.Click
        pboxGuess29.BackColor = pboxColour.BackColor
    End Sub

    Private Sub pboxGuess30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess30.Click
        pboxGuess30.BackColor = pboxColour.BackColor
    End Sub
    Private Sub pboxGuess31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess31.Click
        pboxGuess31.BackColor = pboxColour.BackColor
    End Sub
    Private Sub pboxGuess32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess32.Click
        pboxGuess32.BackColor = pboxColour.BackColor
    End Sub
    Private Sub btnTurn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn8.Click
        If pboxGuess29.BackColor = pboxSoloution1.BackColor And pboxGuess30.BackColor = pboxSoloution2.BackColor And pboxGuess31.BackColor = pboxSoloution3.BackColor And pboxGuess32.BackColor = pboxSoloution4.BackColor Then
            MessageBox.Show("Congratulations, you've won in EIGHT moves!")
            If pboxGuess29.BackColor = pboxSoloution1.BackColor Then Row8 = Row8 + 1
            If pboxGuess30.BackColor = pboxSoloution2.BackColor Then Row8 = Row8 + 1
            If pboxGuess31.BackColor = pboxSoloution3.BackColor Then Row8 = Row8 + 1
            If pboxGuess32.BackColor = pboxSoloution4.BackColor Then Row8 = Row8 + 1
            If pboxGuess29.BackColor = pboxSoloution2.BackColor Then Black8 = Black8 + 1
            If pboxGuess29.BackColor = pboxSoloution3.BackColor Then Black8 = Black8 + 1
            If pboxGuess29.BackColor = pboxSoloution4.BackColor Then Black8 = Black8 + 1
            If pboxGuess30.BackColor = pboxSoloution1.BackColor Then Black8 = Black8 + 1
            If pboxGuess30.BackColor = pboxSoloution3.BackColor Then Black8 = Black8 + 1
            If pboxGuess30.BackColor = pboxSoloution4.BackColor Then Black8 = Black8 + 1
            If pboxGuess31.BackColor = pboxSoloution1.BackColor Then Black8 = Black8 + 1
            If pboxGuess31.BackColor = pboxSoloution4.BackColor Then Black8 = Black8 + 1
            If pboxGuess31.BackColor = pboxSoloution2.BackColor Then Black8 = Black8 + 1
            If pboxGuess32.BackColor = pboxSoloution1.BackColor Then Black8 = Black8 + 1
            If pboxGuess31.BackColor = pboxSoloution3.BackColor Then Black8 = Black8 + 1
            If pboxGuess32.BackColor = pboxSoloution2.BackColor Then Black8 = Black8 + 1
            Dim res = MessageBox.Show("Congratulations, you've won in EIGHT moves, do you want to start a new game?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
            If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
                Dim es As System.EventArgs
                Dim sndr As System.Object
                Try
                    Dim c As Form.ControlCollection = Me.Controls
                    'c= me.co
                    Dim i As Integer = c.Count
                    'Dim cnt As Integer = c.Count
                    While i <> 0
                        c.Item(0).Dispose()
                        i -= 1
                    End While
                    Me.InitializeComponent()
                    'Ur Form Name in place of frmDailySales_Load(sndr,es)
                    Me.frmMastermind_Load(sndr, es)
                Catch ex As Exception
                    MsgBox(ex.ToString())
                    'Exit Sub
                End Try
            Else
                If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
                End If
            End If
        Else
            If pboxGuess29.BackColor = Color.Black Or pboxGuess30.BackColor = Color.Black Or pboxGuess31.BackColor = Color.Black Or pboxGuess32.BackColor = Color.Black Then
                MessageBox.Show("Please choose a colour")
            Else
                'code will restart the form
                MessageBox.Show("Bad luck, you have run out of moves! Try again.")
                Dim res = MessageBox.Show("Are you sure you want to start a new game?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
                If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
                    Dim es As System.EventArgs
                    Dim sndr As System.Object
                    Try
                        Dim c As Form.ControlCollection = Me.Controls
                        'c= me.co
                        Dim i As Integer = c.Count
                        'Dim cnt As Integer = c.Count
                        While i <> 0
                            c.Item(0).Dispose()
                            i -= 1
                        End While
                        Me.InitializeComponent()
                        'Ur Form Name in place of frmDailySales_Load(sndr,es)
                        Me.frmMastermind_Load(sndr, es)
                    Catch ex As Exception
                        MsgBox(ex.ToString())
                        'Exit Sub
                    End Try
                Else
                    If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
                    End If
                End If
            End If
        End If
        lblRow8.Text = Row8
        lblBlack8.Text = Black8
        'refer to btnTurn1_click for a detailed explanation
    End Sub
    Private Sub btnDev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDev.Click
        ' a mode a created to test elements of the game
        pboxHide1.Visible = False
        pboxHide2.Visible = False
        pboxHide3.Visible = False
        pboxHide4.Visible = False
    End Sub
    Private Sub TutorialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TutorialToolStripMenuItem.Click
        Me.Hide() 'takes the user to the tutorial form
        frmTutorial.Show()
    End Sub
    Private Sub SaveGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveGameToolStripMenuItem.Click
        'show the dialog
        SaveFileDialog1.ShowDialog()
    End Sub
    Private Sub saveGame()
        SaveFileDialog1.ShowDialog()
    End Sub
    Private Sub LoadGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadGameToolStripMenuItem.Click
        OpenFileDialog1.ShowDialog()
    End Sub
    Private Sub loadGame()
        OpenFileDialog1.ShowDialog()
    End Sub
    Private Sub NewGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewGameToolStripMenuItem.Click
        Dim res = MessageBox.Show("Are you sure you want to start a new game?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
        If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
            Dim es As System.EventArgs
            Dim sndr As System.Object
            Try
                Dim c As Form.ControlCollection = Me.Controls
                'c= me.co
                Dim i As Integer = c.Count
                'Dim cnt As Integer = c.Count
                While i <> 0
                    c.Item(0).Dispose()
                    i -= 1
                End While
                Me.InitializeComponent()
                'Ur Form Name in place of frmDailySales_Load(sndr,es)
                Me.frmMastermind_Load(sndr, es)
            Catch ex As Exception
                MsgBox(ex.ToString())
                'Exit Sub
            End Try
        Else
            If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
            End If
        End If
    End Sub
    Private Sub SaveFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        'use current or get new filepath
        If fileInUse <> Nothing Then
            SaveFileDialog1.FileName = fileInUse
            SaveFileDialog1.InitialDirectory = pathInUse
        Else
            filename = SaveFileDialog1.FileName
            fileInUse = System.IO.Path.GetFileName(filename)
            pathInUse = System.IO.Path.GetDirectoryName(filename)
        End If
        'open stream writer
        Dim filewriter As IO.StreamWriter = IO.File.CreateText(filename)
    End Sub
    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        filename = OpenFileDialog1.FileName
        'do only if the ok button was pressed
        If filename <> "" Then
            fileInUse = System.IO.Path.GetFileName(filename)
            pathInUse = System.IO.Path.GetDirectoryName(filename)
            'open streamreader
            Dim filereader As IO.StreamReader = IO.File.OpenText(filename)
            filereader.Close()
        End If
    End Sub
End Class