Public Class frmMainmenu
    'load / save
    Dim filename As String = ""
    Dim pathInUse As String = ""
    Dim fileInUse As String = ""
    Private Sub btnNew_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.MouseHover
        btnNew.BackgroundImage = My.Resources.buttonon 'mouse over - changing the image on mouse over
    End Sub

    Private Sub btnNew_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.MouseLeave
        btnNew.BackgroundImage = My.Resources.buttonoff 'mouse leave - changing the image when the mouse leaves
    End Sub

    Private Sub btnLoad_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.MouseHover
        btnLoad.BackgroundImage = My.Resources.button_loadon 'mouse over - changing the image on mouse over
    End Sub
    Private Sub btnLoad_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.MouseLeave
        btnLoad.BackgroundImage = My.Resources.button_loadoff 'mouse leave - changing the image when the mouse leaves
    End Sub
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Call newgame() 'called the newgame sub
    End Sub

    Private Sub btnExit_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.MouseHover
        btnExit.BackgroundImage = My.Resources.button_exiton 'mouse over - changing the image on mouse over
    End Sub

    Private Sub btnExit_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.MouseLeave
        btnExit.BackgroundImage = My.Resources.button_exitoff 'mouse leave - changing the image when the mouse leaves
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Dim res = MessageBox.Show("Are you sure you want to exit?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
        If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
            Me.Close()
        End If
        If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
        End If
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        OpenFileDialog1.ShowDialog()
    End Sub
    Private Sub newgame()
        Dim res = MessageBox.Show("Do you wish to complete the tutorial?", "Complete the tutorial before playing!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 'Advanced exit with a Yes or No alternative
        If res = Windows.Forms.DialogResult.Yes Then 'If user clicks yes, the program will exit.
            Me.Hide()
            frmTutorial.Show()
        End If
        If res = Windows.Forms.DialogResult.No Then 'If user clicks no, nothing will happen and the user will be back on the form
            Me.Hide()
            frmMastermind.Show()
        End If
    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        filename = OpenFileDialog1.FileName
        'do only if the ok button was pressed
        If filename <> "" Then
            fileInUse = System.IO.Path.GetFileName(filename)
            pathInUse = System.IO.Path.GetDirectoryName(filename)
            'open streamreader
            Dim filereader As IO.StreamReader = IO.File.OpenText(filename)
            filereader.Close()
        End If
    End Sub
End Class
