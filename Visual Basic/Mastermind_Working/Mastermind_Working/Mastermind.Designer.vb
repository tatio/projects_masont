<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMastermind
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pboxGuess1 = New System.Windows.Forms.PictureBox
        Me.pboxGuess2 = New System.Windows.Forms.PictureBox
        Me.pboxGuess3 = New System.Windows.Forms.PictureBox
        Me.pboxGuess4 = New System.Windows.Forms.PictureBox
        Me.pboxSoloution4 = New System.Windows.Forms.PictureBox
        Me.pboxSoloution3 = New System.Windows.Forms.PictureBox
        Me.pboxSoloution2 = New System.Windows.Forms.PictureBox
        Me.pboxSoloution1 = New System.Windows.Forms.PictureBox
        Me.gboxAnswer = New System.Windows.Forms.GroupBox
        Me.pboxHide4 = New System.Windows.Forms.PictureBox
        Me.pboxHide3 = New System.Windows.Forms.PictureBox
        Me.pboxHide2 = New System.Windows.Forms.PictureBox
        Me.pboxHide1 = New System.Windows.Forms.PictureBox
        Me.pboxRed = New System.Windows.Forms.PictureBox
        Me.pboxOrange = New System.Windows.Forms.PictureBox
        Me.pboxBlue = New System.Windows.Forms.PictureBox
        Me.pboxPurple = New System.Windows.Forms.PictureBox
        Me.pboxYellow = New System.Windows.Forms.PictureBox
        Me.pboxGreen = New System.Windows.Forms.PictureBox
        Me.pboxColour = New System.Windows.Forms.PictureBox
        Me.pboxColourHolder = New System.Windows.Forms.PictureBox
        Me.lblRow1 = New System.Windows.Forms.Label
        Me.lblRow2 = New System.Windows.Forms.Label
        Me.lblRow3 = New System.Windows.Forms.Label
        Me.lblRow4 = New System.Windows.Forms.Label
        Me.lblRow5 = New System.Windows.Forms.Label
        Me.lblRow6 = New System.Windows.Forms.Label
        Me.lblRow7 = New System.Windows.Forms.Label
        Me.lblRow8 = New System.Windows.Forms.Label
        Me.btnTurn1 = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnStart = New System.Windows.Forms.Button
        Me.btnHint = New System.Windows.Forms.Button
        Me.gboxRow1 = New System.Windows.Forms.GroupBox
        Me.gboxRow2 = New System.Windows.Forms.GroupBox
        Me.pboxGuess7 = New System.Windows.Forms.PictureBox
        Me.pboxGuess5 = New System.Windows.Forms.PictureBox
        Me.pboxGuess6 = New System.Windows.Forms.PictureBox
        Me.pboxGuess8 = New System.Windows.Forms.PictureBox
        Me.btnTurn2 = New System.Windows.Forms.Button
        Me.gboxRow3 = New System.Windows.Forms.GroupBox
        Me.pboxGuess12 = New System.Windows.Forms.PictureBox
        Me.pboxGuess11 = New System.Windows.Forms.PictureBox
        Me.pboxGuess10 = New System.Windows.Forms.PictureBox
        Me.pboxGuess9 = New System.Windows.Forms.PictureBox
        Me.btnTurn3 = New System.Windows.Forms.Button
        Me.gboxRow4 = New System.Windows.Forms.GroupBox
        Me.pboxGuess16 = New System.Windows.Forms.PictureBox
        Me.pboxGuess15 = New System.Windows.Forms.PictureBox
        Me.pboxGuess14 = New System.Windows.Forms.PictureBox
        Me.pboxGuess13 = New System.Windows.Forms.PictureBox
        Me.btnTurn4 = New System.Windows.Forms.Button
        Me.gboxRow5 = New System.Windows.Forms.GroupBox
        Me.pboxGuess20 = New System.Windows.Forms.PictureBox
        Me.btnTurn5 = New System.Windows.Forms.Button
        Me.pboxGuess19 = New System.Windows.Forms.PictureBox
        Me.pboxGuess18 = New System.Windows.Forms.PictureBox
        Me.pboxGuess17 = New System.Windows.Forms.PictureBox
        Me.gboxRow6 = New System.Windows.Forms.GroupBox
        Me.pboxGuess24 = New System.Windows.Forms.PictureBox
        Me.btnTurn6 = New System.Windows.Forms.Button
        Me.pboxGuess23 = New System.Windows.Forms.PictureBox
        Me.pboxGuess22 = New System.Windows.Forms.PictureBox
        Me.pboxGuess21 = New System.Windows.Forms.PictureBox
        Me.gboxRow7 = New System.Windows.Forms.GroupBox
        Me.pboxGuess28 = New System.Windows.Forms.PictureBox
        Me.btnTurn7 = New System.Windows.Forms.Button
        Me.pboxGuess27 = New System.Windows.Forms.PictureBox
        Me.pboxGuess26 = New System.Windows.Forms.PictureBox
        Me.pboxGuess25 = New System.Windows.Forms.PictureBox
        Me.gboxRow8 = New System.Windows.Forms.GroupBox
        Me.pboxGuess32 = New System.Windows.Forms.PictureBox
        Me.btnTurn8 = New System.Windows.Forms.Button
        Me.pboxGuess31 = New System.Windows.Forms.PictureBox
        Me.pboxGuess30 = New System.Windows.Forms.PictureBox
        Me.pboxGuess29 = New System.Windows.Forms.PictureBox
        Me.btnDev = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TutorialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.lblBlack1 = New System.Windows.Forms.Label
        Me.lblBlack2 = New System.Windows.Forms.Label
        Me.lblBlack3 = New System.Windows.Forms.Label
        Me.lblBlack6 = New System.Windows.Forms.Label
        Me.lblBlack5 = New System.Windows.Forms.Label
        Me.lblBlack4 = New System.Windows.Forms.Label
        Me.lblBlack7 = New System.Windows.Forms.Label
        Me.lblBlack8 = New System.Windows.Forms.Label
        Me.gboxHints = New System.Windows.Forms.GroupBox
        CType(Me.pboxGuess1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxSoloution4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxSoloution3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxSoloution2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxSoloution1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxAnswer.SuspendLayout()
        CType(Me.pboxHide4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxHide3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxHide2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxHide1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxOrange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxPurple, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxYellow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxColour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxColourHolder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow1.SuspendLayout()
        Me.gboxRow2.SuspendLayout()
        CType(Me.pboxGuess7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow3.SuspendLayout()
        CType(Me.pboxGuess12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow4.SuspendLayout()
        CType(Me.pboxGuess16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow5.SuspendLayout()
        CType(Me.pboxGuess20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow6.SuspendLayout()
        CType(Me.pboxGuess24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow7.SuspendLayout()
        CType(Me.pboxGuess28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow8.SuspendLayout()
        CType(Me.pboxGuess32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess29, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.gboxHints.SuspendLayout()
        Me.SuspendLayout()
        '
        'pboxGuess1
        '
        Me.pboxGuess1.BackColor = System.Drawing.Color.Black
        Me.pboxGuess1.Location = New System.Drawing.Point(96, 19)
        Me.pboxGuess1.Name = "pboxGuess1"
        Me.pboxGuess1.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess1.TabIndex = 39
        Me.pboxGuess1.TabStop = False
        '
        'pboxGuess2
        '
        Me.pboxGuess2.BackColor = System.Drawing.Color.Black
        Me.pboxGuess2.Location = New System.Drawing.Point(152, 19)
        Me.pboxGuess2.Name = "pboxGuess2"
        Me.pboxGuess2.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess2.TabIndex = 47
        Me.pboxGuess2.TabStop = False
        '
        'pboxGuess3
        '
        Me.pboxGuess3.BackColor = System.Drawing.Color.Black
        Me.pboxGuess3.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess3.Name = "pboxGuess3"
        Me.pboxGuess3.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess3.TabIndex = 55
        Me.pboxGuess3.TabStop = False
        '
        'pboxGuess4
        '
        Me.pboxGuess4.BackColor = System.Drawing.Color.Black
        Me.pboxGuess4.Location = New System.Drawing.Point(264, 19)
        Me.pboxGuess4.Name = "pboxGuess4"
        Me.pboxGuess4.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess4.TabIndex = 63
        Me.pboxGuess4.TabStop = False
        '
        'pboxSoloution4
        '
        Me.pboxSoloution4.BackColor = System.Drawing.Color.Black
        Me.pboxSoloution4.Location = New System.Drawing.Point(225, 15)
        Me.pboxSoloution4.Name = "pboxSoloution4"
        Me.pboxSoloution4.Size = New System.Drawing.Size(66, 64)
        Me.pboxSoloution4.TabIndex = 74
        Me.pboxSoloution4.TabStop = False
        '
        'pboxSoloution3
        '
        Me.pboxSoloution3.BackColor = System.Drawing.Color.Black
        Me.pboxSoloution3.Location = New System.Drawing.Point(153, 15)
        Me.pboxSoloution3.Name = "pboxSoloution3"
        Me.pboxSoloution3.Size = New System.Drawing.Size(66, 64)
        Me.pboxSoloution3.TabIndex = 73
        Me.pboxSoloution3.TabStop = False
        '
        'pboxSoloution2
        '
        Me.pboxSoloution2.BackColor = System.Drawing.Color.Black
        Me.pboxSoloution2.Location = New System.Drawing.Point(81, 15)
        Me.pboxSoloution2.Name = "pboxSoloution2"
        Me.pboxSoloution2.Size = New System.Drawing.Size(66, 64)
        Me.pboxSoloution2.TabIndex = 72
        Me.pboxSoloution2.TabStop = False
        '
        'pboxSoloution1
        '
        Me.pboxSoloution1.BackColor = System.Drawing.Color.Black
        Me.pboxSoloution1.Location = New System.Drawing.Point(9, 15)
        Me.pboxSoloution1.Name = "pboxSoloution1"
        Me.pboxSoloution1.Size = New System.Drawing.Size(66, 64)
        Me.pboxSoloution1.TabIndex = 71
        Me.pboxSoloution1.TabStop = False
        '
        'gboxAnswer
        '
        Me.gboxAnswer.BackColor = System.Drawing.Color.Transparent
        Me.gboxAnswer.Controls.Add(Me.pboxHide4)
        Me.gboxAnswer.Controls.Add(Me.pboxSoloution4)
        Me.gboxAnswer.Controls.Add(Me.pboxHide3)
        Me.gboxAnswer.Controls.Add(Me.pboxSoloution3)
        Me.gboxAnswer.Controls.Add(Me.pboxHide2)
        Me.gboxAnswer.Controls.Add(Me.pboxSoloution2)
        Me.gboxAnswer.Controls.Add(Me.pboxHide1)
        Me.gboxAnswer.Controls.Add(Me.pboxSoloution1)
        Me.gboxAnswer.ForeColor = System.Drawing.Color.White
        Me.gboxAnswer.Location = New System.Drawing.Point(121, 54)
        Me.gboxAnswer.Name = "gboxAnswer"
        Me.gboxAnswer.Size = New System.Drawing.Size(303, 87)
        Me.gboxAnswer.TabIndex = 75
        Me.gboxAnswer.TabStop = False
        Me.gboxAnswer.Text = "Solution"
        '
        'pboxHide4
        '
        Me.pboxHide4.BackColor = System.Drawing.Color.Black
        Me.pboxHide4.Location = New System.Drawing.Point(225, 15)
        Me.pboxHide4.Name = "pboxHide4"
        Me.pboxHide4.Size = New System.Drawing.Size(66, 64)
        Me.pboxHide4.TabIndex = 99
        Me.pboxHide4.TabStop = False
        '
        'pboxHide3
        '
        Me.pboxHide3.BackColor = System.Drawing.Color.Black
        Me.pboxHide3.Location = New System.Drawing.Point(153, 15)
        Me.pboxHide3.Name = "pboxHide3"
        Me.pboxHide3.Size = New System.Drawing.Size(66, 64)
        Me.pboxHide3.TabIndex = 98
        Me.pboxHide3.TabStop = False
        '
        'pboxHide2
        '
        Me.pboxHide2.BackColor = System.Drawing.Color.Black
        Me.pboxHide2.Location = New System.Drawing.Point(81, 15)
        Me.pboxHide2.Name = "pboxHide2"
        Me.pboxHide2.Size = New System.Drawing.Size(66, 64)
        Me.pboxHide2.TabIndex = 97
        Me.pboxHide2.TabStop = False
        '
        'pboxHide1
        '
        Me.pboxHide1.BackColor = System.Drawing.Color.Black
        Me.pboxHide1.Location = New System.Drawing.Point(9, 15)
        Me.pboxHide1.Name = "pboxHide1"
        Me.pboxHide1.Size = New System.Drawing.Size(66, 64)
        Me.pboxHide1.TabIndex = 75
        Me.pboxHide1.TabStop = False
        '
        'pboxRed
        '
        Me.pboxRed.BackColor = System.Drawing.Color.Red
        Me.pboxRed.Location = New System.Drawing.Point(676, 474)
        Me.pboxRed.Name = "pboxRed"
        Me.pboxRed.Size = New System.Drawing.Size(66, 64)
        Me.pboxRed.TabIndex = 71
        Me.pboxRed.TabStop = False
        '
        'pboxOrange
        '
        Me.pboxOrange.BackColor = System.Drawing.Color.Orange
        Me.pboxOrange.Location = New System.Drawing.Point(676, 153)
        Me.pboxOrange.Name = "pboxOrange"
        Me.pboxOrange.Size = New System.Drawing.Size(66, 64)
        Me.pboxOrange.TabIndex = 72
        Me.pboxOrange.TabStop = False
        '
        'pboxBlue
        '
        Me.pboxBlue.BackColor = System.Drawing.Color.Blue
        Me.pboxBlue.Location = New System.Drawing.Point(676, 233)
        Me.pboxBlue.Name = "pboxBlue"
        Me.pboxBlue.Size = New System.Drawing.Size(66, 64)
        Me.pboxBlue.TabIndex = 71
        Me.pboxBlue.TabStop = False
        '
        'pboxPurple
        '
        Me.pboxPurple.BackColor = System.Drawing.Color.Purple
        Me.pboxPurple.Location = New System.Drawing.Point(676, 313)
        Me.pboxPurple.Name = "pboxPurple"
        Me.pboxPurple.Size = New System.Drawing.Size(66, 64)
        Me.pboxPurple.TabIndex = 73
        Me.pboxPurple.TabStop = False
        '
        'pboxYellow
        '
        Me.pboxYellow.BackColor = System.Drawing.Color.Yellow
        Me.pboxYellow.Location = New System.Drawing.Point(676, 393)
        Me.pboxYellow.Name = "pboxYellow"
        Me.pboxYellow.Size = New System.Drawing.Size(66, 64)
        Me.pboxYellow.TabIndex = 75
        Me.pboxYellow.TabStop = False
        '
        'pboxGreen
        '
        Me.pboxGreen.BackColor = System.Drawing.Color.Lime
        Me.pboxGreen.Location = New System.Drawing.Point(676, 74)
        Me.pboxGreen.Name = "pboxGreen"
        Me.pboxGreen.Size = New System.Drawing.Size(66, 64)
        Me.pboxGreen.TabIndex = 74
        Me.pboxGreen.TabStop = False
        '
        'pboxColour
        '
        Me.pboxColour.BackColor = System.Drawing.Color.Black
        Me.pboxColour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pboxColour.Location = New System.Drawing.Point(665, 585)
        Me.pboxColour.Name = "pboxColour"
        Me.pboxColour.Size = New System.Drawing.Size(90, 79)
        Me.pboxColour.TabIndex = 76
        Me.pboxColour.TabStop = False
        '
        'pboxColourHolder
        '
        Me.pboxColourHolder.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Playinterface
        Me.pboxColourHolder.Location = New System.Drawing.Point(535, -3)
        Me.pboxColourHolder.Name = "pboxColourHolder"
        Me.pboxColourHolder.Size = New System.Drawing.Size(262, 752)
        Me.pboxColourHolder.TabIndex = 78
        Me.pboxColourHolder.TabStop = False
        '
        'lblRow1
        '
        Me.lblRow1.AutoSize = True
        Me.lblRow1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow1.ForeColor = System.Drawing.Color.Black
        Me.lblRow1.Location = New System.Drawing.Point(8, 454)
        Me.lblRow1.Name = "lblRow1"
        Me.lblRow1.Size = New System.Drawing.Size(16, 24)
        Me.lblRow1.TabIndex = 79
        Me.lblRow1.Text = "-"
        '
        'lblRow2
        '
        Me.lblRow2.AutoSize = True
        Me.lblRow2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow2.ForeColor = System.Drawing.Color.Black
        Me.lblRow2.Location = New System.Drawing.Point(8, 390)
        Me.lblRow2.Name = "lblRow2"
        Me.lblRow2.Size = New System.Drawing.Size(16, 24)
        Me.lblRow2.TabIndex = 80
        Me.lblRow2.Text = "-"
        '
        'lblRow3
        '
        Me.lblRow3.AutoSize = True
        Me.lblRow3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow3.ForeColor = System.Drawing.Color.Black
        Me.lblRow3.Location = New System.Drawing.Point(8, 328)
        Me.lblRow3.Name = "lblRow3"
        Me.lblRow3.Size = New System.Drawing.Size(16, 24)
        Me.lblRow3.TabIndex = 81
        Me.lblRow3.Text = "-"
        '
        'lblRow4
        '
        Me.lblRow4.AutoSize = True
        Me.lblRow4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow4.ForeColor = System.Drawing.Color.Black
        Me.lblRow4.Location = New System.Drawing.Point(8, 269)
        Me.lblRow4.Name = "lblRow4"
        Me.lblRow4.Size = New System.Drawing.Size(16, 24)
        Me.lblRow4.TabIndex = 82
        Me.lblRow4.Text = "-"
        '
        'lblRow5
        '
        Me.lblRow5.AutoSize = True
        Me.lblRow5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow5.ForeColor = System.Drawing.Color.Black
        Me.lblRow5.Location = New System.Drawing.Point(8, 207)
        Me.lblRow5.Name = "lblRow5"
        Me.lblRow5.Size = New System.Drawing.Size(16, 24)
        Me.lblRow5.TabIndex = 83
        Me.lblRow5.Text = "-"
        '
        'lblRow6
        '
        Me.lblRow6.AutoSize = True
        Me.lblRow6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow6.ForeColor = System.Drawing.Color.Black
        Me.lblRow6.Location = New System.Drawing.Point(8, 147)
        Me.lblRow6.Name = "lblRow6"
        Me.lblRow6.Size = New System.Drawing.Size(16, 24)
        Me.lblRow6.TabIndex = 84
        Me.lblRow6.Text = "-"
        '
        'lblRow7
        '
        Me.lblRow7.AutoSize = True
        Me.lblRow7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow7.ForeColor = System.Drawing.Color.Black
        Me.lblRow7.Location = New System.Drawing.Point(8, 88)
        Me.lblRow7.Name = "lblRow7"
        Me.lblRow7.Size = New System.Drawing.Size(16, 24)
        Me.lblRow7.TabIndex = 85
        Me.lblRow7.Text = "-"
        '
        'lblRow8
        '
        Me.lblRow8.AutoSize = True
        Me.lblRow8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRow8.ForeColor = System.Drawing.Color.Black
        Me.lblRow8.Location = New System.Drawing.Point(8, 25)
        Me.lblRow8.Name = "lblRow8"
        Me.lblRow8.Size = New System.Drawing.Size(16, 24)
        Me.lblRow8.TabIndex = 86
        Me.lblRow8.Text = "-"
        '
        'btnTurn1
        '
        Me.btnTurn1.ForeColor = System.Drawing.Color.Black
        Me.btnTurn1.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn1.Name = "btnTurn1"
        Me.btnTurn1.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn1.TabIndex = 87
        Me.btnTurn1.Text = "T&ake turn"
        Me.btnTurn1.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(105, 668)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 88
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(337, 668)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 96
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnHint
        '
        Me.btnHint.Location = New System.Drawing.Point(430, 94)
        Me.btnHint.Name = "btnHint"
        Me.btnHint.Size = New System.Drawing.Size(50, 23)
        Me.btnHint.TabIndex = 97
        Me.btnHint.Text = "Hint"
        Me.btnHint.UseVisualStyleBackColor = True
        '
        'gboxRow1
        '
        Me.gboxRow1.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow1.Controls.Add(Me.pboxGuess3)
        Me.gboxRow1.Controls.Add(Me.pboxGuess1)
        Me.gboxRow1.Controls.Add(Me.pboxGuess2)
        Me.gboxRow1.Controls.Add(Me.pboxGuess4)
        Me.gboxRow1.Controls.Add(Me.btnTurn1)
        Me.gboxRow1.ForeColor = System.Drawing.Color.White
        Me.gboxRow1.Location = New System.Drawing.Point(75, 585)
        Me.gboxRow1.Name = "gboxRow1"
        Me.gboxRow1.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow1.TabIndex = 98
        Me.gboxRow1.TabStop = False
        Me.gboxRow1.Text = "Row 1"
        '
        'gboxRow2
        '
        Me.gboxRow2.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow2.Controls.Add(Me.pboxGuess7)
        Me.gboxRow2.Controls.Add(Me.pboxGuess5)
        Me.gboxRow2.Controls.Add(Me.pboxGuess6)
        Me.gboxRow2.Controls.Add(Me.pboxGuess8)
        Me.gboxRow2.Controls.Add(Me.btnTurn2)
        Me.gboxRow2.ForeColor = System.Drawing.Color.White
        Me.gboxRow2.Location = New System.Drawing.Point(75, 524)
        Me.gboxRow2.Name = "gboxRow2"
        Me.gboxRow2.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow2.TabIndex = 99
        Me.gboxRow2.TabStop = False
        Me.gboxRow2.Text = "Row 2"
        '
        'pboxGuess7
        '
        Me.pboxGuess7.BackColor = System.Drawing.Color.Black
        Me.pboxGuess7.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess7.Name = "pboxGuess7"
        Me.pboxGuess7.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess7.TabIndex = 55
        Me.pboxGuess7.TabStop = False
        '
        'pboxGuess5
        '
        Me.pboxGuess5.BackColor = System.Drawing.Color.Black
        Me.pboxGuess5.Location = New System.Drawing.Point(96, 19)
        Me.pboxGuess5.Name = "pboxGuess5"
        Me.pboxGuess5.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess5.TabIndex = 39
        Me.pboxGuess5.TabStop = False
        '
        'pboxGuess6
        '
        Me.pboxGuess6.BackColor = System.Drawing.Color.Black
        Me.pboxGuess6.Location = New System.Drawing.Point(152, 19)
        Me.pboxGuess6.Name = "pboxGuess6"
        Me.pboxGuess6.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess6.TabIndex = 47
        Me.pboxGuess6.TabStop = False
        '
        'pboxGuess8
        '
        Me.pboxGuess8.BackColor = System.Drawing.Color.Black
        Me.pboxGuess8.Location = New System.Drawing.Point(264, 19)
        Me.pboxGuess8.Name = "pboxGuess8"
        Me.pboxGuess8.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess8.TabIndex = 63
        Me.pboxGuess8.TabStop = False
        '
        'btnTurn2
        '
        Me.btnTurn2.ForeColor = System.Drawing.Color.Black
        Me.btnTurn2.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn2.Name = "btnTurn2"
        Me.btnTurn2.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn2.TabIndex = 87
        Me.btnTurn2.Text = "T&ake turn"
        Me.btnTurn2.UseVisualStyleBackColor = True
        '
        'gboxRow3
        '
        Me.gboxRow3.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow3.Controls.Add(Me.pboxGuess12)
        Me.gboxRow3.Controls.Add(Me.pboxGuess11)
        Me.gboxRow3.Controls.Add(Me.pboxGuess10)
        Me.gboxRow3.Controls.Add(Me.pboxGuess9)
        Me.gboxRow3.Controls.Add(Me.btnTurn3)
        Me.gboxRow3.ForeColor = System.Drawing.Color.White
        Me.gboxRow3.Location = New System.Drawing.Point(75, 464)
        Me.gboxRow3.Name = "gboxRow3"
        Me.gboxRow3.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow3.TabIndex = 100
        Me.gboxRow3.TabStop = False
        Me.gboxRow3.Text = "Row 3"
        '
        'pboxGuess12
        '
        Me.pboxGuess12.BackColor = System.Drawing.Color.Black
        Me.pboxGuess12.Location = New System.Drawing.Point(262, 18)
        Me.pboxGuess12.Name = "pboxGuess12"
        Me.pboxGuess12.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess12.TabIndex = 39
        Me.pboxGuess12.TabStop = False
        '
        'pboxGuess11
        '
        Me.pboxGuess11.BackColor = System.Drawing.Color.Black
        Me.pboxGuess11.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess11.Name = "pboxGuess11"
        Me.pboxGuess11.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess11.TabIndex = 39
        Me.pboxGuess11.TabStop = False
        '
        'pboxGuess10
        '
        Me.pboxGuess10.BackColor = System.Drawing.Color.Black
        Me.pboxGuess10.Location = New System.Drawing.Point(152, 18)
        Me.pboxGuess10.Name = "pboxGuess10"
        Me.pboxGuess10.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess10.TabIndex = 39
        Me.pboxGuess10.TabStop = False
        '
        'pboxGuess9
        '
        Me.pboxGuess9.BackColor = System.Drawing.Color.Black
        Me.pboxGuess9.Location = New System.Drawing.Point(96, 19)
        Me.pboxGuess9.Name = "pboxGuess9"
        Me.pboxGuess9.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess9.TabIndex = 39
        Me.pboxGuess9.TabStop = False
        '
        'btnTurn3
        '
        Me.btnTurn3.ForeColor = System.Drawing.Color.Black
        Me.btnTurn3.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn3.Name = "btnTurn3"
        Me.btnTurn3.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn3.TabIndex = 87
        Me.btnTurn3.Text = "T&ake turn"
        Me.btnTurn3.UseVisualStyleBackColor = True
        '
        'gboxRow4
        '
        Me.gboxRow4.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow4.Controls.Add(Me.pboxGuess16)
        Me.gboxRow4.Controls.Add(Me.pboxGuess15)
        Me.gboxRow4.Controls.Add(Me.pboxGuess14)
        Me.gboxRow4.Controls.Add(Me.pboxGuess13)
        Me.gboxRow4.Controls.Add(Me.btnTurn4)
        Me.gboxRow4.ForeColor = System.Drawing.Color.White
        Me.gboxRow4.Location = New System.Drawing.Point(75, 403)
        Me.gboxRow4.Name = "gboxRow4"
        Me.gboxRow4.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow4.TabIndex = 101
        Me.gboxRow4.TabStop = False
        Me.gboxRow4.Text = "Row 4"
        '
        'pboxGuess16
        '
        Me.pboxGuess16.BackColor = System.Drawing.Color.Black
        Me.pboxGuess16.Location = New System.Drawing.Point(262, 18)
        Me.pboxGuess16.Name = "pboxGuess16"
        Me.pboxGuess16.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess16.TabIndex = 104
        Me.pboxGuess16.TabStop = False
        '
        'pboxGuess15
        '
        Me.pboxGuess15.BackColor = System.Drawing.Color.Black
        Me.pboxGuess15.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess15.Name = "pboxGuess15"
        Me.pboxGuess15.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess15.TabIndex = 105
        Me.pboxGuess15.TabStop = False
        '
        'pboxGuess14
        '
        Me.pboxGuess14.BackColor = System.Drawing.Color.Black
        Me.pboxGuess14.Location = New System.Drawing.Point(152, 18)
        Me.pboxGuess14.Name = "pboxGuess14"
        Me.pboxGuess14.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess14.TabIndex = 39
        Me.pboxGuess14.TabStop = False
        '
        'pboxGuess13
        '
        Me.pboxGuess13.BackColor = System.Drawing.Color.Black
        Me.pboxGuess13.Location = New System.Drawing.Point(96, 18)
        Me.pboxGuess13.Name = "pboxGuess13"
        Me.pboxGuess13.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess13.TabIndex = 39
        Me.pboxGuess13.TabStop = False
        '
        'btnTurn4
        '
        Me.btnTurn4.ForeColor = System.Drawing.Color.Black
        Me.btnTurn4.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn4.Name = "btnTurn4"
        Me.btnTurn4.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn4.TabIndex = 87
        Me.btnTurn4.Text = "T&ake turn"
        Me.btnTurn4.UseVisualStyleBackColor = True
        '
        'gboxRow5
        '
        Me.gboxRow5.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow5.Controls.Add(Me.pboxGuess20)
        Me.gboxRow5.Controls.Add(Me.btnTurn5)
        Me.gboxRow5.Controls.Add(Me.pboxGuess19)
        Me.gboxRow5.Controls.Add(Me.pboxGuess18)
        Me.gboxRow5.Controls.Add(Me.pboxGuess17)
        Me.gboxRow5.ForeColor = System.Drawing.Color.White
        Me.gboxRow5.Location = New System.Drawing.Point(75, 342)
        Me.gboxRow5.Name = "gboxRow5"
        Me.gboxRow5.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow5.TabIndex = 102
        Me.gboxRow5.TabStop = False
        Me.gboxRow5.Text = "Row 5"
        '
        'pboxGuess20
        '
        Me.pboxGuess20.BackColor = System.Drawing.Color.Black
        Me.pboxGuess20.Location = New System.Drawing.Point(262, 18)
        Me.pboxGuess20.Name = "pboxGuess20"
        Me.pboxGuess20.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess20.TabIndex = 90
        Me.pboxGuess20.TabStop = False
        '
        'btnTurn5
        '
        Me.btnTurn5.ForeColor = System.Drawing.Color.Black
        Me.btnTurn5.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn5.Name = "btnTurn5"
        Me.btnTurn5.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn5.TabIndex = 87
        Me.btnTurn5.Text = "T&ake turn"
        Me.btnTurn5.UseVisualStyleBackColor = True
        '
        'pboxGuess19
        '
        Me.pboxGuess19.BackColor = System.Drawing.Color.Black
        Me.pboxGuess19.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess19.Name = "pboxGuess19"
        Me.pboxGuess19.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess19.TabIndex = 91
        Me.pboxGuess19.TabStop = False
        '
        'pboxGuess18
        '
        Me.pboxGuess18.BackColor = System.Drawing.Color.Black
        Me.pboxGuess18.Location = New System.Drawing.Point(152, 18)
        Me.pboxGuess18.Name = "pboxGuess18"
        Me.pboxGuess18.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess18.TabIndex = 88
        Me.pboxGuess18.TabStop = False
        '
        'pboxGuess17
        '
        Me.pboxGuess17.BackColor = System.Drawing.Color.Black
        Me.pboxGuess17.Location = New System.Drawing.Point(96, 19)
        Me.pboxGuess17.Name = "pboxGuess17"
        Me.pboxGuess17.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess17.TabIndex = 89
        Me.pboxGuess17.TabStop = False
        '
        'gboxRow6
        '
        Me.gboxRow6.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow6.Controls.Add(Me.pboxGuess24)
        Me.gboxRow6.Controls.Add(Me.btnTurn6)
        Me.gboxRow6.Controls.Add(Me.pboxGuess23)
        Me.gboxRow6.Controls.Add(Me.pboxGuess22)
        Me.gboxRow6.Controls.Add(Me.pboxGuess21)
        Me.gboxRow6.ForeColor = System.Drawing.Color.White
        Me.gboxRow6.Location = New System.Drawing.Point(75, 282)
        Me.gboxRow6.Name = "gboxRow6"
        Me.gboxRow6.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow6.TabIndex = 103
        Me.gboxRow6.TabStop = False
        Me.gboxRow6.Text = "Row 6"
        '
        'pboxGuess24
        '
        Me.pboxGuess24.BackColor = System.Drawing.Color.Black
        Me.pboxGuess24.Location = New System.Drawing.Point(262, 17)
        Me.pboxGuess24.Name = "pboxGuess24"
        Me.pboxGuess24.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess24.TabIndex = 94
        Me.pboxGuess24.TabStop = False
        '
        'btnTurn6
        '
        Me.btnTurn6.ForeColor = System.Drawing.Color.Black
        Me.btnTurn6.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn6.Name = "btnTurn6"
        Me.btnTurn6.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn6.TabIndex = 87
        Me.btnTurn6.Text = "T&ake turn"
        Me.btnTurn6.UseVisualStyleBackColor = True
        '
        'pboxGuess23
        '
        Me.pboxGuess23.BackColor = System.Drawing.Color.Black
        Me.pboxGuess23.Location = New System.Drawing.Point(208, 18)
        Me.pboxGuess23.Name = "pboxGuess23"
        Me.pboxGuess23.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess23.TabIndex = 95
        Me.pboxGuess23.TabStop = False
        '
        'pboxGuess22
        '
        Me.pboxGuess22.BackColor = System.Drawing.Color.Black
        Me.pboxGuess22.Location = New System.Drawing.Point(152, 17)
        Me.pboxGuess22.Name = "pboxGuess22"
        Me.pboxGuess22.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess22.TabIndex = 92
        Me.pboxGuess22.TabStop = False
        '
        'pboxGuess21
        '
        Me.pboxGuess21.BackColor = System.Drawing.Color.Black
        Me.pboxGuess21.Location = New System.Drawing.Point(96, 18)
        Me.pboxGuess21.Name = "pboxGuess21"
        Me.pboxGuess21.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess21.TabIndex = 93
        Me.pboxGuess21.TabStop = False
        '
        'gboxRow7
        '
        Me.gboxRow7.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow7.Controls.Add(Me.pboxGuess28)
        Me.gboxRow7.Controls.Add(Me.btnTurn7)
        Me.gboxRow7.Controls.Add(Me.pboxGuess27)
        Me.gboxRow7.Controls.Add(Me.pboxGuess26)
        Me.gboxRow7.Controls.Add(Me.pboxGuess25)
        Me.gboxRow7.ForeColor = System.Drawing.Color.White
        Me.gboxRow7.Location = New System.Drawing.Point(75, 221)
        Me.gboxRow7.Name = "gboxRow7"
        Me.gboxRow7.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow7.TabIndex = 104
        Me.gboxRow7.TabStop = False
        Me.gboxRow7.Text = "Row 7"
        '
        'pboxGuess28
        '
        Me.pboxGuess28.BackColor = System.Drawing.Color.Black
        Me.pboxGuess28.Location = New System.Drawing.Point(262, 17)
        Me.pboxGuess28.Name = "pboxGuess28"
        Me.pboxGuess28.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess28.TabIndex = 98
        Me.pboxGuess28.TabStop = False
        '
        'btnTurn7
        '
        Me.btnTurn7.ForeColor = System.Drawing.Color.Black
        Me.btnTurn7.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn7.Name = "btnTurn7"
        Me.btnTurn7.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn7.TabIndex = 87
        Me.btnTurn7.Text = "T&ake turn"
        Me.btnTurn7.UseVisualStyleBackColor = True
        '
        'pboxGuess27
        '
        Me.pboxGuess27.BackColor = System.Drawing.Color.Black
        Me.pboxGuess27.Location = New System.Drawing.Point(208, 18)
        Me.pboxGuess27.Name = "pboxGuess27"
        Me.pboxGuess27.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess27.TabIndex = 99
        Me.pboxGuess27.TabStop = False
        '
        'pboxGuess26
        '
        Me.pboxGuess26.BackColor = System.Drawing.Color.Black
        Me.pboxGuess26.Location = New System.Drawing.Point(152, 17)
        Me.pboxGuess26.Name = "pboxGuess26"
        Me.pboxGuess26.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess26.TabIndex = 96
        Me.pboxGuess26.TabStop = False
        '
        'pboxGuess25
        '
        Me.pboxGuess25.BackColor = System.Drawing.Color.Black
        Me.pboxGuess25.Location = New System.Drawing.Point(96, 18)
        Me.pboxGuess25.Name = "pboxGuess25"
        Me.pboxGuess25.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess25.TabIndex = 97
        Me.pboxGuess25.TabStop = False
        '
        'gboxRow8
        '
        Me.gboxRow8.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow8.Controls.Add(Me.pboxGuess32)
        Me.gboxRow8.Controls.Add(Me.btnTurn8)
        Me.gboxRow8.Controls.Add(Me.pboxGuess31)
        Me.gboxRow8.Controls.Add(Me.pboxGuess30)
        Me.gboxRow8.Controls.Add(Me.pboxGuess29)
        Me.gboxRow8.ForeColor = System.Drawing.Color.White
        Me.gboxRow8.Location = New System.Drawing.Point(75, 160)
        Me.gboxRow8.Name = "gboxRow8"
        Me.gboxRow8.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow8.TabIndex = 105
        Me.gboxRow8.TabStop = False
        Me.gboxRow8.Text = "Made by Mason Teitzel"
        '
        'pboxGuess32
        '
        Me.pboxGuess32.BackColor = System.Drawing.Color.Black
        Me.pboxGuess32.Location = New System.Drawing.Point(262, 17)
        Me.pboxGuess32.Name = "pboxGuess32"
        Me.pboxGuess32.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess32.TabIndex = 102
        Me.pboxGuess32.TabStop = False
        '
        'btnTurn8
        '
        Me.btnTurn8.ForeColor = System.Drawing.Color.Black
        Me.btnTurn8.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn8.Name = "btnTurn8"
        Me.btnTurn8.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn8.TabIndex = 87
        Me.btnTurn8.Text = "T&ake turn"
        Me.btnTurn8.UseVisualStyleBackColor = True
        '
        'pboxGuess31
        '
        Me.pboxGuess31.BackColor = System.Drawing.Color.Black
        Me.pboxGuess31.Location = New System.Drawing.Point(208, 18)
        Me.pboxGuess31.Name = "pboxGuess31"
        Me.pboxGuess31.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess31.TabIndex = 103
        Me.pboxGuess31.TabStop = False
        '
        'pboxGuess30
        '
        Me.pboxGuess30.BackColor = System.Drawing.Color.Black
        Me.pboxGuess30.Location = New System.Drawing.Point(152, 17)
        Me.pboxGuess30.Name = "pboxGuess30"
        Me.pboxGuess30.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess30.TabIndex = 100
        Me.pboxGuess30.TabStop = False
        '
        'pboxGuess29
        '
        Me.pboxGuess29.BackColor = System.Drawing.Color.Black
        Me.pboxGuess29.Location = New System.Drawing.Point(96, 18)
        Me.pboxGuess29.Name = "pboxGuess29"
        Me.pboxGuess29.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess29.TabIndex = 101
        Me.pboxGuess29.TabStop = False
        '
        'btnDev
        '
        Me.btnDev.Location = New System.Drawing.Point(661, 17)
        Me.btnDev.Name = "btnDev"
        Me.btnDev.Size = New System.Drawing.Size(117, 23)
        Me.btnDev.TabIndex = 106
        Me.btnDev.Text = "Developer Mode"
        Me.btnDev.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(793, 24)
        Me.MenuStrip1.TabIndex = 107
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewGameToolStripMenuItem, Me.SaveGameToolStripMenuItem, Me.LoadGameToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'NewGameToolStripMenuItem
        '
        Me.NewGameToolStripMenuItem.Name = "NewGameToolStripMenuItem"
        Me.NewGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.NewGameToolStripMenuItem.Text = "New Game"
        '
        'SaveGameToolStripMenuItem
        '
        Me.SaveGameToolStripMenuItem.Name = "SaveGameToolStripMenuItem"
        Me.SaveGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveGameToolStripMenuItem.Text = "Save Game"
        '
        'LoadGameToolStripMenuItem
        '
        Me.LoadGameToolStripMenuItem.Name = "LoadGameToolStripMenuItem"
        Me.LoadGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LoadGameToolStripMenuItem.Text = "Load Game"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TutorialToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'TutorialToolStripMenuItem
        '
        Me.TutorialToolStripMenuItem.Name = "TutorialToolStripMenuItem"
        Me.TutorialToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TutorialToolStripMenuItem.Text = "Tutorial"
        '
        'SaveFileDialog1
        '
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'lblBlack1
        '
        Me.lblBlack1.AutoSize = True
        Me.lblBlack1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack1.Location = New System.Drawing.Point(49, 454)
        Me.lblBlack1.Name = "lblBlack1"
        Me.lblBlack1.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack1.TabIndex = 108
        Me.lblBlack1.Text = "-"
        '
        'lblBlack2
        '
        Me.lblBlack2.AutoSize = True
        Me.lblBlack2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack2.Location = New System.Drawing.Point(49, 390)
        Me.lblBlack2.Name = "lblBlack2"
        Me.lblBlack2.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack2.TabIndex = 109
        Me.lblBlack2.Text = "-"
        '
        'lblBlack3
        '
        Me.lblBlack3.AutoSize = True
        Me.lblBlack3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack3.Location = New System.Drawing.Point(49, 328)
        Me.lblBlack3.Name = "lblBlack3"
        Me.lblBlack3.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack3.TabIndex = 110
        Me.lblBlack3.Text = "-"
        '
        'lblBlack6
        '
        Me.lblBlack6.AutoSize = True
        Me.lblBlack6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack6.Location = New System.Drawing.Point(49, 147)
        Me.lblBlack6.Name = "lblBlack6"
        Me.lblBlack6.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack6.TabIndex = 113
        Me.lblBlack6.Text = "-"
        '
        'lblBlack5
        '
        Me.lblBlack5.AutoSize = True
        Me.lblBlack5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack5.Location = New System.Drawing.Point(49, 207)
        Me.lblBlack5.Name = "lblBlack5"
        Me.lblBlack5.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack5.TabIndex = 112
        Me.lblBlack5.Text = "-"
        '
        'lblBlack4
        '
        Me.lblBlack4.AutoSize = True
        Me.lblBlack4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack4.Location = New System.Drawing.Point(49, 269)
        Me.lblBlack4.Name = "lblBlack4"
        Me.lblBlack4.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack4.TabIndex = 111
        Me.lblBlack4.Text = "-"
        '
        'lblBlack7
        '
        Me.lblBlack7.AutoSize = True
        Me.lblBlack7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack7.Location = New System.Drawing.Point(49, 88)
        Me.lblBlack7.Name = "lblBlack7"
        Me.lblBlack7.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack7.TabIndex = 114
        Me.lblBlack7.Text = "-"
        '
        'lblBlack8
        '
        Me.lblBlack8.AutoSize = True
        Me.lblBlack8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlack8.Location = New System.Drawing.Point(49, 25)
        Me.lblBlack8.Name = "lblBlack8"
        Me.lblBlack8.Size = New System.Drawing.Size(16, 24)
        Me.lblBlack8.TabIndex = 115
        Me.lblBlack8.Text = "-"
        '
        'gboxHints
        '
        Me.gboxHints.BackColor = System.Drawing.Color.Transparent
        Me.gboxHints.Controls.Add(Me.lblBlack5)
        Me.gboxHints.Controls.Add(Me.lblBlack8)
        Me.gboxHints.Controls.Add(Me.lblRow1)
        Me.gboxHints.Controls.Add(Me.lblBlack7)
        Me.gboxHints.Controls.Add(Me.lblRow2)
        Me.gboxHints.Controls.Add(Me.lblBlack6)
        Me.gboxHints.Controls.Add(Me.lblRow3)
        Me.gboxHints.Controls.Add(Me.lblRow4)
        Me.gboxHints.Controls.Add(Me.lblBlack4)
        Me.gboxHints.Controls.Add(Me.lblRow5)
        Me.gboxHints.Controls.Add(Me.lblBlack3)
        Me.gboxHints.Controls.Add(Me.lblRow6)
        Me.gboxHints.Controls.Add(Me.lblBlack2)
        Me.gboxHints.Controls.Add(Me.lblRow7)
        Me.gboxHints.Controls.Add(Me.lblBlack1)
        Me.gboxHints.Controls.Add(Me.lblRow8)
        Me.gboxHints.ForeColor = System.Drawing.Color.White
        Me.gboxHints.Location = New System.Drawing.Point(409, 160)
        Me.gboxHints.Name = "gboxHints"
        Me.gboxHints.Size = New System.Drawing.Size(71, 499)
        Me.gboxHints.TabIndex = 116
        Me.gboxHints.TabStop = False
        Me.gboxHints.Text = "Hints"
        '
        'frmMastermind
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Playinterface3
        Me.ClientSize = New System.Drawing.Size(793, 747)
        Me.Controls.Add(Me.gboxHints)
        Me.Controls.Add(Me.btnDev)
        Me.Controls.Add(Me.gboxRow8)
        Me.Controls.Add(Me.gboxRow7)
        Me.Controls.Add(Me.gboxRow6)
        Me.Controls.Add(Me.gboxRow5)
        Me.Controls.Add(Me.gboxRow4)
        Me.Controls.Add(Me.gboxRow3)
        Me.Controls.Add(Me.gboxRow2)
        Me.Controls.Add(Me.gboxRow1)
        Me.Controls.Add(Me.btnHint)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.pboxColour)
        Me.Controls.Add(Me.pboxGreen)
        Me.Controls.Add(Me.pboxYellow)
        Me.Controls.Add(Me.pboxPurple)
        Me.Controls.Add(Me.pboxBlue)
        Me.Controls.Add(Me.pboxOrange)
        Me.Controls.Add(Me.pboxRed)
        Me.Controls.Add(Me.gboxAnswer)
        Me.Controls.Add(Me.pboxColourHolder)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMastermind"
        Me.Text = "Mastermind"
        CType(Me.pboxGuess1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxSoloution4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxSoloution3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxSoloution2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxSoloution1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxAnswer.ResumeLayout(False)
        CType(Me.pboxHide4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxHide3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxHide2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxHide1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxOrange, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxBlue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxPurple, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxYellow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxColour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxColourHolder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow1.ResumeLayout(False)
        Me.gboxRow2.ResumeLayout(False)
        CType(Me.pboxGuess7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow3.ResumeLayout(False)
        CType(Me.pboxGuess12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow4.ResumeLayout(False)
        CType(Me.pboxGuess16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow5.ResumeLayout(False)
        CType(Me.pboxGuess20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow6.ResumeLayout(False)
        CType(Me.pboxGuess24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow7.ResumeLayout(False)
        CType(Me.pboxGuess28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow8.ResumeLayout(False)
        CType(Me.pboxGuess32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess29, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gboxHints.ResumeLayout(False)
        Me.gboxHints.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub


    Friend WithEvents pboxGuess1 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess2 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess3 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess4 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxSoloution4 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxSoloution3 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxSoloution2 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxSoloution1 As System.Windows.Forms.PictureBox
    Friend WithEvents gboxAnswer As System.Windows.Forms.GroupBox
    Friend WithEvents pboxRed As System.Windows.Forms.PictureBox
    Friend WithEvents pboxOrange As System.Windows.Forms.PictureBox
    Friend WithEvents pboxBlue As System.Windows.Forms.PictureBox
    Friend WithEvents pboxPurple As System.Windows.Forms.PictureBox
    Friend WithEvents pboxYellow As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGreen As System.Windows.Forms.PictureBox
    Friend WithEvents pboxColour As System.Windows.Forms.PictureBox
    Friend WithEvents pboxColourHolder As System.Windows.Forms.PictureBox
    Friend WithEvents lblRow1 As System.Windows.Forms.Label
    Friend WithEvents lblRow2 As System.Windows.Forms.Label
    Friend WithEvents lblRow3 As System.Windows.Forms.Label
    Friend WithEvents lblRow4 As System.Windows.Forms.Label
    Friend WithEvents lblRow5 As System.Windows.Forms.Label
    Friend WithEvents lblRow6 As System.Windows.Forms.Label
    Friend WithEvents lblRow7 As System.Windows.Forms.Label
    Friend WithEvents lblRow8 As System.Windows.Forms.Label
    Friend WithEvents btnTurn1 As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents pboxHide1 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxHide2 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxHide3 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxHide4 As System.Windows.Forms.PictureBox
    Friend WithEvents btnHint As System.Windows.Forms.Button
    Friend WithEvents gboxRow1 As System.Windows.Forms.GroupBox
    Friend WithEvents gboxRow2 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess7 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess5 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess6 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess8 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn2 As System.Windows.Forms.Button
    Friend WithEvents gboxRow3 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess9 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn3 As System.Windows.Forms.Button
    Friend WithEvents gboxRow4 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess10 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn4 As System.Windows.Forms.Button
    Friend WithEvents gboxRow5 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess11 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn5 As System.Windows.Forms.Button
    Friend WithEvents gboxRow6 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess12 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn6 As System.Windows.Forms.Button
    Friend WithEvents gboxRow7 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess13 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn7 As System.Windows.Forms.Button
    Friend WithEvents gboxRow8 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess14 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn8 As System.Windows.Forms.Button
    Friend WithEvents pboxGuess16 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess15 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess20 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess19 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess18 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess17 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess24 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess23 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess22 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess21 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess28 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess27 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess26 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess25 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess32 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess31 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess30 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess29 As System.Windows.Forms.PictureBox
    Friend WithEvents btnDev As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TutorialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblBlack1 As System.Windows.Forms.Label
    Friend WithEvents lblBlack2 As System.Windows.Forms.Label
    Friend WithEvents lblBlack3 As System.Windows.Forms.Label
    Friend WithEvents lblBlack6 As System.Windows.Forms.Label
    Friend WithEvents lblBlack5 As System.Windows.Forms.Label
    Friend WithEvents lblBlack4 As System.Windows.Forms.Label
    Friend WithEvents lblBlack7 As System.Windows.Forms.Label
    Friend WithEvents lblBlack8 As System.Windows.Forms.Label
    Friend WithEvents gboxHints As System.Windows.Forms.GroupBox
End Class
