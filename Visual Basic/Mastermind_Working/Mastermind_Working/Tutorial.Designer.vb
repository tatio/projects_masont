<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTutorial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTutorial))
        Me.tconTutorial = New System.Windows.Forms.TabControl
        Me.tpColour = New System.Windows.Forms.TabPage
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.lblStep2 = New System.Windows.Forms.Label
        Me.lblStep1 = New System.Windows.Forms.Label
        Me.tboxSelect = New System.Windows.Forms.TextBox
        Me.tboxColour = New System.Windows.Forms.TextBox
        Me.gboxColour = New System.Windows.Forms.GroupBox
        Me.pboxGreen = New System.Windows.Forms.PictureBox
        Me.pboxColour = New System.Windows.Forms.PictureBox
        Me.pboxRed = New System.Windows.Forms.PictureBox
        Me.pboxOrange = New System.Windows.Forms.PictureBox
        Me.pboxYellow = New System.Windows.Forms.PictureBox
        Me.pboxBlue = New System.Windows.Forms.PictureBox
        Me.pboxPurple = New System.Windows.Forms.PictureBox
        Me.tpManage = New System.Windows.Forms.TabPage
        Me.tboxNewgame = New System.Windows.Forms.TextBox
        Me.tboxUse = New System.Windows.Forms.TextBox
        Me.tboxSaveLoad = New System.Windows.Forms.TextBox
        Me.pboxSaveload = New System.Windows.Forms.PictureBox
        Me.tpGuesses = New System.Windows.Forms.TabPage
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTip1 = New System.Windows.Forms.Label
        Me.lblTip2 = New System.Windows.Forms.Label
        Me.tboxGuess = New System.Windows.Forms.TextBox
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.pboxColour2 = New System.Windows.Forms.PictureBox
        Me.gboxRow1 = New System.Windows.Forms.GroupBox
        Me.pboxGuess3 = New System.Windows.Forms.PictureBox
        Me.pboxGuess1 = New System.Windows.Forms.PictureBox
        Me.pboxGuess2 = New System.Windows.Forms.PictureBox
        Me.pboxGuess4 = New System.Windows.Forms.PictureBox
        Me.btnTurn1 = New System.Windows.Forms.Button
        Me.tpSubmit = New System.Windows.Forms.TabPage
        Me.lblTip4 = New System.Windows.Forms.Label
        Me.lblTip3 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.pboxWin = New System.Windows.Forms.PictureBox
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnPlay = New System.Windows.Forms.Button
        Me.tconTutorial.SuspendLayout()
        Me.tpColour.SuspendLayout()
        Me.gboxColour.SuspendLayout()
        CType(Me.pboxGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxColour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxOrange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxYellow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxPurple, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpManage.SuspendLayout()
        CType(Me.pboxSaveload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpGuesses.SuspendLayout()
        CType(Me.pboxColour2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxRow1.SuspendLayout()
        CType(Me.pboxGuess3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboxGuess4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpSubmit.SuspendLayout()
        CType(Me.pboxWin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tconTutorial
        '
        Me.tconTutorial.Controls.Add(Me.tpColour)
        Me.tconTutorial.Controls.Add(Me.tpManage)
        Me.tconTutorial.Controls.Add(Me.tpGuesses)
        Me.tconTutorial.Controls.Add(Me.tpSubmit)
        Me.tconTutorial.Location = New System.Drawing.Point(103, 104)
        Me.tconTutorial.Name = "tconTutorial"
        Me.tconTutorial.SelectedIndex = 0
        Me.tconTutorial.Size = New System.Drawing.Size(362, 534)
        Me.tconTutorial.TabIndex = 0
        '
        'tpColour
        '
        Me.tpColour.Controls.Add(Me.TextBox1)
        Me.tpColour.Controls.Add(Me.lblStep2)
        Me.tpColour.Controls.Add(Me.lblStep1)
        Me.tpColour.Controls.Add(Me.tboxSelect)
        Me.tpColour.Controls.Add(Me.tboxColour)
        Me.tpColour.Controls.Add(Me.gboxColour)
        Me.tpColour.Location = New System.Drawing.Point(4, 22)
        Me.tpColour.Name = "tpColour"
        Me.tpColour.Padding = New System.Windows.Forms.Padding(3)
        Me.tpColour.Size = New System.Drawing.Size(354, 508)
        Me.tpColour.TabIndex = 0
        Me.tpColour.Text = "Select a colour"
        Me.tpColour.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TextBox1.Location = New System.Drawing.Point(100, 417)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(126, 39)
        Me.TextBox1.TabIndex = 89
        Me.TextBox1.Text = "This is your selected colour ---->"
        '
        'lblStep2
        '
        Me.lblStep2.AutoSize = True
        Me.lblStep2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2.Location = New System.Drawing.Point(8, 424)
        Me.lblStep2.Name = "lblStep2"
        Me.lblStep2.Size = New System.Drawing.Size(75, 24)
        Me.lblStep2.TabIndex = 88
        Me.lblStep2.Text = "Step 2."
        '
        'lblStep1
        '
        Me.lblStep1.AutoSize = True
        Me.lblStep1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1.Location = New System.Drawing.Point(12, 228)
        Me.lblStep1.Name = "lblStep1"
        Me.lblStep1.Size = New System.Drawing.Size(75, 24)
        Me.lblStep1.TabIndex = 87
        Me.lblStep1.Text = "Step 1."
        '
        'tboxSelect
        '
        Me.tboxSelect.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxSelect.Location = New System.Drawing.Point(100, 233)
        Me.tboxSelect.Multiline = True
        Me.tboxSelect.Name = "tboxSelect"
        Me.tboxSelect.ReadOnly = True
        Me.tboxSelect.Size = New System.Drawing.Size(100, 23)
        Me.tboxSelect.TabIndex = 86
        Me.tboxSelect.Text = "Select a colour --->"
        '
        'tboxColour
        '
        Me.tboxColour.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxColour.Location = New System.Drawing.Point(12, 23)
        Me.tboxColour.Multiline = True
        Me.tboxColour.Name = "tboxColour"
        Me.tboxColour.ReadOnly = True
        Me.tboxColour.Size = New System.Drawing.Size(212, 78)
        Me.tboxColour.TabIndex = 85
        Me.tboxColour.Text = "Select a colour to the right that you wish to submit, simply by clicking on the c" & _
            "olour. The larger box below the colours indicates the selected colour and has bl" & _
            "ack as a default."
        '
        'gboxColour
        '
        Me.gboxColour.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.gboxColour.Controls.Add(Me.pboxGreen)
        Me.gboxColour.Controls.Add(Me.pboxColour)
        Me.gboxColour.Controls.Add(Me.pboxRed)
        Me.gboxColour.Controls.Add(Me.pboxOrange)
        Me.gboxColour.Controls.Add(Me.pboxYellow)
        Me.gboxColour.Controls.Add(Me.pboxBlue)
        Me.gboxColour.Controls.Add(Me.pboxPurple)
        Me.gboxColour.ForeColor = System.Drawing.Color.Black
        Me.gboxColour.Location = New System.Drawing.Point(236, 23)
        Me.gboxColour.Name = "gboxColour"
        Me.gboxColour.Size = New System.Drawing.Size(91, 455)
        Me.gboxColour.TabIndex = 84
        Me.gboxColour.TabStop = False
        Me.gboxColour.Text = "Colours"
        '
        'pboxGreen
        '
        Me.pboxGreen.BackColor = System.Drawing.Color.Lime
        Me.pboxGreen.Location = New System.Drawing.Point(21, 19)
        Me.pboxGreen.Name = "pboxGreen"
        Me.pboxGreen.Size = New System.Drawing.Size(48, 49)
        Me.pboxGreen.TabIndex = 81
        Me.pboxGreen.TabStop = False
        '
        'pboxColour
        '
        Me.pboxColour.BackColor = System.Drawing.Color.Black
        Me.pboxColour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pboxColour.Location = New System.Drawing.Point(13, 377)
        Me.pboxColour.Name = "pboxColour"
        Me.pboxColour.Size = New System.Drawing.Size(66, 64)
        Me.pboxColour.TabIndex = 83
        Me.pboxColour.TabStop = False
        '
        'pboxRed
        '
        Me.pboxRed.BackColor = System.Drawing.Color.Red
        Me.pboxRed.Location = New System.Drawing.Point(21, 294)
        Me.pboxRed.Name = "pboxRed"
        Me.pboxRed.Size = New System.Drawing.Size(48, 49)
        Me.pboxRed.TabIndex = 78
        Me.pboxRed.TabStop = False
        '
        'pboxOrange
        '
        Me.pboxOrange.BackColor = System.Drawing.Color.Orange
        Me.pboxOrange.Location = New System.Drawing.Point(21, 74)
        Me.pboxOrange.Name = "pboxOrange"
        Me.pboxOrange.Size = New System.Drawing.Size(48, 49)
        Me.pboxOrange.TabIndex = 79
        Me.pboxOrange.TabStop = False
        '
        'pboxYellow
        '
        Me.pboxYellow.BackColor = System.Drawing.Color.Yellow
        Me.pboxYellow.Location = New System.Drawing.Point(21, 239)
        Me.pboxYellow.Name = "pboxYellow"
        Me.pboxYellow.Size = New System.Drawing.Size(48, 49)
        Me.pboxYellow.TabIndex = 82
        Me.pboxYellow.TabStop = False
        '
        'pboxBlue
        '
        Me.pboxBlue.BackColor = System.Drawing.Color.Blue
        Me.pboxBlue.Location = New System.Drawing.Point(21, 129)
        Me.pboxBlue.Name = "pboxBlue"
        Me.pboxBlue.Size = New System.Drawing.Size(48, 49)
        Me.pboxBlue.TabIndex = 77
        Me.pboxBlue.TabStop = False
        '
        'pboxPurple
        '
        Me.pboxPurple.BackColor = System.Drawing.Color.Purple
        Me.pboxPurple.Location = New System.Drawing.Point(21, 184)
        Me.pboxPurple.Name = "pboxPurple"
        Me.pboxPurple.Size = New System.Drawing.Size(48, 49)
        Me.pboxPurple.TabIndex = 80
        Me.pboxPurple.TabStop = False
        '
        'tpManage
        '
        Me.tpManage.Controls.Add(Me.tboxNewgame)
        Me.tpManage.Controls.Add(Me.tboxUse)
        Me.tpManage.Controls.Add(Me.tboxSaveLoad)
        Me.tpManage.Controls.Add(Me.pboxSaveload)
        Me.tpManage.Location = New System.Drawing.Point(4, 22)
        Me.tpManage.Name = "tpManage"
        Me.tpManage.Padding = New System.Windows.Forms.Padding(3)
        Me.tpManage.Size = New System.Drawing.Size(354, 508)
        Me.tpManage.TabIndex = 1
        Me.tpManage.Text = "Save / Load"
        Me.tpManage.UseVisualStyleBackColor = True
        '
        'tboxNewgame
        '
        Me.tboxNewgame.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxNewgame.Location = New System.Drawing.Point(88, 348)
        Me.tboxNewgame.Multiline = True
        Me.tboxNewgame.Name = "tboxNewgame"
        Me.tboxNewgame.ReadOnly = True
        Me.tboxNewgame.Size = New System.Drawing.Size(171, 57)
        Me.tboxNewgame.TabIndex = 88
        Me.tboxNewgame.Text = "The New Game function will start a new game if you wish."
        '
        'tboxUse
        '
        Me.tboxUse.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxUse.Location = New System.Drawing.Point(88, 285)
        Me.tboxUse.Multiline = True
        Me.tboxUse.Name = "tboxUse"
        Me.tboxUse.ReadOnly = True
        Me.tboxUse.Size = New System.Drawing.Size(171, 57)
        Me.tboxUse.TabIndex = 87
        Me.tboxUse.Text = "This diagram shows the steps to saving and loading within the Mastermind game"
        '
        'tboxSaveLoad
        '
        Me.tboxSaveLoad.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxSaveLoad.Location = New System.Drawing.Point(88, 32)
        Me.tboxSaveLoad.Multiline = True
        Me.tboxSaveLoad.Name = "tboxSaveLoad"
        Me.tboxSaveLoad.ReadOnly = True
        Me.tboxSaveLoad.Size = New System.Drawing.Size(171, 77)
        Me.tboxSaveLoad.TabIndex = 86
        Me.tboxSaveLoad.Text = "To use the save and load feature in Mastermind, you will have to use the navigati" & _
            "on bar and click FILE then click either SAVE or LOAD, depending on your choice."
        '
        'pboxSaveload
        '
        Me.pboxSaveload.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.saveload
        Me.pboxSaveload.Location = New System.Drawing.Point(88, 129)
        Me.pboxSaveload.Name = "pboxSaveload"
        Me.pboxSaveload.Size = New System.Drawing.Size(171, 138)
        Me.pboxSaveload.TabIndex = 0
        Me.pboxSaveload.TabStop = False
        '
        'tpGuesses
        '
        Me.tpGuesses.Controls.Add(Me.Label1)
        Me.tpGuesses.Controls.Add(Me.lblTip1)
        Me.tpGuesses.Controls.Add(Me.lblTip2)
        Me.tpGuesses.Controls.Add(Me.tboxGuess)
        Me.tpGuesses.Controls.Add(Me.btnRefresh)
        Me.tpGuesses.Controls.Add(Me.pboxColour2)
        Me.tpGuesses.Controls.Add(Me.gboxRow1)
        Me.tpGuesses.Location = New System.Drawing.Point(4, 22)
        Me.tpGuesses.Name = "tpGuesses"
        Me.tpGuesses.Padding = New System.Windows.Forms.Padding(3)
        Me.tpGuesses.Size = New System.Drawing.Size(354, 508)
        Me.tpGuesses.TabIndex = 2
        Me.tpGuesses.Text = "Make a guess"
        Me.tpGuesses.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(47, 165)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(245, 24)
        Me.Label1.TabIndex = 104
        Me.Label1.Text = "Click the Box for your colour"
        '
        'lblTip1
        '
        Me.lblTip1.AutoSize = True
        Me.lblTip1.Location = New System.Drawing.Point(43, 331)
        Me.lblTip1.Name = "lblTip1"
        Me.lblTip1.Size = New System.Drawing.Size(279, 13)
        Me.lblTip1.TabIndex = 103
        Me.lblTip1.Text = "Once you clicked on all the four boxes, press 'Take Turn'."
        '
        'lblTip2
        '
        Me.lblTip2.AutoSize = True
        Me.lblTip2.Location = New System.Drawing.Point(47, 248)
        Me.lblTip2.Name = "lblTip2"
        Me.lblTip2.Size = New System.Drawing.Size(159, 13)
        Me.lblTip2.TabIndex = 102
        Me.lblTip2.Text = "This is your selected colour ----->"
        '
        'tboxGuess
        '
        Me.tboxGuess.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tboxGuess.Location = New System.Drawing.Point(80, 41)
        Me.tboxGuess.Multiline = True
        Me.tboxGuess.Name = "tboxGuess"
        Me.tboxGuess.ReadOnly = True
        Me.tboxGuess.Size = New System.Drawing.Size(212, 104)
        Me.tboxGuess.TabIndex = 101
        Me.tboxGuess.Text = resources.GetString("tboxGuess.Text")
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(50, 427)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 100
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'pboxColour2
        '
        Me.pboxColour2.BackColor = System.Drawing.Color.Transparent
        Me.pboxColour2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pboxColour2.Location = New System.Drawing.Point(219, 211)
        Me.pboxColour2.Name = "pboxColour2"
        Me.pboxColour2.Size = New System.Drawing.Size(90, 79)
        Me.pboxColour2.TabIndex = 88
        Me.pboxColour2.TabStop = False
        '
        'gboxRow1
        '
        Me.gboxRow1.BackColor = System.Drawing.Color.Transparent
        Me.gboxRow1.Controls.Add(Me.pboxGuess3)
        Me.gboxRow1.Controls.Add(Me.pboxGuess1)
        Me.gboxRow1.Controls.Add(Me.pboxGuess2)
        Me.gboxRow1.Controls.Add(Me.pboxGuess4)
        Me.gboxRow1.Controls.Add(Me.btnTurn1)
        Me.gboxRow1.ForeColor = System.Drawing.Color.White
        Me.gboxRow1.Location = New System.Drawing.Point(17, 347)
        Me.gboxRow1.Name = "gboxRow1"
        Me.gboxRow1.Size = New System.Drawing.Size(321, 74)
        Me.gboxRow1.TabIndex = 99
        Me.gboxRow1.TabStop = False
        Me.gboxRow1.Text = "Row 1"
        '
        'pboxGuess3
        '
        Me.pboxGuess3.BackColor = System.Drawing.Color.Black
        Me.pboxGuess3.Location = New System.Drawing.Point(208, 19)
        Me.pboxGuess3.Name = "pboxGuess3"
        Me.pboxGuess3.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess3.TabIndex = 55
        Me.pboxGuess3.TabStop = False
        '
        'pboxGuess1
        '
        Me.pboxGuess1.BackColor = System.Drawing.Color.Black
        Me.pboxGuess1.Location = New System.Drawing.Point(96, 19)
        Me.pboxGuess1.Name = "pboxGuess1"
        Me.pboxGuess1.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess1.TabIndex = 39
        Me.pboxGuess1.TabStop = False
        '
        'pboxGuess2
        '
        Me.pboxGuess2.BackColor = System.Drawing.Color.Black
        Me.pboxGuess2.Location = New System.Drawing.Point(152, 19)
        Me.pboxGuess2.Name = "pboxGuess2"
        Me.pboxGuess2.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess2.TabIndex = 47
        Me.pboxGuess2.TabStop = False
        '
        'pboxGuess4
        '
        Me.pboxGuess4.BackColor = System.Drawing.Color.Black
        Me.pboxGuess4.Location = New System.Drawing.Point(264, 19)
        Me.pboxGuess4.Name = "pboxGuess4"
        Me.pboxGuess4.Size = New System.Drawing.Size(50, 50)
        Me.pboxGuess4.TabIndex = 63
        Me.pboxGuess4.TabStop = False
        '
        'btnTurn1
        '
        Me.btnTurn1.ForeColor = System.Drawing.Color.Black
        Me.btnTurn1.Location = New System.Drawing.Point(13, 30)
        Me.btnTurn1.Name = "btnTurn1"
        Me.btnTurn1.Size = New System.Drawing.Size(63, 23)
        Me.btnTurn1.TabIndex = 87
        Me.btnTurn1.Text = "T&ake turn"
        Me.btnTurn1.UseVisualStyleBackColor = True
        '
        'tpSubmit
        '
        Me.tpSubmit.Controls.Add(Me.lblTip4)
        Me.tpSubmit.Controls.Add(Me.lblTip3)
        Me.tpSubmit.Controls.Add(Me.TextBox2)
        Me.tpSubmit.Controls.Add(Me.pboxWin)
        Me.tpSubmit.Location = New System.Drawing.Point(4, 22)
        Me.tpSubmit.Name = "tpSubmit"
        Me.tpSubmit.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSubmit.Size = New System.Drawing.Size(354, 508)
        Me.tpSubmit.TabIndex = 3
        Me.tpSubmit.Text = "How to win"
        Me.tpSubmit.UseVisualStyleBackColor = True
        '
        'lblTip4
        '
        Me.lblTip4.AutoSize = True
        Me.lblTip4.Location = New System.Drawing.Point(240, 262)
        Me.lblTip4.Name = "lblTip4"
        Me.lblTip4.Size = New System.Drawing.Size(96, 13)
        Me.lblTip4.TabIndex = 89
        Me.lblTip4.Text = "<----- Take a guess"
        '
        'lblTip3
        '
        Me.lblTip3.AutoSize = True
        Me.lblTip3.Location = New System.Drawing.Point(240, 51)
        Me.lblTip3.Name = "lblTip3"
        Me.lblTip3.Size = New System.Drawing.Size(89, 13)
        Me.lblTip3.TabIndex = 88
        Me.lblTip3.Text = "<----- The solution"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TextBox2.Location = New System.Drawing.Point(20, 327)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(171, 139)
        Me.TextBox2.TabIndex = 87
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        '
        'pboxWin
        '
        Me.pboxWin.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.win
        Me.pboxWin.Location = New System.Drawing.Point(20, 24)
        Me.pboxWin.Name = "pboxWin"
        Me.pboxWin.Size = New System.Drawing.Size(202, 281)
        Me.pboxWin.TabIndex = 0
        Me.pboxWin.TabStop = False
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(103, 663)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 1
        Me.btnBack.Text = "B&ack"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnPlay
        '
        Me.btnPlay.Location = New System.Drawing.Point(390, 663)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(75, 23)
        Me.btnPlay.TabIndex = 2
        Me.btnPlay.Text = "&Play"
        Me.btnPlay.UseVisualStyleBackColor = True
        '
        'frmTutorial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Playinterface
        Me.ClientSize = New System.Drawing.Size(553, 762)
        Me.Controls.Add(Me.btnPlay)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.tconTutorial)
        Me.Name = "frmTutorial"
        Me.Text = "Tutorial"
        Me.tconTutorial.ResumeLayout(False)
        Me.tpColour.ResumeLayout(False)
        Me.tpColour.PerformLayout()
        Me.gboxColour.ResumeLayout(False)
        CType(Me.pboxGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxColour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxOrange, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxYellow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxBlue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxPurple, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpManage.ResumeLayout(False)
        Me.tpManage.PerformLayout()
        CType(Me.pboxSaveload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpGuesses.ResumeLayout(False)
        Me.tpGuesses.PerformLayout()
        CType(Me.pboxColour2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxRow1.ResumeLayout(False)
        CType(Me.pboxGuess3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboxGuess4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpSubmit.ResumeLayout(False)
        Me.tpSubmit.PerformLayout()
        CType(Me.pboxWin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tconTutorial As System.Windows.Forms.TabControl
    Friend WithEvents tpColour As System.Windows.Forms.TabPage
    Friend WithEvents tpManage As System.Windows.Forms.TabPage
    Friend WithEvents tpGuesses As System.Windows.Forms.TabPage
    Friend WithEvents tpSubmit As System.Windows.Forms.TabPage
    Friend WithEvents pboxColour As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGreen As System.Windows.Forms.PictureBox
    Friend WithEvents pboxYellow As System.Windows.Forms.PictureBox
    Friend WithEvents pboxPurple As System.Windows.Forms.PictureBox
    Friend WithEvents pboxBlue As System.Windows.Forms.PictureBox
    Friend WithEvents pboxOrange As System.Windows.Forms.PictureBox
    Friend WithEvents pboxRed As System.Windows.Forms.PictureBox
    Friend WithEvents gboxColour As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lblStep2 As System.Windows.Forms.Label
    Friend WithEvents lblStep1 As System.Windows.Forms.Label
    Friend WithEvents tboxSelect As System.Windows.Forms.TextBox
    Friend WithEvents tboxColour As System.Windows.Forms.TextBox
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnPlay As System.Windows.Forms.Button
    Friend WithEvents pboxSaveload As System.Windows.Forms.PictureBox
    Friend WithEvents tboxUse As System.Windows.Forms.TextBox
    Friend WithEvents tboxSaveLoad As System.Windows.Forms.TextBox
    Friend WithEvents tboxNewgame As System.Windows.Forms.TextBox
    Friend WithEvents gboxRow1 As System.Windows.Forms.GroupBox
    Friend WithEvents pboxGuess3 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess1 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess2 As System.Windows.Forms.PictureBox
    Friend WithEvents pboxGuess4 As System.Windows.Forms.PictureBox
    Friend WithEvents btnTurn1 As System.Windows.Forms.Button
    Friend WithEvents pboxColour2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents lblTip1 As System.Windows.Forms.Label
    Friend WithEvents lblTip2 As System.Windows.Forms.Label
    Friend WithEvents tboxGuess As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pboxWin As System.Windows.Forms.PictureBox
    Friend WithEvents lblTip4 As System.Windows.Forms.Label
    Friend WithEvents lblTip3 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
End Class
