Public Class frmTutorial
    Dim PictureColour() As Color = {Color.Red, Color.Blue, Color.Lime, Color.Yellow, Color.Purple, Color.Orange} 'colour array
    Private Sub pboxRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxRed.Click
        pboxColour.BackColor = PictureColour(0) 'equalling the colour 0 - RED
    End Sub

    Private Sub pboxYellow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxYellow.Click
        pboxColour.BackColor = PictureColour(3) 'equalling the colour 3 - YELLOW
    End Sub

    Private Sub pboxPurple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxPurple.Click
        pboxColour.BackColor = PictureColour(4) 'equalling the colour 4 - PURPLE
    End Sub

    Private Sub pboxBlue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxBlue.Click
        pboxColour.BackColor = PictureColour(1) 'equalling the colour 1 - BLUE
    End Sub

    Private Sub pboxOrange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxOrange.Click
        pboxColour.BackColor = PictureColour(5) 'equalling the colour 5 - ORANGE
    End Sub

    Private Sub pboxGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGreen.Click
        pboxColour.BackColor = PictureColour(2) 'equalling the colour 2 - LIME
    End Sub
    Private Sub btnPlay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlay.Click
        Me.Hide()
        frmMastermind.Show() 'goes to the mastermind form
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Me.Hide()
        frmMainmenu.Show() 'goes to the main form
    End Sub
    Private Sub pboxGuess1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess1.Click
        pboxGuess1.BackColor = pboxColour2.BackColor 'Selecing the colour on click
    End Sub

    Private Sub pboxGuess2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess2.Click
        pboxGuess2.BackColor = pboxColour2.BackColor 'Selecting the colour on click
    End Sub

    Private Sub pboxGuess3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess3.Click
        pboxGuess3.BackColor = pboxColour2.BackColor 'Selecting colour on click
    End Sub

    Private Sub pboxGuess4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxGuess4.Click
        pboxGuess4.BackColor = pboxColour2.BackColor 'Selecting colour on click
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        pboxGuess1.BackColor = Color.Black 'resets the back colours
        pboxGuess2.BackColor = Color.Black
        pboxGuess3.BackColor = Color.Black
        pboxGuess4.BackColor = Color.Black
    End Sub

    Private Sub btnTurn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTurn1.Click
        MessageBox.Show("Congratulations, you just made your first move!") 'Shows message on click
    End Sub

    Private Sub pboxColour2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pboxColour2.Click
        pboxColour2.BackColor = pboxColour.BackColor 'Selecing colour on click
    End Sub
End Class