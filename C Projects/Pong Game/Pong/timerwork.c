#include <stdlib.h>
#include <cab202_graphics.h>
#include <cab202_sprites.h>
#include <cab202_timers.h>
#include <math.h>
#include <string.h>

//Game state
bool game_over = false;
bool update_screen = true;

double cur_time; 
double prev_time;
double dif_time;

	int left = 0, 
	    right = screen_width() - 1, 
		top = 0, 
		bottom = screen_height() - 1;

// Program entry point.
int main(void) {
	cur_time = get_current_time();
	prev_time = cur_time;
	dif_time = cur_time - prev_time;

	while (! game_over){
		if (update_screen){
			cur_time = get_current_time() * 0.001;
			dif_time = cur_time - prev_time;
			prev_time = cur_time;
			draw_formatted(left + 90, top + 1, "Time:%f", dif_time);


		}
	}
				printf("Time is %f", dif_time);
	
	//cleanup_screen();
	return 0;
}