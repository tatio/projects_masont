#include <stdlib.h>
#include <cab202_graphics.h>
#include <cab202_sprites.h>
#include <cab202_timers.h>
#include <math.h>
#include <string.h>
#include <time.h>

// Configuration
#define DELAY (5) /* Millisecond delay between game updates */
#define PLAYER_WIDTH (1)
#define AI_WIDTH (1)
#define MAX_BRICKS (1000)

void update_player1(int key);
bool ball_collided( sprite_id sprite_1, sprite_id sprite_2 );
void do_game_over( void );
void setup_sprites(void); 
void game_countdown(void);
void restart_msg_notification(void);
void setup_brick(void);
void draw_brick(void);

//Game state
bool game_over = false;
bool update_screen = true;
int lives = 10;
int score = 0;
int level = 1;
int minutes = 0;
int seconds = 0;
int counter = 0;
int aiH;
int paddleHeight;

timer_id countdown;
timer_id timer; 
sprite_id ball;
sprite_id player_1;
sprite_id ai_paddle;
sprite_id singularity;
sprite_id brick;
int top_coords[MAX_BRICKS];
int bot_coords[MAX_BRICKS];

int x_range;
int y_range; 
int x_rangeb;
int y_rangeb;

char * paddle_img =
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|";

char * paddle_img2 =
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|"
/**/          "|";

char * help_msg =
/**/	"           w to move up a to move down           "
/**/	"           Created by Mason Teitzel             "
/**/	"       h for help menu l change level         "
/**/	"                   N9440941                  "
/**/	"                  q to Quit                    "
/**/	"             Press any key to play...           ";

char * restart_msg =
/**/	"         Do you wish to play again?             "
/**/	"                  Y / N                         ";

char * singularity_img = 
/**/ " \\ | / "
/**/ "  \\|/  "
/**/ "--   --"
/**/ "  /|\\  "
/**/ " / | \\ ";

void draw_border (void){

	int left = 0, 
	 right = screen_width() - 1, 
		top = 0, 
		bottom = screen_height() - 1;

			

	draw_line( left, top, right, top, '*' );
	draw_line( left, top + 2, right, top + 2, '*' );
	draw_line( left, bottom, right, bottom, '*' );
	draw_line( left, top, left, bottom, '*' );
	draw_line( right, top, right, bottom, '*' );

	draw_formatted(left + 3, top + 1, "* Lives:%d", lives);
	draw_formatted(left + 20, top + 1, "* Score:%d", score);
	draw_formatted(left + 40, top + 1, "* Level:%d", level);
	draw_formatted(left + 60, top + 1, "* Time: %02d:%02d", minutes, seconds);
}

void intro_message(void){
int intro_msg_h = 3;
int intro_msg_w = strlen(help_msg) / intro_msg_h;

sprite_id msg_box = sprite_create(
		( screen_width() - intro_msg_w ) / 2,
		( screen_height() - intro_msg_h ) / 2,
		intro_msg_w, intro_msg_h, help_msg );
		sprite_draw( msg_box );
		show_screen();
		wait_char();
}

void help_setup(void){
	intro_message();
	int key = get_char();

	if (key) {

	clear_screen();
	draw_border();

	return;
	}
}

void draw_game(void){
	draw_border();
}

void process (void){
	int key = get_char();


	if (key == 'h') {
		clear_screen();
		help_setup();
	}

	if (key == 'q'){
		game_over = true;
	}

	if (key == 'l' && level == 1) {
		level = 2;
		clear_screen();
		draw_border();
		setup_sprites();
		game_countdown();
	} else if (key == 'l' && level == 2){
		level = 3;
		clear_screen();
		draw_border();
		setup_sprites();
		game_countdown();
		counter = 0;
	} else if (key == 'l' && level == 3){
		level = 4;
		clear_screen();
		draw_border();
		setup_sprites();
		game_countdown();
		setup_brick();
	} else if (key == 'l' && level == 4){
		level = 1;
		clear_screen();
		draw_border();
		setup_sprites();
		game_countdown();
		setup_brick();
	}

	int zx = round(sprite_x(ball));
	int zy = round(sprite_y(ball));
	update_player1(key);
	int prevzy = zy;
	sprite_step(ball);

	zx = round(sprite_x(ball));
	zy = round(sprite_y(ball));
	int w = screen_width();
	int h = screen_height();

	double dx = sprite_dx(ball);
    double dy = sprite_dy(ball);
    bool dir_changed = false;	

    int sprite_top = round(sprite_y(player_1));
    int sprite_bot = sprite_top + sprite_height(player_1) - 1;

    if (ball_collided(ball, player_1)){
    	if (prevzy +1 == sprite_top){
    		dir_changed = true;
    		dy = -dy;
    		score++;
    	} 
    	if (prevzy -1 == sprite_bot){
    		dy = -dy;
    		dx = -dx;
    		dir_changed = true;

    		score++;
    	} else {
    	
    	dir_changed = true;
    	dx = -dx;
    	score++;

    	}
    }

    //Tests left collision
	if ((zx == 0)) {
		dx = -dx;
		dir_changed = true;
	}

 	//Tests right collision
	if ((zx == w - 1) ) {
			lives--;

		if (lives <= 0){
			restart_msg_notification();
		} else {
		clear_screen();
		draw_border();
		setup_sprites();
		game_countdown();
		counter = 0;
		}
	}

	//Tests top and bottom collision
	if (zy == 2 || zy == h - 1) {
		dy = -dy;
		dir_changed = true;
	}

	//Test to see if ball needs to change direction
	if (dir_changed) {
	sprite_back(ball);
	sprite_turn_to(ball, dx, dy);
	}

    clear_screen();
    sprite_draw(ball);
    sprite_draw(player_1);

	if (level == 2){

    	if (ball_collided(ball, ai_paddle)){
			dir_changed = true;
			dx = -dx;
		}

		if (dir_changed) {
			sprite_back(ball);
			sprite_turn_to(ball, dx, dy);
		}

		if (zy - (paddleHeight/2) < 3) {

		} 
		else if ( zy + (paddleHeight/2) > screen_height() - 1){

		}
		else {
		sprite_move_to(ai_paddle, 2, zy - (paddleHeight/2));
		}

		sprite_draw(ai_paddle);
	}
	else if (level == 3){

		if (ball_collided(ball, ai_paddle)){
			dir_changed = true;
			dx = -dx;
		}

		if (dir_changed) {
			sprite_back(ball);
			sprite_turn_to(ball, dx, dy);
		}

		if (zy - (paddleHeight/2) < 3) {

		} 
		else if ( zy + (paddleHeight/2) > screen_height() - 1){

		}
		else {
		sprite_move_to(ai_paddle, 2, zy - (paddleHeight/2));
		}

		sprite_draw(ai_paddle);

		if (counter > 4){

			int sx = screen_width() /2;
			int sy = screen_height() /2;

    	sprite_draw(singularity);

    	if (ball_collided(ball, singularity)){

	    	double diff_x = sx - sprite_x(ball);
	    	double diff_y = sy - sprite_y(ball);

	 		double dist_squared = (diff_x * diff_x) + (diff_y * diff_y);

	  		if(dist_squared < 1e-10) {
	        	dist_squared = 1e-10;
	    	}

	 		double dist = sqrt(dist_squared);

			double a = 0.2/dist_squared;


	  		dx = dx + (a * diff_x / dist);
	    	dy = dy + (a * diff_y / dist);

			double speed = sqrt((dx * dx) + (dy * dy));
	  		
	  		if(speed > 1) {
	        	dx = dx/speed;
	        	dy = dy/speed;
	    	}
				sprite_turn_to(ball, dx, dy);
			}
		}
	}
		else if (level == 4){
		
		int array_size = (screen_width() /2) -2;

    	if (ball_collided(ball, ai_paddle)){
			dir_changed = true;
			dx = -dx;
		}

		if (dir_changed) {
			sprite_back(ball);
			sprite_turn_to(ball, dx, dy);
		}

		if (zy - (paddleHeight/2) < 3) {

		} 
		else if ( zy + (paddleHeight/2) > screen_height() - 1){

		}
		else {
		sprite_move_to(ai_paddle, 2, zy - (paddleHeight/2));
		}

		sprite_draw(ai_paddle);
		draw_brick();
		
		for (int i = 0; i < array_size; i++ ){

			if (zx == top_coords[i] && zy == y_range ){
				if (prevzy == zy){
					dx = -dx;
				} else {

					dy = -dy;
				}

				sprite_back(ball);
				sprite_turn_to(ball, dx, dy);
				top_coords[i] = -1;
				top_coords[i + 1] = -1;
				top_coords[i - 1] = -1;
				score++;
			}

			if (zx == bot_coords[i] && zy == y_rangeb){
				if (prevzy == zy){
					dx = -dx;
					dy = -dy;
				} else {
					dy = -dy;
				}
			
			sprite_back(ball);
			sprite_turn_to(ball, dx, dy);
			bot_coords[i] = -1;
			bot_coords[i + 1] = -1;
			bot_coords[i - 1] = -1;
			score++;

			}
			} 
		}
	}

void setup_brick(void){

	int array_size = (screen_width() /2) -2;
	x_range = (screen_width() /4);
	y_range = (screen_height() *1/3) ; // *1/3

	x_rangeb = (screen_width() *1/4);
	y_rangeb = (screen_height() * 2/3);
	
	for (int i = 0; i < array_size; i++){
		draw_char(x_range, y_range, '=');
		top_coords[i] = x_range;
		x_range++;	
	}

	for (int i = 0; i < array_size; i++){
		draw_char(x_rangeb, y_rangeb, '=');
		bot_coords[i] = x_rangeb;
		x_rangeb++;	
	}

}

void draw_brick(void){

	int array_size = (screen_width() /2) -2;
	x_range = (screen_width() /4);
	y_range = (screen_height() *1/3);

	x_rangeb = (screen_width() *1/4);
	y_rangeb = (screen_height() * 2/3);
	
	for (int i = 0; i < array_size; i++){
		draw_char(top_coords[i], y_range, '=');	
	}

	for (int i = 0; i < array_size; i++){
		draw_char(bot_coords[i], y_rangeb, '=');
	}
}

void timer_process(void){
		if (timer_expired(timer)){
		seconds++;
		counter++;
			if (seconds % 60 == 0){
				clear_screen();
				seconds = 0;
				minutes++;
			}
		}
}

void setup_sprite_size(void){
	
	if ((screen_height() - 3 - 1) / 2 > 7){
		paddleHeight = (screen_height() - 3 - 1) /2;
		aiH = (screen_height() - 3 - 1) /2;
	} else {
		paddleHeight = 7;
		aiH = 7;
	}
}

void setup_sprites(void){
	int width = screen_width();
	int height = screen_height(); 
	int aiW = AI_WIDTH;
	int pw = PLAYER_WIDTH; 

	setup_sprite_size();

	int player1_x = (width - 3);
	int player1_y = (height - paddleHeight) / 2;

	int ai_x = (width - width + 2);
	int ai_y = (height - paddleHeight) / 2;

	player_1 = sprite_create(player1_x, player1_y + 1, pw, paddleHeight, paddle_img);
	ball = sprite_create(width/2, height/2, 1, 1, "O");

	if (level == 2){
		ai_paddle = sprite_create(ai_x, ai_y + 1, aiW, aiH, paddle_img2);
		sprite_draw(ai_paddle);
	} else if (level == 3){
		singularity = sprite_create((width - 7) /2, (height - 5)/2, 7, 5, singularity_img);
		ai_paddle = sprite_create(ai_x, ai_y + 1, aiW, aiH, paddle_img2);
		sprite_draw(ai_paddle);
	} else if (level == 4){
		ai_paddle = sprite_create(ai_x, ai_y + 1, aiW, aiH, paddle_img2);
		sprite_draw(ai_paddle);
	}

	sprite_turn_to(ball, 0.1, 0);

	int angle = rand()%45;
	int randrand = rand() % 6;

	srand(get_current_time());
	if (randrand <= 3 ){	
		angle = angle * -1;
	}

	sprite_turn(ball, angle);
	sprite_draw(ball);
	sprite_draw(player_1);
}

void update_player1(int key){

int yh = round( sprite_y( player_1 ) );

//Paddle up
if ('w' == key && yh > 3){
	sprite_move(player_1, 0, -1);
	}

//Paddle down
	else if ( 's' == key && yh < screen_height() - paddleHeight - 1 ) {
	sprite_move( player_1, 0, +1 );
	}
}

bool ball_collided (sprite_id sprite_1, sprite_id sprite_2 ){
		int sprite_1_top = round( sprite_y( sprite_1 ) ),
		sprite_1_bottom = sprite_1_top + sprite_height(sprite_1) - 1,
		sprite_1_left = round( sprite_x( sprite_1 ) ),
		sprite_1_right = sprite_1_left + sprite_width(sprite_1) - 1;

	int sprite_2_top = round( sprite_y( sprite_2 ) ),
		sprite_2_bottom = sprite_2_top + sprite_height(sprite_2) - 1,
		sprite_2_left = round( sprite_x( sprite_2 ) ),
		sprite_2_right = sprite_2_left + sprite_width( sprite_2 ) - 1;

	return !(
		sprite_1_bottom < sprite_2_top
		|| sprite_1_top > sprite_2_bottom
		|| sprite_1_right < sprite_2_left
		|| sprite_1_left > sprite_2_right
		);
}

void restart_msg_notification(void){

	bool check = true;
	clear_screen();
	int restart_msg_h = 2;
	int restart_msg_w = strlen(restart_msg) / restart_msg_h;

	sprite_id restart_msg_box = sprite_create(
		(screen_width() - restart_msg_w ) /2,
		( screen_height() - restart_msg_h ) / 2,
		restart_msg_w, restart_msg_h, restart_msg);
	sprite_draw(restart_msg_box);

	show_screen();

	while (check) {
	
	int key2 = wait_char();

	if (key2 == 'n') {
		do_game_over();
		check = false;
	}

	if (key2 == 'y'){
		check = false;

		seconds = 0;
		lives = 2;
		minutes = 0;
		score = 0;
		level = 1;
		clear_screen();
		help_setup();
		setup_sprites();
		draw_border();
		game_countdown();
	}
}
}

void do_game_over(void){
	game_over = true;
}

void game_countdown(void){
	int count = 3;

	while (count != -1){
		draw_formatted(screen_width()/2, screen_height()/2, "%d", count);
		timer_pause(1000);
		show_screen();
		count--;	
	}
}

// Program entry point.
int main(void) {

	setup_screen();
	help_setup();
	setup_sprites();
	if (level == 4){
		setup_brick();
	}
	game_countdown();
	clear_screen();
	
	timer = create_timer(1000);
	countdown = create_timer(1000);

	while (! game_over){
		process();
		timer_process();

		if (update_screen){
			draw_border();
			show_screen();
		}

		timer_pause(DELAY);
	}

	return 0;
}