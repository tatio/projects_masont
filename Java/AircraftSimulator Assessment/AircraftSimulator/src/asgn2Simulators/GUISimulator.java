/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;
import asgn2Passengers.PassengerException;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;

/**
 * @author hogan
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame {

    Simulator sim;
    Log log;
    private JPanel leftPanel;
    private JPanel bottomPanel;
    private JLabel lblSimulation;
    private JLabel lblFareClasses;
    private JLabel lblOperation;
    private JTextPane tpaneRng;
    private JTextPane tpaneMean;
    private JTextPane tpaneQueue;
    private JTextPane tpaneCancel;
    private JTextPane tpaneFirst;
    private JTextPane tpaneBusiness;
    private JTextPane tpanePrem;
    private JTextPane tpaneEcon;
    private JLabel lblFirst;
    private JLabel lblBusiness;
    private JLabel lblPrem;
    private JLabel lblEcon;
    private JLabel lblCancel;
    private JLabel lblRng;
    private JLabel lblMean;
    private JLabel lblQueue;
    private JButton btnRunSimulation;
    private JPanel rightPanel;
    private JPanel topPanel;
    private JTabbedPane tabbedMiddle;
    private JPanel panelText;
    private JPanel panelGraph;
    private JTextArea textField;
    private JScrollPane scrollPane;
    private int[] numFirst = new int[Constants.DURATION + 1];
    private int[] numBusiness = new int[Constants.DURATION + 1];
    private int[] numPremium = new int[Constants.DURATION + 1];
    private int[] numEconomy = new int[Constants.DURATION + 1];
    private int[] numTotal = new int[Constants.DURATION + 1];
    private int[] numEmpty = new int[Constants.DURATION + 1];
    private int[] numQueue = new int[Constants.DURATION + 1];
    private int[] numRefused = new int[Constants.DURATION + 1];

    /**
     * @param arg0
     * @throws HeadlessException
     * @throws SimulationException
     * @throws NumberFormatException
     * @throws IOException
     * @throws PassengerException
     * @throws AircraftException
     */
    public GUISimulator(String[] args) throws HeadlessException, NumberFormatException, SimulationException,
            IOException, PassengerException, AircraftException {
        super();
        setResizable(false);
        setMinimumSize(new Dimension(800, 600));

        GUISimulator that = this;

        // private void createGUI() {
        setTitle("Aircraft Simulator");
        getContentPane().setLayout(null);

        leftPanel = new JPanel();
        leftPanel.setBackground(SystemColor.menu);
        leftPanel.setBounds(0, 29, 25, 347);
        getContentPane().add(leftPanel);
        leftPanel.setLayout(null);

        bottomPanel = new JPanel();
        bottomPanel.setBackground(SystemColor.menu);
        bottomPanel.setBounds(0, 375, 794, 196);
        getContentPane().add(bottomPanel);
        bottomPanel.setLayout(null);

        lblSimulation = new JLabel("Simulation");
        lblSimulation.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblSimulation.setBounds(155, 26, 94, 25);
        bottomPanel.add(lblSimulation);

        lblFareClasses = new JLabel("Fare Classes");
        lblFareClasses.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblFareClasses.setBounds(318, 26, 111, 25);
        bottomPanel.add(lblFareClasses);

        lblOperation = new JLabel("Operation");
        lblOperation.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblOperation.setBounds(500, 26, 94, 25);
        bottomPanel.add(lblOperation);

        tpaneRng = new JTextPane();
        tpaneRng.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneRng.setText(String.valueOf(Constants.DEFAULT_SEED)); // Int?
        tpaneRng.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg1) {

                try {
                    String S = tpaneRng.getText();

                    tpaneRng.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Integer.parseInt(tpaneRng.getText());
                    }
                } catch (Exception e) {
                    tpaneRng.setBackground(Color.RED);
                }
            }
        });
        tpaneRng.setBounds(243, 62, 54, 17);
        bottomPanel.add(tpaneRng);

        tpaneMean = new JTextPane();
        tpaneMean.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneMean.setText(String.valueOf(Constants.DEFAULT_DAILY_BOOKING_MEAN));
        tpaneMean.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {

                try {
                    String S = tpaneMean.getText();

                    tpaneMean.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpaneMean.getText());
                    }
                } catch (Exception e) {
                    tpaneMean.setBackground(Color.RED);
                }
            }
        });
        tpaneMean.setBounds(243, 90, 54, 17);
        bottomPanel.add(tpaneMean);

        tpaneQueue = new JTextPane();
        tpaneQueue.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneQueue.setText(String.valueOf(Constants.DEFAULT_MAX_QUEUE_SIZE));
        tpaneQueue.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
                try {
                    String S = tpaneQueue.getText();

                    tpaneQueue.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Integer.parseInt(tpaneQueue.getText());
                    }
                } catch (Exception e) {
                    tpaneQueue.setBackground(Color.RED);
                }
            }
        });
        tpaneQueue.setBounds(243, 118, 54, 17);
        bottomPanel.add(tpaneQueue);

        tpaneCancel = new JTextPane();
        tpaneCancel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneCancel.setText(String.valueOf(Constants.DEFAULT_CANCELLATION_PROB));
        tpaneCancel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {

                try {
                    String S = tpaneCancel.getText();

                    tpaneCancel.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpaneCancel.getText());
                    }
                } catch (Exception e) {
                    tpaneCancel.setBackground(Color.RED);
                }
            }
        });
        tpaneCancel.setBounds(243, 146, 54, 17);
        bottomPanel.add(tpaneCancel);

        tpaneFirst = new JTextPane();
        tpaneFirst.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneFirst.setText(String.valueOf(Constants.DEFAULT_FIRST_PROB));
        tpaneFirst.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {

                try {
                    String S = tpaneFirst.getText();

                    tpaneFirst.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpaneFirst.getText());
                    }
                } catch (Exception e) {
                    tpaneFirst.setBackground(Color.RED);
                }
            }
        });
        tpaneFirst.setBounds(394, 62, 54, 17);
        bottomPanel.add(tpaneFirst);

        tpaneBusiness = new JTextPane();
        tpaneBusiness.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneBusiness.setText(String.valueOf(Constants.DEFAULT_BUSINESS_PROB));
        tpaneBusiness.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {

                try {
                    String S = tpaneBusiness.getText();

                    tpaneBusiness.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpaneBusiness.getText());
                    }
                } catch (Exception e) {
                    tpaneBusiness.setBackground(Color.RED);
                }
            }
        });
        tpaneBusiness.setBounds(394, 90, 54, 17);
        bottomPanel.add(tpaneBusiness);

        tpanePrem = new JTextPane();
        tpanePrem.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpanePrem.setText(String.valueOf(Constants.DEFAULT_PREMIUM_PROB));
        tpanePrem.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {

                try {
                    String S = tpanePrem.getText();

                    tpanePrem.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpanePrem.getText());
                    }
                } catch (Exception e) {
                    tpanePrem.setBackground(Color.RED);
                }
            }
        });
        tpanePrem.setBounds(394, 118, 54, 17);
        bottomPanel.add(tpanePrem);

        tpaneEcon = new JTextPane();
        tpaneEcon.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        tpaneEcon.setText(String.valueOf(Constants.DEFAULT_ECONOMY_PROB));
        tpaneEcon.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
                try {
                    String S = tpaneEcon.getText();

                    tpaneEcon.setBackground(Color.WHITE);
                    if (!S.isEmpty()) {
                        Double.parseDouble(tpaneEcon.getText());
                    }
                } catch (Exception e) {
                    tpaneEcon.setBackground(Color.RED);
                }
            }
        });
        tpaneEcon.setBounds(394, 146, 54, 17);
        bottomPanel.add(tpaneEcon);

        lblFirst = new JLabel("First");
        lblFirst.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblFirst.setBounds(318, 65, 46, 14);
        bottomPanel.add(lblFirst);

        lblBusiness = new JLabel("Business");
        lblBusiness.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblBusiness.setBounds(318, 93, 58, 14);
        bottomPanel.add(lblBusiness);

        lblPrem = new JLabel("Premium");
        lblPrem.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblPrem.setBounds(318, 121, 58, 14);
        bottomPanel.add(lblPrem);

        lblEcon = new JLabel("Economy");
        lblEcon.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblEcon.setBounds(318, 149, 68, 14);
        bottomPanel.add(lblEcon);

        lblCancel = new JLabel("Cancellation");
        lblCancel.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblCancel.setBounds(155, 149, 77, 14);
        bottomPanel.add(lblCancel);

        lblRng = new JLabel("RNG Seed");
        lblRng.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblRng.setBounds(155, 65, 68, 14);
        bottomPanel.add(lblRng);

        lblMean = new JLabel("Daily Mean");
        lblMean.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblMean.setBounds(155, 93, 77, 14);
        bottomPanel.add(lblMean);

        lblQueue = new JLabel("Queue Size");
        lblQueue.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblQueue.setBounds(155, 121, 77, 14);
        bottomPanel.add(lblQueue);

        btnRunSimulation = new JButton("Run Simulation");
        btnRunSimulation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Integer.parseInt(tpaneRng.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in RNG Seed",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneRng.setText("");
                    tpaneRng.setBackground(Color.WHITE);
                    return;
                }

                try {
                    Double.parseDouble(tpaneMean.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Daily Mean",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneMean.setText("");
                    tpaneMean.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Integer.parseInt(tpaneQueue.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Queue Size",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneQueue.setText("");
                    tpaneQueue.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Double.parseDouble(tpaneCancel.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Cancellation",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneCancel.setText("");
                    tpaneCancel.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Double.parseDouble(tpaneFirst.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in First",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneFirst.setText("");
                    tpaneFirst.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Double.parseDouble(tpaneBusiness.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Business",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneBusiness.setText("");
                    tpaneBusiness.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Double.parseDouble(tpanePrem.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Premium",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpanePrem.setText("");
                    tpanePrem.setBackground(Color.WHITE);
                    return;
                }
                try {
                    Double.parseDouble(tpaneEcon.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(that, "Invalid character(s) Enterered in Economy",
                            "Warning: Invalid Input", JOptionPane.ERROR_MESSAGE);
                    tpaneEcon.setText("");
                    tpaneEcon.setBackground(Color.WHITE);
                    return;
                }
                try {
                    runSimulator(args);
                } catch (Exception e) {
                	JOptionPane.showMessageDialog(that, "Simulation Error",
                            "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnRunSimulation.setBounds(474, 93, 145, 42);
        bottomPanel.add(btnRunSimulation);

        rightPanel = new JPanel();
        rightPanel.setBackground(SystemColor.menu);
        rightPanel.setBounds(773, 0, 25, 376);
        getContentPane().add(rightPanel);
        rightPanel.setLayout(null);

        topPanel = new JPanel();
        topPanel.setBackground(SystemColor.menu);
        topPanel.setBounds(0, 0, 773, 29);
        getContentPane().add(topPanel);

        tabbedMiddle = new JTabbedPane(JTabbedPane.TOP);
        tabbedMiddle.setBounds(25, 29, 748, 347);
        getContentPane().add(tabbedMiddle);

        panelText = new JPanel();
        tabbedMiddle.addTab("Text", null, panelText, null);
        getContentPane().setLayout(new BorderLayout(0, 0));
        panelText.setLayout(new BorderLayout(0, 0));

        textField = new JTextArea();
        panelText.add(textField, BorderLayout.CENTER);
        textField.setEditable(false);
        textField.setWrapStyleWord(true);
        textField.setLineWrap(true);

        scrollPane = new JScrollPane(textField);
        panelText.add(scrollPane, BorderLayout.CENTER);

        panelGraph = new JPanel();
        tabbedMiddle.addTab("Graph", null, panelGraph, null);
        panelGraph.setLayout(new BorderLayout(0, 0));
        
        this.setVisible(true);
    }

    // Might need to change to casting

    // panelTextsim.toString();
    // panelText.toString(test);

    /**
     * @param args
     * @throws SimulationException
     * @throws NumberFormatException
     * @throws HeadlessException
     * @throws IOException
     * @throws AircraftException
     * @throws PassengerException
     */
    public static void main(String[] args) throws HeadlessException, NumberFormatException, SimulationException,
            IOException, PassengerException, AircraftException {
        // Threads is not used, as window builder is being used instead
        new GUISimulator(args);

    }

    /**
     * @param args
     * @throws NumberFormatException
     * @throws SimulationException
     * @throws IOException
     * @throws AircraftException
     * @throws PassengerException
     */
    public void runSimulator(String[] args)
            throws NumberFormatException, SimulationException, IOException, AircraftException, PassengerException {
    	sim = new Simulator(Integer.parseInt(tpaneRng.getText()), Integer.parseInt(tpaneQueue.getText()),
                Double.parseDouble(tpaneMean.getText()), (0.33 * Double.parseDouble(tpaneMean.getText())),
                Double.parseDouble(tpaneFirst.getText()), Double.parseDouble(tpaneBusiness.getText()),
                Double.parseDouble(tpanePrem.getText()), Double.parseDouble(tpaneEcon.getText()),
                Double.parseDouble(tpaneCancel.getText()));

        log = new Log();

        SimulationRunner.main(args);
        sim.createSchedule();

        textField.setText((new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())
                + ": Start of Simulation\n" + sim.toString() + "\n"
                + sim.getFlights(Constants.FIRST_FLIGHT).initialState()));
        
        for (int time = 0; time <= Constants.DURATION; time++) {
            sim.resetStatus(time);
            sim.rebookCancelledPassengers(time);
            sim.generateAndHandleBookings(time);
            sim.processNewCancellations(time);
            if (time >= Constants.FIRST_FLIGHT) {
                sim.processUpgrades(time);
                sim.processQueue(time);
                sim.flyPassengers(time);
                sim.updateTotalCounts(time);
                log.logFlightEntries(time, sim);
            } else {
                sim.processQueue(time);
            }
            this.log.logQREntries(time, sim);
            this.log.logEntry(time, sim);
            boolean inFlight = (time >= Constants.FIRST_FLIGHT);
            
            textField.setText(textField.getText() + sim.getSummary(time, inFlight));			
			
            if(inFlight) {
				Flights flights = sim.getFlights(time);
				Bookings counts = flights.getCurrentCounts();
				
				numEconomy[time] = counts.getNumEconomy();
				numPremium[time] = counts.getNumPremium();
				numBusiness[time] = counts.getNumBusiness();
				numFirst[time] = counts.getNumFirst();
				numTotal[time] = numEconomy[time] + numPremium[time] + numBusiness[time] + numFirst[time];
				numEmpty[time] = counts.getAvailable();
				
				numQueue[time] = sim.numInQueue();
				numRefused[time] = sim.numRefused();
			}
        }
        
        
		sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION); 
		chartGraphDisplay(true);
		textField.setText(textField.getText()+"\n" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())
				+ ": End of Simulation\n" + sim.finalState());
		

        
    }
    
    private void chartGraphDisplay(boolean chart1) {
		getContentPane().remove(getContentPane().getComponentAt(27, 11));
		getContentPane().repaint();
		final TimeSeriesCollection dataset;
		if (chart1) {
			dataset = createChart1();
		}
		else {
			dataset = createChart2();
		}
        JFreeChart chart = createChart(dataset, chart1);
        ChartPanel display = new ChartPanel(chart);
        display.setSize(573, 251);
        display.setBounds(27, 11, 573, 251);
        panelGraph.add(display);
        
	}
	
	private TimeSeriesCollection createChart1() {
		TimeSeriesCollection tsc = new TimeSeriesCollection(); 
		TimeSeries bookTotal = new TimeSeries("Total");
		TimeSeries econTotal = new TimeSeries("Economy"); 
		TimeSeries busTotal = new TimeSeries("Business");
		TimeSeries premTotal = new TimeSeries("Premium");
		TimeSeries firTotal = new TimeSeries("First");
		TimeSeries empTotal = new TimeSeries("Seats Available");
		Calendar cal = GregorianCalendar.getInstance(); 
		
		int economy = 0;
		int premium = 0;
		int business = 0; 
		int first = 0;
		int total = 0;
		int empty = 0;
		for (int i = Constants.FIRST_FLIGHT; i <= Constants.DURATION; i++) {
			
			cal.set(2016,0,i,6,0);
	        Date timePoint = cal.getTime();
	        
	        economy = numEconomy[i];
	        premium = numPremium[i];
	        business = numBusiness[i];
	        first = numFirst[i];
	        total = numTotal[i];
	        empty = numEmpty[i];
	        
	        busTotal.add(new Day(timePoint), business);
			econTotal.add(new Day(timePoint), economy);
			premTotal.add(new Day(timePoint), premium);
			firTotal.add(new Day(timePoint), first);
			empTotal.add(new Day(timePoint), empty);
			bookTotal.add(new Day(timePoint),total);
		}
		
		tsc.addSeries(firTotal);
		tsc.addSeries(busTotal);
		tsc.addSeries(premTotal);
		tsc.addSeries(econTotal);
		tsc.addSeries(bookTotal);
		tsc.addSeries(empTotal);
		
		return tsc; 
	}
	
	private TimeSeriesCollection createChart2() {
		TimeSeriesCollection tsc = new TimeSeriesCollection(); 
		TimeSeries bookTotal = new TimeSeries("Total");
		TimeSeries econTotal = new TimeSeries("Economy"); 
		TimeSeries busTotal = new TimeSeries("Business");
		TimeSeries premTotal = new TimeSeries("Premium");
		TimeSeries firTotal = new TimeSeries("First");
		TimeSeries empTotal = new TimeSeries("Seats Available");
		Calendar cal = GregorianCalendar.getInstance(); 
		
		int economy = 0;
		int premium = 0;
		int business = 0; 
		int first = 0;
		int total = 0;
		int empty = 0;
		for (int i = Constants.FIRST_FLIGHT; i <= Constants.DURATION; i++) {
			
			cal.set(2016,0,i,6,0);
	        Date timePoint = cal.getTime();
	        
	        economy = numEconomy[i];
	        premium = numPremium[i];
	        business = numBusiness[i];
	        first = numFirst[i];
	        total = numTotal[i];
	        empty = numEmpty[i];
	        
	        busTotal.add(new Day(timePoint), business);
			econTotal.add(new Day(timePoint), economy);
			premTotal.add(new Day(timePoint), premium);
			firTotal.add(new Day(timePoint), first);
			empTotal.add(new Day(timePoint), empty);
			bookTotal.add(new Day(timePoint),total);
		}
		tsc.addSeries(firTotal);
		tsc.addSeries(busTotal);
		tsc.addSeries(premTotal);
		tsc.addSeries(econTotal);
		tsc.addSeries(bookTotal);
		tsc.addSeries(empTotal);
		
		return tsc; 
	}
	private JFreeChart createChart(final XYDataset dataset, boolean chart1) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
        		"Simulation Results", "Days", "Passengers", dataset, true, true, false);
        final XYPlot plot = result.getXYPlot();
        if (chart1) {
	        plot.getRenderer().setSeriesPaint(0, Color.black);
	        plot.getRenderer().setSeriesPaint(1, Color.blue);
	        plot.getRenderer().setSeriesPaint(2, Color.cyan);
	        plot.getRenderer().setSeriesPaint(3, Color.gray);
	        plot.getRenderer().setSeriesPaint(4, Color.green);
	        plot.getRenderer().setSeriesPaint(5, Color.red);
        }
        else {
            plot.getRenderer().setSeriesPaint(0, Color.black);
            plot.getRenderer().setSeriesPaint(1, Color.red);
        }
        ValueAxis domain = plot.getDomainAxis();
        domain.setAutoRange(true);
        ValueAxis range = plot.getRangeAxis();
        range.setAutoRange(true);
        return result; 
        }
   }
