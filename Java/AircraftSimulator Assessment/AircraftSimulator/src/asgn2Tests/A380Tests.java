/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;
import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;
import asgn2Aircraft.AircraftException;

/**
 * @author Callum
 *
 */
public class A380Tests {
	private final int CANCELLATION_TIME = 40;
	private final int BOOKING_TIME = 20;
	private final int DEPARTURE_TIME = 60;
	private final int CONFIRMATION_TIME = 30;
	private final int QUEUE_TIME = 45;
	private final int REFUSAL_TIME = 50;

	private final int FIRST = 2;
	private final int BUSINESS = 2;
	private final int PREMIUM = 3;
	private final int ECONOMY = 3;

	private final int FIRST_CAPACITY = 3;
	private final int BUSINESS_CAPACITY = 3;
	private final int PREMIUM_CAPACITY = 4;
	private final int ECONOMY_CAPACITY = 4;

	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 */
	@Test
	public void testA380StringIntDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}
	 * .
	 */
	@Test
	public void testA380StringIntIntIntIntIntDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsFlightCodeException() throws AircraftException {
		Aircraft a = new A380(null, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsDepartureTimeException() throws AircraftException {
		int DUMMY_DEPARTURE_TIME = 0;
		Aircraft a = new A380("test", DUMMY_DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsFirstIsLessThanZeroException() throws AircraftException {
		int DUMMY_FIRST = -1;
		Aircraft a = new A380("test", DEPARTURE_TIME, DUMMY_FIRST, BUSINESS, PREMIUM, ECONOMY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsBusinessIsLessThanZeroException() throws AircraftException {
		int DUMMY_BUSINESS = -1;
		Aircraft a = new A380("test", DEPARTURE_TIME, FIRST, DUMMY_BUSINESS, PREMIUM, ECONOMY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsPremiumIsLessThanZeroException() throws AircraftException {
		int DUMMY_PREMIUM = -1;
		Aircraft a = new A380("test", DEPARTURE_TIME, FIRST, BUSINESS, DUMMY_PREMIUM, ECONOMY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
	 * .
	 * 
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testAircraftThrowsEconomyIsLessThanZeroException() throws AircraftException {
		int DUMMY_ECONOMY = -1;
		Aircraft a = new A380("test", DEPARTURE_TIME, FIRST, BUSINESS, PREMIUM, DUMMY_ECONOMY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test(expected = AircraftException.class)
	public void testCancelBookingPassengerNotRecordedException() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new First(BOOKING_TIME, DEPARTURE_TIME);
		p.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testCancelBookingDecementFirst() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new First(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		assertEquals(0, a.getNumFirst());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testCancelBookingDecementBusiness() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Business(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		assertEquals(0, a.getNumBusiness());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testCancelBookingDecementPremium() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		assertEquals(0, a.getNumPremium());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testCancelBookingDecementEconomy() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		assertEquals(0, a.getNumEconomy());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmBookingIconfirmedsException() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		p.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmlBookingIsFlownException() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Business(BOOKING_TIME, DEPARTURE_TIME);
		p.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		p.flyPassenger(DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmBookingIsRefusedException() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		p.refusePassenger(REFUSAL_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmlBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testConfirmBookingIncrementFirst() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new First(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumFirst());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmlBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testConfirmBookingIncrementBusiness() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Business(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumBusiness());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmlBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testConfirmBookingIncrementPremium() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumPremium());
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#confirmlBooking(asgn2Passengers.Passenger, int)}
	 * .
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testConfirmBookingIncrementEconomy() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		a.cancelBooking(p, CANCELLATION_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumEconomy());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#finalState()}.
	 */
	@Test
	public void testFinalStateDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testFlightEmpty() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		int total = a.getNumFirst() + a.getNumBusiness() + a.getNumPremium() + a.getNumEconomy();
		assertEquals(0, total);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testFlightFull() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME, FIRST, BUSINESS, PREMIUM, ECONOMY);
		int cap = FIRST + BUSINESS + PREMIUM + ECONOMY;
		assertEquals(10, cap);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getBookings()}.
	 */
	@Test
	public void testGetBookingsDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testIncrementsPassengers() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", CANCELLATION_TIME);
		Passenger pEcon = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		Passenger pBus = new Business(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(pEcon, CONFIRMATION_TIME);
		a.confirmBooking(pBus, CONFIRMATION_TIME);
		int total = a.getNumFirst() + a.getNumBusiness() + a.getNumPremium() + a.getNumEconomy();
		assertEquals(total, a.getNumPassengers());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPassengers()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testGetNumPassengers() throws AircraftException, PassengerException {
		int cancellationTime = 40;
		int bookingTime = 20;
		int departureTime = 50;
		int confirmationTime = 30;
		Aircraft a = new A380("test", cancellationTime);
		Passenger pEcon = new Economy(bookingTime, departureTime);
		Passenger pBus = new Business(bookingTime, departureTime);
		a.confirmBooking(pEcon, confirmationTime);
		a.confirmBooking(pBus, confirmationTime);
		assertEquals(2, a.getNumPassengers());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumBusiness()}.
	 */
	@Test
	public void testGetNumBusinessDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumEconomy()}.
	 */
	@Test
	public void testGetNumEconomyDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumFirst()}.
	 */
	@Test
	public void testGetNumFirstDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPremium()}.
	 */
	@Test
	public void testGetNumPremiumDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getPassengers()}.
	 * 
	 * @throws AircraftException
	 */
	@Test
	public void testGetPassengers() throws AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		assertFalse(a.getPassengers() == a.getPassengers());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getStatus(int)}.
	 */
	@Test
	public void testGetStatusDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#hasPassenger(asgn2Passengers.Passenger)}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testHasPassenger() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Business(BOOKING_TIME, DEPARTURE_TIME);
		p.isConfirmed();
		a.hasPassenger(p);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#initialState()}.
	 */
	@Test
	public void testInitialStateDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testSeatsAvailableFirst() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p1 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p2 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p3 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p4 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p5 = new First(BOOKING_TIME, DEPARTURE_TIME);

		a.confirmBooking(p1, CONFIRMATION_TIME);
		a.confirmBooking(p2, CONFIRMATION_TIME);
		a.confirmBooking(p3, CONFIRMATION_TIME);
		a.confirmBooking(p4, CONFIRMATION_TIME);
		a.confirmBooking(p5, CONFIRMATION_TIME);
		assertFalse(a.getNumFirst() < FIRST_CAPACITY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testSeatsAvailableBusiness() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p1 = new Business(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p2 = new Business(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p3 = new Business(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p4 = new Business(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p5 = new Business(BOOKING_TIME, DEPARTURE_TIME);

		a.confirmBooking(p1, CONFIRMATION_TIME);
		a.confirmBooking(p2, CONFIRMATION_TIME);
		a.confirmBooking(p3, CONFIRMATION_TIME);
		a.confirmBooking(p4, CONFIRMATION_TIME);
		a.confirmBooking(p5, CONFIRMATION_TIME);
		assertFalse(a.getNumBusiness() < BUSINESS_CAPACITY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testSeatsAvailablePremium() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p1 = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p2 = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p3 = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p4 = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p5 = new Premium(BOOKING_TIME, DEPARTURE_TIME);

		a.confirmBooking(p1, CONFIRMATION_TIME);
		a.confirmBooking(p2, CONFIRMATION_TIME);
		a.confirmBooking(p3, CONFIRMATION_TIME);
		a.confirmBooking(p4, CONFIRMATION_TIME);
		a.confirmBooking(p5, CONFIRMATION_TIME);
		assertFalse(a.getNumPremium() < PREMIUM_CAPACITY);
	}

	/**
	 * Test method for
	 * {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * 
	 * @throws PassengerException
	 * @throws AircraftException
	 */
	@Test
	public void testSeatsAvailableEconomy() throws PassengerException, AircraftException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p1 = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p2 = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p3 = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p4 = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		Passenger p5 = new Economy(BOOKING_TIME, DEPARTURE_TIME);

		a.confirmBooking(p1, CONFIRMATION_TIME);
		a.confirmBooking(p2, CONFIRMATION_TIME);
		a.confirmBooking(p3, CONFIRMATION_TIME);
		a.confirmBooking(p4, CONFIRMATION_TIME);
		a.confirmBooking(p5, CONFIRMATION_TIME);
		assertFalse(a.getNumEconomy() < ECONOMY_CAPACITY);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#toString()}.
	 */
	@Test
	public void testToStringDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeBookingsEconomyToPremiumAddsToTotal() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumEconomy());
		assertEquals(0, a.getNumPremium());
		a.upgradeBookings();
		assertEquals(0, a.getNumEconomy());
		assertEquals(1, a.getNumPremium());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeBookingsPremiumToBusinesstAddsToTotal() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumPremium());
		assertEquals(0, a.getNumBusiness());
		a.upgradeBookings();
		assertEquals(0, a.getNumPremium());
		assertEquals(1, a.getNumBusiness());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * 
	 * @throws AircraftException
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeBookingsBusinessToFirstAddsToTotal() throws AircraftException, PassengerException {
		Aircraft a = new A380("test", DEPARTURE_TIME);
		Passenger p = new Business(BOOKING_TIME, DEPARTURE_TIME);
		a.confirmBooking(p, CONFIRMATION_TIME);
		assertEquals(1, a.getNumBusiness());
		assertEquals(0, a.getNumFirst());
		a.upgradeBookings();
		assertEquals(0, a.getNumBusiness());
		assertEquals(1, a.getNumFirst());
	}
}
