/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

/**
 * @author MASON
 *
 */
public class BusinessTests {

	private final int BOOKING_TIME = 20;
	private final int DEPARTURE_TIME = 50;
	private final int DEPARTURE_LESS_THAN_BOOKING = 10;
	private final int NEGATIVE_NUMBER = -1;

	/**
	 * Test method for {@link asgn2Passengers.Business#noSeatsMsg()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testNoSeatsMsg() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals("No seats available in Business", testBusiness.noSeatsMsg());
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCurrentClass() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testBusiness instanceof Business);
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeClass() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testBusiness.upgrade() instanceof First);
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testBusinessConstructorOutOfBoundsBooking() throws PassengerException {
		Passenger testBusiness = new Business(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testBusinessConstructorOutOfBoundsDeparture() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testBusinessConstructorOutOfBoundsDepLessBooking() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testBusinessConstructorInitialState() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals(true, testBusiness.isNew());
	}
}
