/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author MASON
 *
 */
public class EconomyTests {

	private final int BOOKING_TIME = 20;
	private final int DEPARTURE_TIME = 50;
	private final int DEPARTURE_LESS_THAN_BOOKING = 10;
	private final int NEGATIVE_NUMBER = -1;

	/**
	 * Test method for {@link asgn2Passengers.Economy#noSeatsMsg()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testNoSeatsMsg() throws PassengerException {
		Passenger testEco = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals("No seats available in Economy", testEco.noSeatsMsg());
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCurrentClass() throws PassengerException {
		Passenger testEco = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testEco instanceof Economy);
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeClass() throws PassengerException {
		Passenger testEco = new Economy(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testEco.upgrade() instanceof Premium);
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#Economy(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testEconomyConstructorOutOfBoundsBooking() throws PassengerException {
		Passenger testBusiness = new Business(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#Economy(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testEconomyConstructorOutOfBoundsDeparture() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#Economy(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testEconomyConstructorOutOfBoundsDepLessBooking() throws PassengerException {
		Passenger testBusiness = new Business(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#Economy(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testEconomyConstructorInitialState() throws PassengerException {
		Passenger testEconomy = new Business(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals(true, testEconomy.isNew());
	}
}
