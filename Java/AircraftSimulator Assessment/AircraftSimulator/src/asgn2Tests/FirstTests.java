/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

/**
 * @author MASON
 *
 */
public class FirstTests {

	private final int BOOKING_TIME = 20;
	private final int DEPARTURE_TIME = 50;
	private final int DEPARTURE_LESS_THAN_BOOKING = 10;
	private final int NEGATIVE_NUMBER = -1;
	private final int CANCELLATION_TIME = 40;
	private final int QUEUE_TIME = 35;
	private final int CONFIRMATION_TIME = 35;
	private final int REFUSAL_TIME = 30;

	/**
	 * Test method for {@link asgn2Passengers.First#noSeatsMsg()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testNoSeatsMsgWorks() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals("No seats available in First", testPass.noSeatsMsg());
	}

	/**
	 * Test method for {@link asgn2Passengers.First#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCurrentClass() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testPass instanceof First);
	}

	/**
	 * Test method for {@link asgn2Passengers.First#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeClass() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testPass.upgrade() instanceof First);
	}

	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFirstConstructorOutOfBoundsBooking() throws PassengerException {
		Passenger testPass = new First(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFirstConstructorOutOfBoundsDeparture() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFirstConstructorOutOfBoundsDepLessBooking() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.First#First()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testFirstConstructorInitialState() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals(true, testPass.isNew());
	}

	// Passenger Tests

	/**
	 * Test method for {@link asgn2Passengers.Passenger#Passenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPassengerConstructorBookingOutOfBounds() throws PassengerException {
		Passenger testPass = new First(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#Passenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPassengerConstructorDepartureOutOfBounds() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#Passenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPassengerConstructorDepLessThanBooking() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#Passenger()}.
	 */
	@Test
	public void testPassengerNoArgConstructorDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCancelSeatCancelFunction() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(BOOKING_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(CANCELLATION_TIME);
		assertEquals(true, testPass.isNew());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testCancelSeatOutOfBoundsMinus() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(BOOKING_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testCancelSeatCancelOutOfBoundsDepLessThan() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
		testPass.cancelSeat(CANCELLATION_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testCancelSeatNewStateException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(CANCELLATION_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testCancelSeatQueuedStateException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(CANCELLATION_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCancelSeatPreviousStateReset() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(CANCELLATION_TIME);
		assertEquals(false, testPass.isConfirmed());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testConfirmSeatFunction() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertEquals(true, testPass.isConfirmed());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmSeatOutOfBoundsConfirm() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmSeatOutOfBoundsDeparture() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmSeatDepLessThan() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testConfirmSeatConfirmedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testConfirmSeatPreviousStateReset() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertEquals(false, testPass.isNew());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testFlyPassengerFunction() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
		assertEquals(true, testPass.isFlown());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFlyPassengerOutOfBounds() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFlyPassengerIsNewException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFlyPassengerIsQueuedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFlyPassengerIsRefusedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testFlyPassengerIsFlownException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testFlyPassengerPreviousStateReset() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
		assertEquals(false, testPass.isConfirmed());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getBookingTime()}.
	 */
	@Test
	public void testGetBookingTimeDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getConfirmationTime()}.
	 */
	@Test
	public void testGetConfirmationTimeDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getDepartureTime()}.
	 */
	@Test
	public void testGetDepartureTimeDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getEnterQueueTime()}.
	 */
	@Test
	public void testGetEnterQueueTimeDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getExitQueueTime()}.
	 */
	@Test
	public void testGetExitQueueTimeDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#getPassID()}.
	 */
	@Test
	public void testGetPassIDDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#isConfirmed()}.
	 */
	@Test
	public void testIsConfirmedDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#isFlown()}.
	 */
	@Test
	public void testIsFlownDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#isNew()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testIsNewDontTest() throws PassengerException {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#isQueued()}.
	 */
	@Test
	public void testIsQueuedDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#isRefused()}.
	 */
	@Test
	public void testIsRefusedDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testQueuePassengerFunction() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		assertEquals(true, testPass.isQueued());
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerOutOfBounds() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerOutOfBoundsDepLessQueue() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerIsQueuedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerIsConfirmedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerIsRefusedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testQueuePassengerIsFlownException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testQueuePassengerPreviousStateReset() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		assertEquals(false, testPass.isNew());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testRefusePassengerFunctionIsNew() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		assertEquals(true, testPass.isRefused());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testRefusePassengerFunctionIsQueued() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		assertEquals(true, testPass.isRefused());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testRefusePassengerOutOfBounds() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testRefusePassengerOutOfBoundsDepLessRef() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
		testPass.refusePassenger(REFUSAL_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testRefusePassengerIsConfirmedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testRefusePassengerIsRefusedException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testRefusePassengerIsFlownException() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.flyPassenger(DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testRefusePassengerResetPreviousStateNew() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		assertEquals(false, testPass.isNew());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testRefusePassengerResetPreviousStateQueued() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.refusePassenger(REFUSAL_TIME);
		assertEquals(false, testPass.isQueued());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#toString()}.
	 */
	@Test
	public void testToStringDontTest() {
		assertTrue(true);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasConfirmed()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testWasConfirmed() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		testPass.cancelSeat(CANCELLATION_TIME);
		assertEquals(CONFIRMATION_TIME, testPass.getConfirmationTime());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasQueued()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testWasQueued() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		assertEquals(QUEUE_TIME, testPass.getEnterQueueTime());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasQueued()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testWasQueuedExitQueueTime() throws PassengerException {
		Passenger testPass = new First(BOOKING_TIME, DEPARTURE_TIME);
		testPass.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		testPass.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertEquals(QUEUE_TIME, testPass.getEnterQueueTime());
	}

	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#copyPassengerState(asgn2Passengers.Passenger)}
	 * .
	 */
	@Test
	public void testCopyPassengerState() {
		assertTrue(true);
	}

}
