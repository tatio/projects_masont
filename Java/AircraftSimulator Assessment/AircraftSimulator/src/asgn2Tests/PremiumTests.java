/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author MASON
 *
 */
public class PremiumTests {

	private final int BOOKING_TIME = 20;
	private final int DEPARTURE_TIME = 50;
	private final int DEPARTURE_LESS_THAN_BOOKING = 10;
	private final int NEGATIVE_NUMBER = -1;

	/**
	 * Test method for {@link asgn2Passengers.Premium#noSeatsMsg()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testNoSeatsMsg() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals("No seats available in Premium", testPrem.noSeatsMsg());
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testCurrentClass() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testPrem instanceof Premium);
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testUpgradeClass() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		assertTrue(testPrem.upgrade() instanceof Business);
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPremiumConstructorOutOfBoundsBooking() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, NEGATIVE_NUMBER);
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPremiumConstructorOutOfBoundsDeparture() throws PassengerException {
		Passenger testPrem = new Premium(NEGATIVE_NUMBER, DEPARTURE_TIME);
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public void testPremiumConstructorOutOfBoundsDepLessBooking() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, DEPARTURE_LESS_THAN_BOOKING);
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public void testPremiumConstructorInitialState() throws PassengerException {
		Passenger testPrem = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		assertEquals(true, testPrem.isNew());
	}
}
