package asgn1Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn1Election.CandidateIndex;
import asgn1Election.ElectionException;

/**
 * @author MASON
 *
 */
public class CandidateIndexTests {

    /**
     * Test method for {@link asgn1Election.CandidateIndex#inRange()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testInRangeOverMaxRange() throws ElectionException {
        int value = 16;
        assertFalse(CandidateIndex.inRange(value));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#inRange()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testInRangeInBound() throws ElectionException {
        int value = 7;
        assertTrue(CandidateIndex.inRange(value));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#inRange()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testInRangeBelowMinRange() throws ElectionException {
        int value = -1;
        assertFalse(CandidateIndex.inRange(value));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#CandidateIndex()}.
     */
    @Test
    public void testCandidateIndexDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#compareTo()}.
     */
    @Test
    public void testCompareToEqualToIndex() {
        CandidateIndex testCopy = new CandidateIndex(3);
        CandidateIndex testCopyTwo = new CandidateIndex(3);
        assertEquals(0, testCopy.compareTo(testCopyTwo));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#compareTo()}.
     */
    @Test
    public void testCompareToLessThanIndex() {
        CandidateIndex testCopy = new CandidateIndex(2);
        CandidateIndex testCopyTwo = new CandidateIndex(5);
        assertEquals(-1, testCopy.compareTo(testCopyTwo));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#compareTo()}.
     */
    @Test
    public void testCompareToGreaterThanIndex() {
        CandidateIndex testCopy = new CandidateIndex(7);
        CandidateIndex testCopyTwo = new CandidateIndex(1);
        assertEquals(1, testCopy.compareTo(testCopyTwo));
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#copy()}.
     */
    @Test
    public void testCopyDeepCopyFunctionality() {
        CandidateIndex testCopy = new CandidateIndex(5);
        testCopy.copy();
        CandidateIndex newTestCopy = testCopy.copy();
        assertEquals(testCopy.toString(), newTestCopy.toString());
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#incrementIndex()}.
     */
    @Test
    public void testIncrementIndexVoteCount() {
        CandidateIndex candIndex = new CandidateIndex(1);
        candIndex.incrementIndex();
        assertEquals(candIndex.toString(), "2");
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#setValue()}.
     */
    @Test
    public void testSetValueDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.CandidateIndex#toString()}.
     */
    @Test
    public void testToStringDontTest() {
        assertTrue(true);
    }
}
