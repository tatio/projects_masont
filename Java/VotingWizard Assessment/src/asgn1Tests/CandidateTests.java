/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn1Election.Candidate;
import asgn1Election.ElectionException;

/**
 * @author MASON
 *
 */
public class CandidateTests {

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateVoteCountLessThanZero() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", "Dummy Political Party", "DPP", -1);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateVoteCountOutOfBounds() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", "Dummy Political Party", "DPP", 16);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateCandNameNotNull() throws ElectionException {
        Candidate cand = new Candidate(null, "Dummy Political Party", "DPP", 1);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateCandNameNotEmpty() throws ElectionException {
        Candidate cand = new Candidate("", "Dummy Political Party", "DPP", 1);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateCandPartyNotEmpty() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", "", "DPP", 1);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateCandPartyNotNull() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", null, "DPP", 1);
    }

    /**
     * Test method for
     * {@link asgn1Election.Candidate#Candidate(java.lang.String, java.lang.String, java.lang.String, int)}
     * .
     * 
     * @throws ElectionException
     */
    @Test(expected = ElectionException.class)
    public void testCandidateCandAbbrevNotEmpty() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", "Dummy Political Party", "", 1);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#candidateListing()}.
     */
    @Test
    public void testCandidateListingDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#copy()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testCopyDeepCopyFunctionality() throws ElectionException {
        Candidate cand = new Candidate("Dummy Candidate", "Dummy Political Party", "DPP", 1);
        cand.copy();
        Candidate newCand = cand.copy();
        assertEquals(cand.toString(), newCand.toString());
    }

    /**
     * Test method for {@link asgn1Election.Candidate#getName()}.
     */
    @Test
    public void testGetNameDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#getParty()}.
     */
    @Test
    public void testGetPartyDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#getVoteCount()}.
     */
    @Test
    public void testGetVoteCountDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#getVoteCountString()}.
     */
    @Test
    public void testGetVoteCountStringDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.Candidate#incrementVoteCount()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testIncrementVoteCount() throws ElectionException {
        Candidate candidates = new Candidate("Dummy Candidate", "Dummy Political Party", "DPP", 1);
        candidates.incrementVoteCount();
        assertEquals(2, candidates.getVoteCount());
    }

    /**
     * Test method for {@link asgn1Election.Candidate#toString()}.
     */
    @Test
    public void testToStringDontTest() {
        assertTrue(true);
    }

}
