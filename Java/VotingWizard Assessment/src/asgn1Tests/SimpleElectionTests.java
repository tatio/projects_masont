/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import asgn1Election.Election;
import asgn1Election.ElectionException;
import asgn1Election.SimpleElection;
import asgn1Election.Vote;
import asgn1Election.VoteList;
import asgn1Util.NumbersException;

/**
 * @author MASON
 *
 */
public class SimpleElectionTests {

    /**
     * Test method for {@link asgn1Election.SimpleElection#findWinner()}.
     */
    @Test
    public void testFindWinner() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link asgn1Election.SimpleElection#isFormal(asgn1Election.Vote)}.
     * 
     * @throws FileNotFoundException
     * @throws ElectionException
     * @throws IOException
     * @throws NumbersException
     */
    @Test
    public void testIsFormalFuctionality()
            throws FileNotFoundException, ElectionException, IOException, NumbersException {

        Election testElection = new SimpleElection("Demo");
        Vote isFormalList = new VoteList(4);
        isFormalList.addPref(1);
        isFormalList.addPref(2);
        isFormalList.addPref(3);
        isFormalList.addPref(4);
        boolean checkFormality = testElection.isFormal(isFormalList);
        assertTrue(checkFormality);
    }

    /**
     * Test method for
     * {@link asgn1Election.SimpleElection#isFormal(asgn1Election.Vote)}.
     * 
     * @throws FileNotFoundException
     * @throws ElectionException
     * @throws IOException
     * @throws NumbersException
     */
    @Test
    public void testIsFormalOutOfBounds()
            throws FileNotFoundException, ElectionException, IOException, NumbersException {

        Election testElection = new SimpleElection("Demo");
        Vote isFormalList = new VoteList(4);
        isFormalList.addPref(2);
        isFormalList.addPref(2);
        isFormalList.addPref(3);
        isFormalList.addPref(4);
        boolean checkFormality = testElection.isFormal(isFormalList);
        assertFalse(checkFormality);
    }

    /**
     * Test method for {@link asgn1Election.SimpleElection#clearWinner(int)}.
     */
    @Test
    public void testClearWinnerDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for
     * {@link asgn1Election.SimpleElection#SimpleElection(java.lang.String)}.
     */
    @Test
    public void testSimpleElection() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn1Election.SimpleElection#toString()}.
     */
    @Test
    public void testToStringDontTest() {
        assertTrue(true);
    }

}
