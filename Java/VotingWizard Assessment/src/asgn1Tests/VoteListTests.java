/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn1Election.CandidateIndex;
import asgn1Election.ElectionException;
import asgn1Election.Vote;
import asgn1Election.VoteList;

/**
 * @author MASON
 *
 */
public class VoteListTests {

    CandidateIndex cand; // Object of CandIndex
    int numOfCand = 5; // Random number of Cands
    Vote vote; // Object of vote

    /**
     * 
     */
    @Before
    @Test
    public void buildTestList() {
        vote = new VoteList(numOfCand);
        vote.addPref(4); // cand 1
        vote.addPref(2); // cand 2
        vote.addPref(3); // cand 3
        vote.addPref(5); // cand 4
        vote.addPref(1); // cand 5
    }

    /**
     * Test method for {@link asgn1Election.VoteList#VoteList(int)}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testVoteListDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.VoteList#addPref(int)}.
     */
    @Test
    public void testAddPrefFunctionality() {
        VoteList testList = new VoteList(3);
        testList.addPref(3);
        testList.addPref(2);
        testList.addPref(1);
        String expectedValue = "3 2 1";
        String prefVotes = testList.toString().trim();
        assertEquals(expectedValue, prefVotes);
    }

    /**
     * Test method for {@link asgn1Election.VoteList#addPref(int)}.
     */
    @Test
    public void testAddPrefInRange() {
        VoteList inRangeList = new VoteList(3);
        inRangeList.addPref(1);
        inRangeList.addPref(2);
        assertEquals(true, inRangeList.addPref(3));
    }

    /**
     * Test method for {@link asgn1Election.VoteList#addPref(int)}.
     */
    @Test
    public void testAddPrefOutOfRange() {
        VoteList outOfRangeList = new VoteList(3);
        outOfRangeList.addPref(1);
        outOfRangeList.addPref(2);
        outOfRangeList.addPref(3);
        outOfRangeList.addPref(4);
        outOfRangeList.addPref(5);
        assertEquals(false, outOfRangeList.addPref(6));
    }

    /**
     * Test method for {@link asgn1Election.VoteList#copyVote()}.
     */
    @Test
    public void testCopyVoteFunctionality() {
        vote.copyVote();
        Vote newCopy = vote.copyVote();
        assertEquals(vote.toString(), newCopy.toString());
    }

    /**
     * Test method for {@link asgn1Election.VoteList#getPreference(int)}.
     */
    @Test
    public void testGetPreferenceFunctionality() {
        int candidate = 4;
        CandidateIndex candIndex = vote.getPreference(candidate);
        String output = candIndex.toString();
        assertEquals("1", output);
    }

    /**
     * Test method for {@link asgn1Election.VoteList#invertVote()}.
     */
    @Test
    public void testInvertVoteFunctionality() {
        Vote test = new VoteList(numOfCand);
        test = vote.invertVote();
        // Use code below to output non-invert and invert values
        // String value = test.toString();
        // System.out.println("This is the ballot paper " + "|| " + vote);
        // System.out.println("This is the invert paper " + "|| " + value);
        String expectedInvert = ("5 2 3 1 4");
        assertEquals(expectedInvert, test.toString().trim());

    }

    /**
     * Test method for {@link asgn1Election.VoteList#iterator()}.
     */
    @Test
    public void testIteratorIsNotNull() {
        boolean iteratorCheck = false;
        if (vote.iterator().hasNext()) {
            iteratorCheck = true;
        }
        assertEquals(true, iteratorCheck);
    }

    /**
     * Test method for {@link asgn1Election.VoteList#toString()}.
     */
    @Test
    public void testToStringDontVote() {
        assertTrue(true);
    }
}
