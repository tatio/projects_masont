/**
 * 
 */
package asgn1Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.TreeMap;

import org.junit.Test;

import asgn1Election.Candidate;
import asgn1Election.CandidateIndex;
import asgn1Election.Collection;
import asgn1Election.ElectionException;
import asgn1Election.Vote;
import asgn1Election.VoteCollection;
import asgn1Election.VoteList;

/**
 * @author MASON
 *
 */
public class VoteCollectionTests {

    /**
     * Test method for {@link asgn1Election.VoteCollection#VoteCollection(int)}.
     * 
     * @throws ElectionException
     */
    @Test(expected = Exception.class)
    public void testVoteCollectionAboveRange() throws ElectionException {
        Collection voteCollection = new VoteCollection(16);
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#VoteCollection(int)}.
     * 
     * @throws ElectionException
     */
    @Test(expected = Exception.class)
    public void testVoteCollectionBelowRange() throws ElectionException {
        Collection voteCollection = new VoteCollection(-1);
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#VoteCollection(int)}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testVoteCollectionInRange() throws ElectionException {
        Collection voteCollection = new VoteCollection(7);
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#countPrefVotes(java.util.TreeMap, asgn1Election.CandidateIndex)}
     * .
     */
    @Test
    public void testCountPrefVotesHaveNotDone() {
        assertTrue(true);
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#countPrimaryVotes(java.util.TreeMap)}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testCountPrimaryVotes() throws ElectionException {
        TreeMap<CandidateIndex, Candidate> cds = new TreeMap<CandidateIndex, Candidate>();
        cds.put(1, "Test");

        assertEquals(1, 1);

    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#emptyTheCollection()}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testEmptyTheCollectionFormalCount() throws ElectionException {
        Collection emptyList = new VoteCollection(12);
        emptyList.emptyTheCollection();
        assertEquals(0, emptyList.getFormalCount());
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#emptyTheCollection()}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testEmptyTheCollectionInformalCount() throws ElectionException {
        Collection emptyList = new VoteCollection(6);
        emptyList.emptyTheCollection();
        assertEquals(0, emptyList.getInformalCount());
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#emptyTheCollection()}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testEmptyTheCollectionNumCands() throws ElectionException {
        int numOfCandidate = 0;
        Collection emptyList = new VoteCollection(3);
        Vote emptyVotes = new VoteList(2);
        emptyVotes.addPref(1);
        emptyVotes.addPref(2);

        for (int x : emptyVotes) {
            numOfCandidate++;
        }

        emptyList.emptyTheCollection();

        assertEquals(0, numOfCandidate);

        // assertEquals(0, emptyList.getFormalCount());
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#getFormalCount()}.
     */
    @Test
    public void testGetFormalCountDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for {@link asgn1Election.VoteCollection#getInformalCount()}.
     */
    @Test
    public void testGetInformalCountDontTest() {
        assertTrue(true);
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#includeFormalVote(asgn1Election.Vote)}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testIncludeFormalVote() throws ElectionException {

        VoteCollection testFormalVote = new VoteCollection(4);
        Vote formalVote = new VoteList(2);
        testFormalVote.includeFormalVote(formalVote);
        assertEquals(1, testFormalVote.getFormalCount());
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#includeFormalVote(asgn1Election.Vote)}
     * .
     * 
     * @throws ElectionException
     */
    @Test
    public void testIncludeFormalVoteHigherExpect() throws ElectionException {

        VoteCollection testFormalVote = new VoteCollection(4);
        Vote formalVote = new VoteList(2);
        Vote formalNewVote = new VoteList(2);
        Vote formalAnotherNewVote = new VoteList(2);
        testFormalVote.includeFormalVote(formalVote);
        testFormalVote.includeFormalVote(formalNewVote);
        testFormalVote.includeFormalVote(formalAnotherNewVote);
        assertEquals(3, testFormalVote.getFormalCount());
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#updateInformalCount()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testUpdateInformalCount() throws ElectionException {
        VoteCollection testInformalCount = new VoteCollection(2);
        testInformalCount.updateInformalCount();
        assertEquals(1, testInformalCount.getInformalCount());
    }

    /**
     * Test method for
     * {@link asgn1Election.VoteCollection#updateInformalCount()}.
     * 
     * @throws ElectionException
     */
    @Test
    public void testUpdateInformalCountHigherCount() throws ElectionException {
        VoteCollection testInformalCount = new VoteCollection(2);
        testInformalCount.updateInformalCount();
        testInformalCount.updateInformalCount();
        testInformalCount.updateInformalCount();
        testInformalCount.updateInformalCount();
        assertEquals(4, testInformalCount.getInformalCount());
    }

}
