/**
 * 
 * This file is part of the VotingWizard Project, written as 
 * part of the assessment for CAB302, Semester 1, 2016. 
 * 
 */
package asgn1Election;

import asgn1Util.Strings;

/**
 * 
 * Subclass of <code>Election</code>, specialised to simple, first past the post
 * voting
 * 
 * @author hogan
 * 
 */
public class SimpleElection extends Election {

    Candidate candWinner;

    /**
     * Simple Constructor for <code>SimpleElection</code>, takes name and also
     * sets the election type internally.
     * 
     * @param name
     *            <code>String</code> containing the Election name
     */
    public SimpleElection(String name) {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see asgn1Election.Election#findWinner()
     */
    @Override
    public String findWinner() {
        java.util.Collection<Candidate> candCollection = this.getCandidates();
        this.vc.countPrimaryVotes(cds);
        int highestCount = 0;

        // Allows the first value to enter, from there the highest value is
        // stored, iterating through, to find the winner
        for (Candidate cand : candCollection) {
            int voteNum = cand.getVoteCount();
            if (voteNum > highestCount) {
                highestCount = voteNum;
                candWinner = cand; // Candidate object
            }
        }
        // Outputs the format for the winner
        return this.showResultHeader() + this.reportCountResult() + this.reportWinner(candWinner);
    }

    /*
     * (non-Javadoc)
     * 
     * @see asgn1Election.Election#isFormal(asgn1Election.Vote)
     */
    @Override
    public boolean isFormal(Vote v) {

        int count = 0;
        boolean containsOne = false;
        int numOfCandidate = 0;

        for (int x : v) { // iterator to count candidates
            numOfCandidate++;
        }

        for (int x : v) {
            // Checks out of bounds
            if (x > numOfCandidate || x <= 0) {
                return false;
                // Checks primary vote, storing additional values for checks
            } else if (x == 1) {
                containsOne = true;
                count++;
            }
            // Checks for more than one primary vote
            if (count > 1) {
                return false;
            }
        }
        // Only one primary key & rest of tests have passed the formal check
        if (containsOne || count == 1) {
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String str = this.name + " - Simple Voting";
        return str;
    }

    // Protected and Private/helper methods below///

    /*
     * (non-Javadoc)
     * 
     * @see asgn1Election.Election#clearWinner(int)
     */
    // @Override
    protected Candidate clearWinner(int wVotes) {

        // Did not understand the use of this method for Simple
        return null;
    }

    /*
     * Pref Election int totalVotes = 0;
     * 
     * java.util.Collection<Candidate> candidates = this.getCandidates(); for
     * (Candidate cand : candidates) { totalVotes += cand.getVoteCount(); }
     * 
     * if (totalVotes == (totalVotes / 2) + 1) { return candWinner; } else {
     * return null; }
     */

    /**
     * Helper method to create a string reporting the count result
     * 
     * @return <code>String</code> containing summary of the count
     */
    private String reportCountResult() {
        String str = "\nSimple election: " + this.name + "\n\n" + candidateVoteSummary() + "\n";
        String inf = "Informal";
        String voteStr = "" + this.vc.getInformalCount();
        int length = ElectionManager.DisplayFieldWidth - inf.length() - voteStr.length();
        str += inf + Strings.createPadding(' ', length) + voteStr + "\n\n";

        String cast = "Votes Cast";
        voteStr = "" + this.numVotes;
        length = ElectionManager.DisplayFieldWidth - cast.length() - voteStr.length();
        str += cast + Strings.createPadding(' ', length) + voteStr + "\n\n";
        return str;
    }
}