﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelConsumption_MasonTeitzel
{
/*
* Calculates fuel consumption in l/100km and the equivalent mpg,
* input units of measurement are litres (l) for the fuel used and
* kilometres (km) for the distance travelled
*
* Author: Mason Teitzel, n9440941
* Date: August 2015
*
*/

    /*
     * Declare variables fuelltrs, disttrav, consumption 
     * 
     * Method Welcome
     * Write informative statement - welcome
     * 
     * 
     * Method Fuel
     * Write informative statement - fuelltrs
     * Ask input from the user
     * Store fuel as fuelltrs
     * convert fuelltrs to double
     * 
     * Distance
     * Next informative statement - disttrav
     * Ask input from user
     * Store distance travel as disttrav
     * Convert disttrav to double
     * 
     * 
     * 
     */
    class Program
    {
        //WelcomeMessage method is to display a message at the start of the program
        public static void WelcomeMessage()
        {
            Console.BackgroundColor = ConsoleColor.Red; //changes background colour behind text to red
            Console.Write("\n\n\tWelcome to Fuel Consumption Calculator!\n\n");
            Console.ResetColor(); //resets the colour to default
      
        } //end of WelcomeMessage() method


        //AskFuel method is to ask the user to input an amount to be stored
        public static double AskFuel()
        {
            double fuelltrs = 0;
            bool okay = false;
            string fuelinput = "";
            Console.Write("\nPlease enter amount of fuel used in litres: ");
        do //do while loop to ensure correct data is entered
        {
            fuelinput = Console.ReadLine();
            okay = double.TryParse(fuelinput, out fuelltrs);
            if (okay && fuelltrs < 20) //if double and greater than 20
            {
                Console.WriteLine("\t" + fuelltrs + " is below the minimum value of 20");
                Console.Write("\nPlease re-enter amount of fuel used in litres: ");
                okay = false; // false makes sure the loop repeats

            } //end of if statement

        } while (!okay); //end of do while loop -- loop while false // not okay

        return fuelltrs; //returns the value
        } //end of AskFuel() method


        //AskDistance method is to ask the user to input an amount of data to be stored as well as input data from previous method (fuelltrs)
        public static double AskDistance(double fuelltrs) //data from previous method
        {
            double disttrav = 0;
            bool okay = false;
            string distinput = "";
            Console.Write("\nPlease enter distance travelled in kilometres: ");
            do
            {
                distinput = Console.ReadLine();
                okay = double.TryParse(distinput, out disttrav);
                if (okay && disttrav < fuelltrs * 8) //if true and variable is greater than the fuelltrs times 8
                {
                    Console.WriteLine("\t" + disttrav + " is below the minimum value of " + (fuelltrs * 8)); //displays the current value and expected value
                    Console.Write("\nPlease re-enter a number greater than " + (fuelltrs * 8) + ": "); //prompts the user to enter expected value
                    okay = false; //false ensures the loop repeats
                }
            } while (!okay); //end of do while loop
            return disttrav; //returns the value 
        } //end of AskDistance() method

        //FuelConsumption method takes the values from previous methods to calculate the fuel consumption
        public static double FuelConsumption(double fuelltrs, double disttrav) //the values from previous methods
        {
            double consumption = 0.0;
            consumption = (fuelltrs / disttrav * 100); //declares a new variables using the user input data
            return consumption; //returns the value of the calculation
        } //end of FuelConsumption() method

        //MpgConversion method takes the value from the previous method to calculate the MPG
        public static double MpgConversion(double consumption) //value from previous method
        {
            double kmtompg = 0.0;
            kmtompg = (282.48 / consumption); //calculates the km to mpg using previous calculations
            return kmtompg; //returns the value
        } //end of MpgConversion() method

        //OutputResult method gives the output of all the data stored and calculated back to the user
        static void OutputResult(double consumption, double kmtompg) //values from previous methods
        {
            consumption = Math.Round(consumption, 2); //rounds to 2 decimal places as {0:f4} doesn't seem to work
            Console.Write("\nYour fuel consumption rate is " + consumption + "lt/100km\n"); //outputs the fuel consumption data
            kmtompg = Math.Round(kmtompg, 2);
            Console.Write("  which is eqivalent to " + kmtompg + "mpg\n"); //outputs the kmtompg data
        } //end of OutputResult() method

        //AshRepeat method asks the user if they wish to make another calculation
        public static bool AskRepeat()
        {
            string inputrestart = "";
            bool restart = false;
                do
                {
                    Console.Write("\n\nAnother calculation <Enter Y or N>: ");
                    inputrestart = Console.ReadLine();
                    if (inputrestart.ToUpper() == "Y") //if the data is equal to y upper or lower
                    {
                        return true; //starts back at the first method
                    }
                    else if(inputrestart.ToUpper() == "N")
                    {
                        return false; //prompts the user for key
                    }
                } while (!restart); //while false loop

            return restart; //return the value entered
        } //end of AskRepeat() method

        //Main holds all the methods and loops according to user input
        static void Main(string[] args) //main Method
        {
            double fuelltrs = 0, disttrav = 0, consumption  = 0, kmtompg = 0;
            bool restart = false;

            do
            {
                WelcomeMessage(); //Welcome method
                fuelltrs = AskFuel(); //fuel method
                disttrav = AskDistance(fuelltrs); //distance method containing fuelltrs
                consumption = FuelConsumption(fuelltrs, disttrav); //consumption method containing fuelltrs and disttrav
                kmtompg = MpgConversion(consumption); //mpg method containing consumption
                OutputResult(consumption, kmtompg); //output method containing consumption and kmtompg
                restart = AskRepeat(); //restart method

            } while (restart); //while restart is true
            //the exit process, closes when out of the loop
            Console.Write("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}
