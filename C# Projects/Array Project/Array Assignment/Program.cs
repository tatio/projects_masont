﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array_Assignment
{
    class Program
    {
        enum Months { January = 1, February, March, April, May, June, July, August, September, October, November, December }

        static int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        const int MONTHS_IN_YEAR = 12;

        static void 
            Main(string[] args)
        {
            Welcome();

            int[][] rainfall = new int[MONTHS_IN_YEAR][];
            int[] daysNoRain = new int[MONTHS_IN_YEAR];
            int[] DaysRained = new int[MONTHS_IN_YEAR];

            int monthInput = 0;

            RainData(rainfall);
            RainPopulate(rainfall);
            PrintCalendar(rainfall);
            monthInput = InputValue();
            GenerateYearlyRain(daysInMonth, daysNoRain, DaysRained);
            GenerateYearlyNoRain(rainfall, daysNoRain);
            FindAverageRain(rainfall[monthInput - 1]);
            OutputRainDays(daysNoRain, DaysRained, monthInput);
            OutputRainMax(rainfall, monthInput);
            OutputDayMax(rainfall, monthInput);
            OutputAverage(rainfall, monthInput);
            ExitProgram();

        }//end Main

        /// <summary>
        /// Gives a welcome message to the user
        /// </summary>
        static void Welcome()
        {
            Console.WriteLine("\n\n\t Welcome to Yearly Rainfall Report \n");
        }//end Welcome


        /// <summary>
        /// This populates the second dimension of the array, corresponding to the day values
        /// </summary>
        /// <param name="rainfall">Populated with daysInMonth for the second dimension (the days)</param>
        public static void RainData(int[][] rainfall)
        {
            for (int i = 0; i < MONTHS_IN_YEAR; i++)
            {
                rainfall[i] = new int[daysInMonth[i]];
            }
        }//end Raindata

        /// <summary>
        /// This method randoms a value between 0 and 3 in each of the j (day) array,
        /// if the value is equal to 3 (a 25% chance) the rainfall otherwise the then
        /// it randoms another value between 1 and 28, which gives you value stored
        /// in [j](the day) is 0 (didn't rain)
        /// </summary>
        /// <param name="rainfall">Populating ALL the days with rainfall data</param>
        public static void RainPopulate(int[][] rainfall)
        {
            Random randomChance = new Random();
            Random randomRain = new Random();

            for (int i = 0; i < MONTHS_IN_YEAR; i++)
            {
                for (int j = 0; j < rainfall[i].Length; j++)
                {
                    int RainChance = randomChance.Next(4);

                    if (RainChance == 3)
                    {
                        int rainAmount = randomRain.Next(1, 28);
                        rainfall[i][j] = rainAmount;
                    }
                }
            }
        }//end RainPopulate

        /// <summary>
        /// This method prints the Months corresponding to the index, with the days, as well as the rain data stored
        /// on any day it doesn't rain (a 0 value) print the values
        /// </summary>
        /// <param name="rainfall">Contains the rainfall data for each day in all the months</param>
        static void PrintCalendar(int[][] rainfall)
        {
            for (int i = 0; i < MONTHS_IN_YEAR; i++)
            {
                Console.WriteLine("\n\n\t\t\t" + (Months)(i + 1) + " " + daysInMonth[i] + " days\n");
                Console.Write("\t\t\t");
                for (int j = 0; j < rainfall[i].Length; j++)
                {
                    if (rainfall[i][j] != 0)
                    {
                        Console.Write(rainfall[i][j] + " ");
                    }
                }
            }
        }//end PrintCalendar

        /// <summary>
        /// Asks the user to input a value corresponding to the month (1-12)
        /// The user can only input a INT between 1 and 12 or asks for input again
        /// </summary>
        /// <returns>Returns the value input by the user</returns>
        public static int InputValue()
        {
            int monthnum = 0;
            string monthinput = "";
            bool okay = false;

            Console.Write("\n\n\n\nEnter a value between 1 and 12 which corresponds to a month of the year: ");

            do
            {
                monthinput = Console.ReadLine();
                okay = int.TryParse(monthinput, out monthnum);
                if (okay && monthnum > 12 || monthnum < 1)
                {
                    Console.Write("Please enter a valid number <Between 1 & 12>: ");
                    okay = false;
                }
            } while (!okay);
            return monthnum;
        }//end InputValue

        /// <summary>
        /// This method finds the days it didn't rain by searching through the days with the 
        /// value of 0 and counts them
        /// </summary>
        /// <param name="rainfall">Used to navigate through the days to find 0</param>
        /// <returns>Returns the counter of all the days it didn't rain</returns>
        public static int FindDaysWithoutRain(int[] rainfall)
        {
            int daysNoRain = 0;

            for (int j = 0; j < rainfall.Length; j++)
            {
                if (rainfall[j] == 0)
                {
                    daysNoRain++;
                }
            }
            return daysNoRain;
        }//end DaysWithoutRain

        /// <summary>
        /// This method calculates the days it did rain, by taking away the days by the days it didn't rain
        /// </summary>
        /// <param name="daysInMonth">Contains the amount days in each month</param>
        /// <param name="daysNoRain">Contains the days it didn't rain</param>
        /// <param name="DaysRained"> Gaining value from the calculation</param>
        static void GenerateYearlyRain(int[] daysInMonth, int[] daysNoRain, int[] DaysRained)
        {
            for (int i = 0; i < MONTHS_IN_YEAR; i++)
            {
                DaysRained[i] = daysInMonth[i] - daysNoRain[i];
            }
        }//end of GenerateYearlyRain

        /// <summary>
        /// This appends an array with values for the days it didn't rain
        /// </summary>
        /// <param name="rainfall">Used to navigate through the days</param>
        /// <param name="daysNoRain">Gained the values for the days it didn't rain from previous method</param>
        static void GenerateYearlyNoRain(int[][] rainfall, int[] daysNoRain)
        {
            for (int i = 0; i < MONTHS_IN_YEAR; i++)
            {
                daysNoRain[i] = FindDaysWithoutRain(rainfall[i]);
            }
        }//end of GenerateYearlyNoRain

        /// <summary>
        /// This method is used to find the max rainfall for each month
        /// </summary>
        /// <param name="rainfall">Used to navigate through the days</param>
        /// <returns>Returns the highest value from the for loop</returns>
        static int FindMax(int[] rainfall)
        {
            int max = 0;

            for (int j = 0; j < rainfall.Length; j++)
            {
                if (rainfall[j] > max)
                {
                    max = rainfall[j];
                }
            }
            return max;
        }//end of FindMax

        /// <summary>
        /// This method is used to find the day associated with the highest amount of rainfall
        /// </summary>
        /// <param name="rainfall">Used to navigate through the days</param>
        /// <returns>Returns the index value for the day with the highest rain</returns>
        public static int FindMaxDay(int[] rainfall)
        {
            int maxDay = 0;
            int max = 0;

            for (int j = 0; j < rainfall.Length; j++)
            {
                if (rainfall[j] >= max)
                {
                    max = rainfall[j];
                    maxDay = j;
                }
            }
            return maxDay;
        }//end FindMaxDay

        /// <summary>
        /// This method is used to calculate the average amount of rain by dividing 
        /// the total by the length of the array
        /// </summary>
        /// <param name="rainfall">Used to navigate the length of the j</param>
        /// <returns>Returns the value of the Average rainfall</returns>
        public static double FindAverageRain(int[] rainfall)
        {
            double AvgRain = 0;
            double total = 0;

            for (int j = 0; j < rainfall.Length; j++)
            {
                total = total + rainfall[j];
            }

            AvgRain = total / rainfall.Length;
            return AvgRain;
        } //end FindAverageRain

        static void OutputRainDays(int[] daysNoRain, int[] DaysRained, int monthInput)
        {
            Console.WriteLine("\n\nThere were " + daysNoRain[monthInput - 1] + " days without rain in the month of " + (Months)(monthInput) + " and " + DaysRained[monthInput - 1] + " with rain");
        }//end OutputRainDays

        static void OutputRainMax(int[][] rainfall, int monthInput)
        {
            Console.WriteLine("\nand the maximum amount on a single day in " + (Months)(monthInput) + " was " + FindMax(rainfall[monthInput - 1]) + "mm");
        }//end OutputRainMax

        static void OutputDayMax(int[][] rainfall, int monthInput)
        {
            Console.WriteLine("\nwhich occured on day " + FindMaxDay(rainfall[monthInput - 1]) + " of that month");
        }//end OutputDayMax

        static void OutputAverage(int[][] rainfall, int monthInput)
        {
            double average = FindAverageRain(rainfall[monthInput - 1]);

            Console.WriteLine("\nand the average rainfall for that month was {0:f2}mm", average);
        }//end OutputAverage

        static void ExitProgram()
        {
            Console.Write("\n\nPress any key to exit.");
            Console.ReadKey();
        }//end ExitProgram

    }//end class
}//end namespace
