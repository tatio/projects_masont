﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMICalculator
{
    public partial class frmBMI : Form
    {
        public frmBMI()
        {
            InitializeComponent();
        }

        public double GetHeight()
        {
            double heightM = 0;
            string inputHeight = "";
            bool okay = false;

            inputHeight = tboxHeight.Text;

            okay = double.TryParse(inputHeight, out heightM);

            if (okay && (heightM <= 1.19 || heightM > 2.7))
            {
                MessageBox.Show("Height should be greater than or equal to 1.2m and less than 2.7");
                return -1;
            }
            else if (!okay)
            {
                MessageBox.Show("Height value is not numeric");
                return -1;
            }

            return heightM;
        }

        public double GetWeight()
        {
            double weightKgs = 0;
            string inputWeight = "";
            bool okay = false;

            inputWeight = tboxWeight.Text;

            okay = double.TryParse(inputWeight, out weightKgs);

            if (okay && weightKgs <= 44)
            {
                MessageBox.Show("Weight should be greater than or equal to 45kgs");
                return -1;
            }
            else if (!okay)
            {
                MessageBox.Show("Weight value is not numeric");
                return -1;
            }

            return weightKgs;
        }

        public void CalculateBMI(double weightKgs, double heightM)
        {
            double BMI = 0;

            BMI = weightKgs / (heightM * heightM);
            BMI = Math.Round(BMI, 2);

            if (BMI < 18.5)
            {
                MessageBox.Show("Your BMI is " + BMI + " and therefore you are underweight.");
            }
            else if (BMI > 18.5 && BMI < 24.99)
            {
                MessageBox.Show("Your BMI is " + BMI + " and therefore you are healthy bodyweight.");
            }
            else if (BMI > 25 && BMI < 29.99)
            {
                MessageBox.Show("Your BMI is " + BMI + " and therefore you are overweight.");
            }
            else if (BMI >= 30)
            {
                MessageBox.Show("Your BMI is " + BMI + " and therefore you are a fat sack of dog shit.");
            }

            //MessageBox.Show("Your BMI is " + BMI);
        }



        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double weightKgs = 0;
            double heightM = 0;

            weightKgs = GetWeight();
            heightM = GetHeight();

            if (weightKgs > 0 && heightM > 0)
            {
                CalculateBMI(weightKgs, heightM);
                gboxAnother.Visible = true;
            }
        }
    }
}
