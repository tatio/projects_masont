﻿namespace BMICalculator
{
    partial class frmBMI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBMI));
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.rbtnYes = new System.Windows.Forms.RadioButton();
            this.rbtnNo = new System.Windows.Forms.RadioButton();
            this.gboxAnother = new System.Windows.Forms.GroupBox();
            this.tboxWeight = new System.Windows.Forms.TextBox();
            this.tboxHeight = new System.Windows.Forms.TextBox();
            this.lblHeight = new System.Windows.Forms.Label();
            this.gboxAnother.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCalculate
            // 
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculate.ForeColor = System.Drawing.Color.Black;
            this.btnCalculate.Location = new System.Drawing.Point(123, 195);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(94, 49);
            this.btnCalculate.TabIndex = 0;
            this.btnCalculate.Text = "Calculate BMI";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(40, 22);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(268, 24);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Body Mass Index Calculator";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.BackColor = System.Drawing.Color.Transparent;
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.ForeColor = System.Drawing.Color.White;
            this.lblWeight.Location = new System.Drawing.Point(41, 116);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(95, 18);
            this.lblWeight.TabIndex = 2;
            this.lblWeight.Text = "Weight (kg)";
            // 
            // rbtnYes
            // 
            this.rbtnYes.AutoSize = true;
            this.rbtnYes.Location = new System.Drawing.Point(17, 49);
            this.rbtnYes.Name = "rbtnYes";
            this.rbtnYes.Size = new System.Drawing.Size(53, 20);
            this.rbtnYes.TabIndex = 4;
            this.rbtnYes.TabStop = true;
            this.rbtnYes.Text = "Yes";
            this.rbtnYes.UseVisualStyleBackColor = true;
            // 
            // rbtnNo
            // 
            this.rbtnNo.AutoSize = true;
            this.rbtnNo.BackColor = System.Drawing.Color.Transparent;
            this.rbtnNo.Location = new System.Drawing.Point(109, 49);
            this.rbtnNo.Name = "rbtnNo";
            this.rbtnNo.Size = new System.Drawing.Size(46, 20);
            this.rbtnNo.TabIndex = 5;
            this.rbtnNo.TabStop = true;
            this.rbtnNo.Text = "No";
            this.rbtnNo.UseVisualStyleBackColor = false;
            // 
            // gboxAnother
            // 
            this.gboxAnother.BackColor = System.Drawing.Color.Transparent;
            this.gboxAnother.Controls.Add(this.rbtnYes);
            this.gboxAnother.Controls.Add(this.rbtnNo);
            this.gboxAnother.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gboxAnother.ForeColor = System.Drawing.Color.Black;
            this.gboxAnother.Location = new System.Drawing.Point(68, 264);
            this.gboxAnother.Name = "gboxAnother";
            this.gboxAnother.Size = new System.Drawing.Size(200, 100);
            this.gboxAnother.TabIndex = 6;
            this.gboxAnother.TabStop = false;
            this.gboxAnother.Text = "Another Calculation?";
            this.gboxAnother.Visible = false;
            // 
            // tboxWeight
            // 
            this.tboxWeight.Location = new System.Drawing.Point(44, 137);
            this.tboxWeight.Name = "tboxWeight";
            this.tboxWeight.Size = new System.Drawing.Size(100, 20);
            this.tboxWeight.TabIndex = 7;
            // 
            // tboxHeight
            // 
            this.tboxHeight.Location = new System.Drawing.Point(208, 137);
            this.tboxHeight.Name = "tboxHeight";
            this.tboxHeight.Size = new System.Drawing.Size(100, 20);
            this.tboxHeight.TabIndex = 8;
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.BackColor = System.Drawing.Color.Transparent;
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.ForeColor = System.Drawing.Color.White;
            this.lblHeight.Location = new System.Drawing.Point(205, 115);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(87, 18);
            this.lblHeight.TabIndex = 9;
            this.lblHeight.Text = "Height (m)";
            // 
            // frmBMI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(362, 436);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.tboxHeight);
            this.Controls.Add(this.tboxWeight);
            this.Controls.Add(this.gboxAnother);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.btnCalculate);
            this.Name = "frmBMI";
            this.Text = "Body Mass Index";
            this.gboxAnother.ResumeLayout(false);
            this.gboxAnother.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.RadioButton rbtnYes;
        private System.Windows.Forms.RadioButton rbtnNo;
        private System.Windows.Forms.GroupBox gboxAnother;
        private System.Windows.Forms.TextBox tboxWeight;
        private System.Windows.Forms.TextBox tboxHeight;
        private System.Windows.Forms.Label lblHeight;
    }
}

