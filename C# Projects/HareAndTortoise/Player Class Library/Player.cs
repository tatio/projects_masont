﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Square_Class_Library;
using Die_Class_Library;

namespace Player_Class_Library
{
/*
* The Player Class containing all relevant methods
* relating to the player as well as additional booleans for board movement
*
* Author: Mason Teitzel, n9440941
* Date: October 2015
*
*/
    public class Player
    {
        private string name;
        private Square location;
        private Image playerTokenImage;
        private Brush playerTokenColour;
        private int money;
        private bool hasWon;
        private bool rollAgain;
        private bool moveBack;
        private int numSteps;

        public Player()
        {
            throw new ArgumentException();
        }//end Player

        public Player(string name, Square location)
        {
            this.name = name;
            this.location = location;
            this.money = 100;
        }//end Player

        public int Money
        {
            set
            {
                money = value;
            }
            get
            {
                return money;
            }
        } //end Money Property

        public bool HasWon
        {
            set
            {
                hasWon = value;
            }
            get
            {
                return hasWon;
            }
        } //end HasWon Property

        public string Name
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        } //end Name Property

        public Square Location
        {
            set
            {
                location = value;
                location.EffectOnPlayer(this);
            }
            get
            {
                return location;
            }
        } // end Location Property

        public Image PlayerTokenImage
        {
            set
            {
                playerTokenImage = value;
            }
            get
            {
                return playerTokenImage;
            }
        } //end PlayerTokenImage Property

        public Brush PlayerTokenColor
        {
            set
            {
                playerTokenColour = value;
                playerTokenImage = new Bitmap(1, 1);
                using (Graphics g = Graphics.FromImage(playerTokenImage))
                {
                    g.FillRectangle(playerTokenColour, 0, 0, 1, 1);
                }
            }
            get
            {
                return playerTokenColour;
            }
        } //end PlayerTokenColor Property

        public int RollDie(Die d1, Die d2)
        {
            int totalRoll = d1.Roll() + d2.Roll();
            return totalRoll;
        } //end Rolldie Method

        public void Add(int amount)
        {
            money = money + amount;
        } //end Add

        public void Deduct(int amount)
        {
            money = money - amount;
            if (money < 0)
            {
                money = 0;
            }
        } //end Deduct

        public bool RollAgain
        {
            set
            {
                rollAgain = value;
            }
            get
            {
                return rollAgain;
            }
        }//end RollAgain
        public bool MoveBack
        {
            set
            {
                moveBack = value;
            }
            get
            {
                return moveBack;
            }
        } //end MoveBack

        public int NumSteps
        {
            set
            {
                numSteps = value;
            }
            get
            {
                return numSteps;
            }
        } //end NumSteps
    }//end Player Class
}
