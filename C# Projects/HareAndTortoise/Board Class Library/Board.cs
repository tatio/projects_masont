﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Square_Class_Library;

namespace Board_Class_Library
{
/*
* The Board Class
* Contains all methods related to the board such as square values and object initializing
*
* Author: Mason Teitzel, n9440941
* Date: October 2015
*
*/
    public static class Board
    {
        public const int Lose = 10;
        public const int Win = 5;
        public const int Chance = 6;
        public const int Finish_Square = 55;
        public const int Start_Square = 0;
        public const int Next_Square = 1;

        private static Square[] gameBoard = new Square[56];

        public static void SetUpBoard()
        {
            gameBoard[Start_Square] = new Square("Start", Start_Square);
            gameBoard[Finish_Square] = new Square("Finish", Finish_Square);

            for (int i = 1; i < gameBoard.Length - 1; i++)
            {
                if (i % Lose == 0)
                {
                    gameBoard[i] = new Lose_Square(i.ToString(), i);
                }
                else if (i % Win == 0)
                {
                    gameBoard[i] = new Win_Square(i.ToString(), i);
                }
                else if (i % Chance == 0)
                {
                    gameBoard[i] = new Chance_Square(i.ToString(), i);
                }
                else
                {
                    gameBoard[i] = new Square(i.ToString(), i);
                }
            }
        }//end SetUpBoard

        public static Square GetGameBoardSquare(int number)
        {
            if (number < Start_Square || number > Finish_Square)
            {
                throw new Exception("Intger is out of bound...");
            }
            else
            {
                return gameBoard[number];
            }
        } //end GetGameBoardSquare

        public static Square StartSquare()
        {
            return gameBoard[Start_Square];
        } //end StartSquare

        public static Square NextSquare(int number)
        {
            if (number < Start_Square || number >= Finish_Square)
            {
                throw new Exception("Intger is out of bound...");
            }
            else
            {
                return gameBoard[number + Next_Square];
            }
        } //end NextSquare

        public static Square[] GetBoard()
        {
            return gameBoard;
        } //end Square
    }//end Board Class
}
