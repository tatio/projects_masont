﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;

namespace Square_Class_Library
{
    public class Lose_Square : Square
    {
        public Lose_Square(string name, int number)
            : base(name, number)
        {

        }//end Lose_Square

        public override void EffectOnPlayer(Player who)
        {
            //Code moved for animation functionality
        }//end EffectOnPlayer
    }//end Lose_Square Class
}
