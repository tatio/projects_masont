﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;

namespace Square_Class_Library
{
    public class Chance_Square : Square
    {
        public Chance_Square(string name, int number)
            : base(name, number)
        {

        }//end Chance_Square

        public override void EffectOnPlayer(Player who)
        {
            //Code moved for animation functionality
        }//end EffectOnPlayer
    }//end Chance_Square Class
}
