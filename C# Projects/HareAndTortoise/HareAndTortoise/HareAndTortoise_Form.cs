﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Square_Class_Library;
using Player_Class_Library;
using Board_Class_Library;
using System.Diagnostics;
using System.Threading;

namespace HareAndTortoise
{
/*
* The HareAndTortise Form
* contains all the form methods (buttons, listbox, etc...)
* as well as addition methods for calculating the board and detail outputting
*
* Author: Mason Teitzel, n9440941
* Date: October 2015
*
*/
    public partial class HareAndTortoise_Form : Form
    {

        const int NUM_OF_ROWS = 8;
        const int NUM_OF_COLUMNS = 7;

        public HareAndTortoise_Form()
        {
            InitializeComponent();
            HareAndTortoise_Game.SetUpGame();
            ResizeGameBoard();
            SetUpGuiGameBoard();
            dgvPlayers.DataSource = HareAndTortoise_Game.Players;
            UpdateSquare(true);
            Trace.Listeners.Add(new ListBoxTraceListener(lboxPlayers));
        }

        /// <summary>
        /// Sets up the Game Board Gui, adding functionality to the gameBoardPanel
        /// Object allows for the background colour to be changed (Start & Finish)
        /// </summary>
        public void SetUpGuiGameBoard()
        {
            int row = 0;
            int col = 0;

            foreach (Square s in Board.GetBoard())
            {
                SquareControl sq = new SquareControl(s, HareAndTortoise_Game.Players);

                if (s.Number == Board.Start_Square || s.Number == Board.Finish_Square)
                {
                    sq.BackColor = System.Drawing.Color.BurlyWood;
                }

                MapSquareToTablePanel(s.Number, out row, out col);
                gameBoardPanel.Controls.Add(sq, col, row);
            }
        }

        /// <summary>
        /// The maps the row & columns using an equation to position them based on modulus remainders of 0
        /// Row refined using Floor to return a value then Aboslute to give positive value minus 7
        /// The reversal was based on modulus remainder 0 as it is zero for every second row
        /// </summary>
        /// <param name="number">player number</param>
        /// <param name="row">row val</param>
        /// <param name="column">column val</param>
        private static void MapSquareToTablePanel(int number, out int row, out int column)
        {
            column = (number % 7);

            row = (int)Math.Floor(number / 7.0);
            row = Math.Abs(row - 7);

            if (row % 2 == 0)
            {
                column = Math.Abs(column - 6);
            }
        } //end SetUpGuiGameBoard()

        /// <summary>
        /// Resizes the board (Code wasn't touched)
        /// </summary>
        private void ResizeGameBoard()
        {
            const int SQUARE_SIZE = SquareControl.SQUARE_SIZE;
            int currentHeight = gameBoardPanel.Size.Height;
            int currentWidth = gameBoardPanel.Size.Width;
            int desiredHeight = SQUARE_SIZE * NUM_OF_ROWS;
            int desiredWidth = SQUARE_SIZE * NUM_OF_COLUMNS;
            int increaseInHeight = desiredHeight - currentHeight;
            int increaseInWidth = desiredWidth - currentWidth;
            this.Size += new Size(increaseInWidth, increaseInHeight);
            gameBoardPanel.Size = new Size(desiredWidth, desiredHeight);
        } //end ResizeGameBoard

        /// <summary>
        /// Used to update the Squares on the board
        /// </summary>
        /// <param name="toMove">Used to determine if there's a player on the square or not</param>
        public void UpdateSquare(bool toMove)
        {
            int col, row;
            for (int i = 0; i < HareAndTortoise_Game.numberOfPlayers; i++)
            {
                Square thisSquare = HareAndTortoise_Game.Players[i].Location;
                MapSquareToTablePanel(thisSquare.Number, out row, out col);
                SquareControl sq = (SquareControl)gameBoardPanel.GetControlFromPosition(col, row);
                sq.ContainsPlayers[i] = toMove;
                gameBoardPanel.Invalidate(true);

                if (thisSquare.Number == Board.Finish_Square)
                {
                    btnRoll.Enabled = false;
                }
            }
        } //end UpdateSquare


        /// <summary>
        /// Button click rolls for each player, players move after eachother, step by step (animation)
        /// All squares are updated to allow the players to move
        /// Methods called from game to add functionality to the moving forwards and back on special squares
        /// NOTE: The sleep has been changed slightly to see when a player hits a special square
        /// It'll slow when a user passes over a special square
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">EventArgs</param>
        private void btnRoll_Click(object sender, EventArgs e)
        {
            btnRoll.Enabled = false;
            btnReset.Enabled = false;
            btnExit.Enabled = false;
            cboxPlayers.Enabled = false;
            for (int i = 0; i < HareAndTortoise_Game.numberOfPlayers; i++)
            {

                    int playerRolled = HareAndTortoise_Game.PlayerRoll(i);

                    for (int j = 0; j < playerRolled; j++)
                    {
                        UpdateSquare(false);
                        HareAndTortoise_Game.PlayOneRound(i);
                        UpdateSquare(true);
                        Application.DoEvents();
                        Thread.Sleep(100);
                    }
                    UpdateSquare(false);
                    bool rollAgain = HareAndTortoise_Game.CheckPlayerRollAgain(i);
                    int moveBack = HareAndTortoise_Game.CheckPlayerMoveBack(i);
                    bool playerMove;
                    if (moveBack != -1)
                    {
                        playerMove = true;
                    }
                    else
                    {
                        playerMove = false;
                    }

                    while (rollAgain == true || playerMove == true)
                    {
                        if (HareAndTortoise_Game.finished)
                        {
                            break; //ends loop
                        }

                        if (rollAgain)
                        {
                            rollAgain = false;
                            playerRolled = HareAndTortoise_Game.PlayerRoll(i);

                            for (int z = 0; z < playerRolled; z++)
                            {
                                UpdateSquare(false);
                                HareAndTortoise_Game.PlayOneRound(i);
                                UpdateSquare(true);
                                Application.DoEvents();
                                Thread.Sleep(300);
                            }
                        }
                        else if (playerMove)
                        {
                            playerMove = false;
                            int type = moveBack;
                            int stepCount = Math.Abs(moveBack);
                            moveBack = -1;

                            for (int L = 0; L < stepCount; L++)
                            {
                                UpdateSquare(false);
                                HareAndTortoise_Game.MovePlayerBack(i, type);
                                UpdateSquare(true);
                                Application.DoEvents();
                                Thread.Sleep(300);
                            }
                        }

                        UpdateSquare(true);
                        playerMove = false;
                        rollAgain = false;
                        rollAgain = HareAndTortoise_Game.CheckPlayerRollAgain(i);
                        moveBack = HareAndTortoise_Game.CheckPlayerMoveBack(i);

                        if (moveBack != -1)
                        {
                            playerMove = true;
                        }
                        else
                        {
                            playerMove = false;
                        }
                        UpdateSquare(true);
                    }
            }

            HareAndTortoise_Game.OutputAllPlayerDetails();
            UpdateDataGridView();
            cboxPlayers.Enabled = false;
            UpdateSquare(true);
            btnRoll.Enabled = true;
            btnReset.Enabled = true;
            btnExit.Enabled = true;

            if (HareAndTortoise_Game.finished)
            {
                btnRoll.Enabled = false;
                HareAndTortoise_Game.determineWinner();
                HareAndTortoise_Game.finished = false;
            }
        }

        /// <summary>
        /// Outputs the Player Details on the board to the listbox
        /// </summary>
        private void OutputPlayersDetails()
        {
            HareAndTortoise_Game.OutputAllPlayerDetails();
            lboxPlayers.Items.Add("");
            lboxPlayers.SelectedIndex = lboxPlayers.Items.Count - 1;
        } //end OutputPlayersDetails

        /// <summary>
        /// Updates the Data Grid
        /// </summary>
        public void UpdateDataGridView()
        {
            HareAndTortoise_Game.Players.ResetBindings();
        } //end UpdateDataGridView

        /// <summary>
        /// Closes the program
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event e</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult exitResult = MessageBox.Show("Are you sure you want to exit?", "Are you sure?", MessageBoxButtons.YesNo);
            if (exitResult == DialogResult.Yes)
            {
                this.Close();
            }
            else if (exitResult == DialogResult.No)
            {

            }
        } //end btnExit_Click


        /// <summary>
        /// Resets the game, restoring it to when the game is first initialised
        /// Or to the desired new amount of players if selected, followed by
        /// functions to the amount of players chosen, eg listbox displays
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event e</param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            UpdateSquare(false);
            var numberText = cboxPlayers.Text;
            var number = int.Parse(numberText);
            HareAndTortoise_Game.numberOfPlayers = number;
            HareAndTortoise_Game.Players.Clear();
            HareAndTortoise_Game.PlayerInitalise();
            UpdateSquare(false);
            btnRoll.Enabled = true;
            cboxPlayers.Enabled = true;
            UpdateDataGridView();
            UpdateSquare(true);
            lboxPlayers.Items.Clear();
        } //end btnReset_Click

        /// <summary>
        /// Similar to the reset button, when a value is selected it resets the game
        /// and changes the desired players to the selected, then updates the listbox 
        /// and functions as per selection (num of players)
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">Event e</param>
        private void cboxPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var numberText = cboxPlayers.Text;
            var number = int.Parse(numberText);
            HareAndTortoise_Game.numberOfPlayers = number;
            HareAndTortoise_Game.Players.Clear();
            HareAndTortoise_Game.PlayerInitalise();
            UpdateSquare(false);
            btnRoll.Enabled = true;
            cboxPlayers.Enabled = true;
            UpdateDataGridView();
            UpdateSquare(true);
            lboxPlayers.Items.Clear();
        } //end cboxPlayers_SelectedIndexChanged
    }//end class 
} //end namespace
