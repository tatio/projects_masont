﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Square_Class_Library;
using Player_Class_Library;
using Board_Class_Library;
using Die_Class_Library;
using System.Drawing;
using System.Diagnostics;
 using System.Security;
 using System.Threading;


namespace HareAndTortoise
{
/*
* The HareAndTortise Game
* contains all relevant methods in relation to gameplay and functionality 
* of players, dice rolling, board initalising, etc...
*
* Author: Mason Teitzel, n9440941
* Date: October 2015
*
*/
    public static class HareAndTortoise_Game
    {
        private static string[] playerNumber = { "One", "Two", "Three", "Four", "Five", "Six" };
        private static Brush[] playerColor = { Brushes.Black, Brushes.Red, Brushes.Gold, 
                                               Brushes.GreenYellow, Brushes.Fuchsia, Brushes.BlueViolet};
        public static int numberOfPlayers = 6;
        static Die d1 = new Die(6);
        static Die d2 = new Die(6);
        public static bool finished = false;
        private static BindingList<Player> players = new BindingList<Player>();

        const int winMod = 5;
        const int loseMod = 10;
        const int chanceMod = 6;

        /// <summary>
        /// returns the Binding list allowing it for use
        /// </summary>
        public static BindingList<Player> Players
        {
            get
            {
                return players;
            }
        } //end BindingList<Player>


        /// <summary>
        /// Calls methods to setup game
        /// </summary>
        public static void SetUpGame()
        {
            Board.SetUpBoard();
            PlayerInitalise();
        }// end SetUpGame

        /// <summary>
        /// Initializes all the players, location, list, as well as added object controls
        /// </summary>
        public static void PlayerInitalise()
        {
            for (int i = 0; i < numberOfPlayers; i++)
            {
                Player p = new Player(playerNumber[i], Board.StartSquare());
                p.PlayerTokenColor = playerColor[i];
                Players.Add(p);
                Players[i].RollAgain = false;
                Players[i].MoveBack = false;
                Players[i].NumSteps = 0;
            }
        } //end PlayerInitalise

        /// <summary>
        /// Resets all controls related to the players
        /// </summary>
        public static void ResetPlayers()
        {
            for (int i = 0; i < numberOfPlayers; i++)
            {
                Players[i].Location = Board.GetGameBoardSquare(Board.Start_Square);
                Players[i].Money = 100;
                Players[i].HasWon = false;
                finished = false;
                Players[i].RollAgain = false;
                Players[i].MoveBack = false;
                Players[i].NumSteps = 0;
            }
        } //end ResetPlayers

        /// <summary>
        /// Rolls the dice returning a role value
        /// </summary>
        /// <param name="playernumber">the players index</param>
        /// <returns>Roll value of the two die rolled</returns>
        public static int PlayerRoll(int playernumber)
        {
            int totalRoll = Players[playernumber].RollDie(d1, d2);
            return totalRoll;
        } //end PlayerRoll

        /// <summary>
        /// Plays a single round on the board, by moving, 
        /// checking for location to initialize finish state
        /// as well as restraints to going outside the board
        /// </summary>
        /// <param name="playernumber">the players index</param>
        public static void PlayOneRound(int playernumber)
        {
            int dieRoll = 0;

            if (Players[playernumber].Location.Number == Board.Finish_Square)
            {
                finished = true;
            }
            else if (Players[playernumber].Location.Number + dieRoll > Board.Finish_Square)
            {
                Players[playernumber].Location = Board.GetGameBoardSquare(Board.Finish_Square);
                finished = true;
            }
            else if (Players[playernumber].Location.Number < 55)
            {
                Players[playernumber].Location = Board.GetGameBoardSquare(1 + players[playernumber].Location.Number);
            }
            if (Players[playernumber].Location.Number == 55)
            {
                finished = true;
            }
        } //end PlayOneRound

        /// <summary>
        /// Checks if the player is required to move again based on the square
        /// hence use of modulus based on square value
        /// </summary>
        /// <param name="playernumber">the players index</param>
        /// <returns></returns>
        public static bool CheckPlayerRollAgain(int playernumber)
        {
            var square = Board.GetGameBoardSquare(Players[playernumber].Location.Number);
            int modWin = square.Number % 5;
            int modTen = square.Number % 10;
            return false;
        }//end CheckPlayerRollAgain


        /// <summary>
        /// For the additional moving forward and back functions of the win/loss/chance squares (moved from effect on player)
        /// Using modulus to compare square values for each of the special squares
        /// Returns value based on the special square
        /// </summary>
        /// <param name="playernumber">the players number</param>
        /// <returns>returns -1 (back 1 square) for the moveback method</returns>
        public static int CheckPlayerMoveBack(int playernumber)
        {
            var square = Board.GetGameBoardSquare(Players[playernumber].Location.Number);
            int modChance = square.Number % chanceMod;
            int modLose = square.Number % loseMod;
            int modWin = square.Number % winMod;

            if (modWin == 0 && modLose != 0)
            {
                Players[playernumber].Add(15);
                int playerRolled = PlayerRoll(playernumber);
                Players[playernumber].MoveBack = true;
                return playerRolled;
            }

            else if (modLose == 0)
            {
                Players[playernumber].Deduct(25);
                Players[playernumber].MoveBack = true;
                return -3;
            }

            else if (square.Number != 30 && modChance == 0) //!30 as it's not a special square
            {
                Random rnd = new Random();
                int i = rnd.Next(0, 2);

                if (i == 0)
                {
                    Players[playernumber].Add(50);
                    Players[playernumber].MoveBack = true;
                    return 5;
                }

                if (i == 1)
                {
                    Players[playernumber].Deduct(50);
                    Players[playernumber].MoveBack = true;
                    return -5;
                }
            }
            return -1;
        }//end CheckPlayerMoveBack

        /// <summary>
        /// Finished state to determined winner based on finished (bool)
        /// Compares players to determines winner, eg max amount of money location on the board
        /// Using list to contain the index to be compared against the first player
        /// </summary>
        public static void determineWinner()
        {
            if (finished)
            {
                List<int> playerIndex = new List<int>();
                int winnerMoney = players[0].Money;
                int playerCount = 0;
                playerIndex.Add(0);

                for (int i = 0; i < numberOfPlayers; i++)
                {
                    if (players[i].Money > winnerMoney)
                    {
                        winnerMoney = players[i].Money;
                        playerIndex.Clear();
                        playerIndex.Add(i);
                        playerCount = 1;
                    }
                    else if (players[i].Money == winnerMoney)
                    {
                        if (players[i].Location.Number > Players[playerIndex[0]].Location.Number)
                        {
                            winnerMoney = players[i].Money;
                            playerIndex.Clear();
                            playerIndex.Add(i);
                            playerCount = 1;
                        }
                        else if (players[i].Location.Number == Players[playerIndex[0]].Location.Number)
                        {
                            playerIndex.Add(i);
                            playerCount++;
                        }
                    }
                }

                if (playerCount > 1)
                {
                    Trace.WriteLine("And the Winner(s) is...\n");
                    for (int z = 0; z < playerCount; z++)
                    {
                        Trace.WriteLine(String.Format("Player {0} with {1:C}\n", playerIndex[z] + 1, players[playerIndex[z]].Money));
                    }
                }
                else
                {
                    Players[playerIndex[0]].HasWon = true;
                    Trace.WriteLine("And the Winner(s) is...\n");
                    Trace.WriteLine(String.Format("Player {0} with {1:C}\n", (playerIndex[0] + 1), Players[playerIndex[0]].Money));
                }
            }
        } //end determineWinner

        /// <summary>
        /// Outputs each players details
        /// </summary>
        public static void OutputAllPlayerDetails()
        {
            for (int i = 0; i < numberOfPlayers; i++)
            {
                OutputIndividualDetails(Players[i]);
            }
            Trace.WriteLine("");
        } // end OutputAllPlayerDetails

        /// <summary>
        /// Moves the player based on the square value (win or lose)
        /// </summary>
        /// <param name="playerNum">players number</param>
        /// <param name="type">type of square</param>
        public static void MovePlayerBack(int playerNum, int type)
        {
            int number = 0;
            if (type > 0)
            {
                number = 1;
            }
            else
            {
                number = -1;
            }

            if (Players[playerNum].Location.Number + number > Board.Finish_Square)
            {
                Players[playerNum].Location = Board.GetGameBoardSquare(Board.Finish_Square);
                finished = true;
            }
            else
            {
                Players[playerNum].Location = Board.GetGameBoardSquare(players[playerNum].Location.Number + number);
            }
        } //MovePlayerBack

        /// <summary>
        /// Outputs a player's current location and amount of money
        /// pre:  player's object to display
        /// post: displayed the player's location and amount
        /// </summary>
        /// <param name="who">the player to display</param>
        public static void OutputIndividualDetails(Player who)
        {
            if (!finished)
            {
                Square playerLocation = who.Location;
                Trace.WriteLine(String.Format("Player {0} on square {1} with {2:C}",
                                               who.Name, playerLocation.Name, who.Money));
            }
        }// end OutputIndividualDetails
    }//end class
}//end namespace
